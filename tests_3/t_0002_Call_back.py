import sys
import re
from time import sleep
import traceback
from src.DriverObject import driver_object
from src.PageObject import page_element, elements

"""LOCATORS"""
locators = dict(
                css_search="input#title-search-input_fixed",
                css_callback="span.callback-block",
                css_form_callback="div.CALLBACK",
                css_aria_name="input[data-sid=CLIENT_NAME]",
                css_aria_tel="input[data-sid=PHONE]",
                css_sumbit="div.CALLBACK input[type=submit]",
                css_error="label.error, div.alert",
                css_success="div.CALLBACK div.success"
                )
""""LOCATORS"""
type_locator = "css"
result = []


def step_1(Test):
    el = elements(Test, locators)
    print(el)
    Test.get("https://confaelshop.ru/")
    Test.doc_readyState()
    Test.ajax_complite()
    el.css_callback.click()
    return el.css_form_callback.visible

def step_2(Test, values):
    el = elements(Test, locators)
    el.css_aria_name.scroll = False
    el.css_aria_name.double_click()
    flag = el.css_aria_name.input(values["name"], return_check=True)
    if not flag:
        raise Exception("Ошибка ввода поле имя")
    el.css_aria_tel.scroll = False
    el.css_aria_tel.double_click()
    el.css_aria_tel.input(values["tel"], check=False)
    if not flag:
        raise Exception("Ошибка ввода поле телефон")
    return True


def step_3(Test, type_test):
    el = elements(Test, locators)
    el.css_sumbit.scroll = False
    el.css_sumbit.message = f"Ошибка подтверждения {el.css_sumbit}"
    el.css_sumbit.click()
    if type_test == "None":
        el.css_error.scroll = False
        el.css_error.message = f"Ошибка не найден элемент {el.css_error}"
        if el.css_error.in_page():
            return len(el.css_error.return_all()) == 2
        return False
    elif type_test == "False":
        el.css_error.scroll = False
        el.css_error.message = f"Ошибка не найден элемент {el.css_error}"
        if el.css_error.in_page():
            return len(el.css_error.return_all()) == 1
        return False
    elif type_test == "True":
        el.css_success.scroll = False
        el.css_success.message = f"Ошибка ожидания появления сообщения об успехе формы"
        if el.css_success.in_page():
            return True
        return False
