import sys
import re
from time import sleep
import traceback
from src.DriverObject import driver_object
from src.PageObject import elements
from src.PageObject import page_element
import src.steps as steps
import pytest
"""LOCATORS"""
locators = dict(
                search="input#title-search-input_fixed",
                product_link="div.block_list div.item-title a",
                btn_one_click="span.one_click",
                locator_form="input#one_click_buy_id_PHONE",
                aria_name="input[name*=FIO]",
                aria_tel="input[name*=PHONE]",
                aria_email="input[name*=EMAIL]",
                aria_addres="input[name*=ADDRESS]",
                sumbit="button#one_click_buy_form_button",
                error="div.form-control label.error",
                success="div.one_click_buy_result_success")
""""LOCATORS"""


def step_1(Test):
    return steps.go_to_product_page(Test)


def step_2(Test, type_input, debug=False):
    Test.doc_readyState()
    Test.ajax_complite
    el = elements(Test, locators)
    el.btn_one_click.click()
    if type_input == "None":
        name = ""
        tel = ""
        email = ""
        address = ""
        count = 4
        locator_result = locators["error"]
    elif type_input == "False":
        name = "Тестирование !@#$%^&*(123"
        tel = "Тестирование !@#$%^&*(123"
        email = "Тестирование !@#$%^&*(123"
        address = "Тестирование !@#$%^&*(123"
        count = 2
        locator_result = locators["error"]
    else:
        name = "Тестирование"
        tel = "90698989898"
        email = "al8594212@gmail.com"
        address = "Тестирование"
        locator_result = locators["success"]
        count = 1
    el.aria_tel.scroll = False
    el.aria_tel.double_click()
    el.aria_tel.input(tel, check=False)

    el.aria_email.scroll = False
    el.aria_email.double_click()
    el.aria_email.input(email)

    el.aria_name.scroll = False
    el.aria_name.double_click()
    el.aria_name.input(name)

    el.aria_addres.scroll = False
    el.aria_addres.double_click()
    el.aria_addres.input(address)

    el.sumbit.click()
    label = page_element(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {locator_result}")
    if label.in_page():
        return len(label.return_all()) == count
    return False

if __name__ == "__main__":
    with driver_object("ff") as Test:
        step_1(Test)
        step_2(Test, "False")
