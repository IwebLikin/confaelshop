import sys
import re
from time import sleep, time
import traceback
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps
from src.PageObject import elements
import src.mail as mail
"""LOCATORS"""
locators = dict(
                search="input#title-search-input_fixed",
                wait="div.mobileheader-v1 I.svg-inline-basket",
                product_link="div.block_list div.item-title a",
                btn_send_ask="div.catalog_detail  li.product_ask_tab",
                mobile_btn_send_ask="div#ask",
                ask="textarea[data-sid=QUESTION]",
                name="input[data-sid=CLIENT_NAME]",
                tel="input[data-sid=PHONE]",
                email="input[data-sid=EMAIL]",
                sumbit="div.catalog_detail input.btn",
                error="div.catalog_detail label.error",
                success="div.form_result.success"
                )
""""LOCATORS"""

def step_1(Test):
    return steps.go_to_product_page(Test)

def step_2(Test):
    el = elements(Test, locators)
    if Test.browser_name in ("ie", "c", "ff"):
        btn = el.btn_send_ask
        Test.doc_readyState()
        Test.ajax_complite()
    else:
        btn = el.mobile_btn_send_ask
        if Test.browser_name == "ipad":
            btn = el.btn_send_ask
        Test.doc_readyState()
        Test.ajax_complite()
    for a in range(3):
        if "active" in btn.click().get_attribute("class"):
            return True
        sleep(0.5)


def step_3(Test, type_input):
    el = elements(Test, locators)
    if type_input == "None":
        ask = ""
        name = ""
        tel = ""
        email = ""
        locator_result = locators["error"]
        count = 3
    elif type_input == "False":
        ask = "Тестирование !@#$%^&*(12"
        name = "Тестирование !@#$%^&*(12"
        tel = "Тестирование !@#$%^&*(12"
        email = "Тестирование !@#$%^&*(12"
        locator_result = locators["error"]
        count = 2
    else:
        ask = "Много слов"
        name = "Тестирование"
        tel = "89070834444"
        email = "al8594212@gmail.com"
        locator_result = locators["success"]
        count = 1
    el.tel.scroll = False
    el.tel.double_click()
    el.tel.input(tel, check=False)

    el.email.scroll = False
    el.email.double_click()
    el.email.input(email)

    el.ask.scroll = False
    el.ask.double_click()
    el.ask.input(ask)

    el.name.scroll = False
    el.name.double_click()
    el.name.input(name)

    el.sumbit.click()
    label = elem(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {locator_result}")
    if label.in_page():
        return len(label.return_all()) == count
    return False

if __name__ == "__main__":
    with driver_object("galaxys5") as Test:
        step_1(Test)
        step_2(Test)
        step_3(Test, "False")
