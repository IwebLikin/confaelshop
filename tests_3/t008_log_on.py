import sys
import re
from time import sleep, time
import traceback
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps
import src.mail as mail

def step_1(Test):
    return steps.open_log_on(Test)


def step_2(Test, login):
    return steps.log_on(Test, login)
