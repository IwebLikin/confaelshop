# -*- coding: utf-8 -*-s
from time import sleep, time
from src.DriverObject import driver_object
from src.PageObject import elements
from src.PageObject import page_element
locators = dict(
                btn_login="div.login a.personal-link",
                ipad_btn_login="div#mobileheader a.personal-link",
                mobile_menu_login="div.mobileheader-v1 div.burger",
                mobile_btn_login="div.mobilemenu-v1 a[href*=personal]",
                aria_login="input[name=USER_LOGIN]"
)

def open_log_on(Test):
    el = elements(Test, locators)
    for a in range(10):
        Test.get("https://confaelshop.ru")
        Test.doc_readyState()
        Test.ajax_complite
        if Test.browser_name in ("ie", "c", "ff"):
            el.btn_login.click(pause=1)
        elif Test.browser_name == "ipad":
            el.ipad_btn_login.click(pause=1)
        else:
            print("open menu")
            menu = el.mobile_menu_login
            if "c" not in menu.get_attribute("class"):
                menu.click(pause=1)
            if "c" in menu.get_attribute("class"):
                el.mobile_btn_login.click(pause=1)
            Test.doc_readyState()
            Test.ajax_complite
        flag = el.aria_login.in_page()
        if not flag:
            continue
        return flag
def log_on(Test, login, check=True):
    """locator"""
    _locators = dict(
                        aria_name="input[name=USER_LOGIN]",
                        aria_password="input[name=USER_PASSWORD]",
                        sumbit="button[name=Login]",
                        error="label.error",
                        alert="div.alert",
                        )
    """locator"""
    values = {
              "None": {
                     "name": "",
                     "password": "",
                     "count": 2
                      },
              "False": {
                     "name": "Тестирование !@#$%^&*(123",
                     "password": "Тестирование !@#$%^&*(123",
                      "count": 1
                      },
              "user_one": {
                        "name": "test768578@gmail.com",
                        "password": "982413",
                          },
              "user_two": {
                        "name": "al8594212@gmail.com",
                        "password": "12345678",
                          },
              "user_two_alt_password": {
                        "name": "al8594212@gmail.com",
                        "password": "87654321",
                          },
              "user_tree": {
                          "name": "Test@One.asd",
                          "password": "tt9KwFbjpiuMc4D",
                          },
              "user_tree_alt_login": {
                                      "name": "Test@two.asd",
                                      "password": "tt9KwFbjpiuMc4D",
                                        },
              "user_four": {
                            "name": "ali@nkhstudio.ru",
                            "password": "19e7529c",
                             },
              "TestSite": {
                            "name": "dbo@imaginweb.ru",
                            "password": "Gfdrse234!q",
                             }
            }
    el = elements(Test, _locators)
    open_log_on(Test)
    el.aria_name.double_click()
    el.aria_name.input(values[login]["name"])
    el.aria_password.double_click()
    el.aria_password.input(values[login]["password"])
    el.sumbit.click()
    if login == "None":
        Errors = page_element(Test, _locators["error"], scroll=False, message=f'Ошибка не найден элемент {_locators["error"]}', wait=15)
        if Errors.in_page():
            count_error = len(Errors.return_all())
            print(f'колво ошибок не совпадает с заданным {count_error} / {values[login]["count"]}')
            return count_error == values[login]["count"]
        print("не корректный ввод не верен")
        return False
    elif login == "False":
        Errors = page_element(Test, _locators["error"], scroll=False, message=f'Ошибка не найден элемент {_locators["error"]}', wait=15)
        alert = page_element(Test, _locators["alert"], scroll=False, message=f'Ошибка не найден элемент {_locators["alert"]}', wait=15)
        if Errors.in_page():
            count_error = len(Errors.return_all())
            print(f' {count_error} / {values[login]["count"]}')
            return count_error == values[login]["count"]
        if alert.in_page():
            print("alert check")
            return True
        print("не корректный ввод не верен")
        return False
    else:
        Test.doc_readyState()
        Test.ajax_complite
        Errors = page_element(Test, _locators["error"], scroll=False, message=f'Ошибка не найден элемент {_locators["error"]}', wait=5)
        if Errors.in_page() and check is True:
            print("корректный ввод не верен")
            return False
        return True


def check_results(Test, locator_result, type_input, count):
    if type_input == "None":
        Errors = page_element(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {locator_result}")
        if Errors.in_page():
            return len(Errors.return_all()) == count
        return False
    elif type_input == "False":
        Errors = page_element(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {locator_result}")
        if Errors.in_page():
            return len(Errors.return_all()) == count
        return False
    elif type_input == "True":
        succes = page_element(Test, locator_result, scroll=False, message=f"Ошибка ожидания появления сообщения об успехе формы")
        if succes.in_page():
            return True
        return False

def simple_form(Test, locator_form, values, type_input, sumbit, locator_result, count, debug=False):
    if locator_form is not None:
        page_element(Test, locator_form).visible
    for key, value in values[type_input].items():
        print(key, value)
        page_element(Test, value["locator"], scroll=value.get("scroll", True)).double_click().input("backspase", button=True, check=False).input(value["input_value"], check=value["check"], return_check=value["return_check"])
    if debug:
        sleep(100)
        return True
    page_element(Test, sumbit).click()

    if type_input == "None":
        Errors = page_element(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {locator_result}")
        if Errors.in_page():
            return len(Errors.return_all()) == count
        return False
    elif type_input == "False":
        Errors = page_element(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {locator_result}")
        if Errors.in_page():
            return len(Errors.return_all()) == count
        return False
    elif type_input == "True":
        succes = page_element(Test, locator_result, scroll=False, message=f"Ошибка ожидания появления сообщения об успехе формы")
        if succes.in_page():
            return True
        return False


def dell_pice(Test):
    _locators = dict(price="div.sendsay-popup")
    el = elements(Test, _locators)
    el.price.wait = 5
    try:
        Test.driver.execute_script("""
                              var element = arguments[0];
                              element.parentNode.removeChild(element);
                              """, el.price.find())
    except Exception:
        pass


def go_to_product_page(Test, index=3):
    _locators = dict(product_link="div.block_list div.item-title a")
    Test.get("https://confaelshop.ru/catalog/komu/")
    for a in range(3):
        if Test.driver.current_url != "https://confaelshop.ru/catalog/komu/":
            Test.doc_readyState()
            Test.ajax_complite
            dell_pice(Test)
            break
        elif a > 1:
            Test.get("https://confaelshop.ru/catalog/komu/")
        el = elements(Test, _locators)
        Test.doc_readyState()
        Test.ajax_complite()
        el.product_link.index = index
        el.product_link.click(pause=1)
        if Test.driver.current_url == "https://confaelshop.ru/catalog/komu/":
            try:
                el.product_link.click(pause=1)
            except Exception:
                pass
        sleep(1)
    return Test.driver.current_url != "https://confaelshop.ru/catalog/komu/"
