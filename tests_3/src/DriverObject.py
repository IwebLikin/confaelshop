# -*- coding: utf-8 -*-
#!/usr/bin/python3.6
import sys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.touch_actions import TouchActions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import *
from time import sleep
from src.Support import raindbow, log
import time
import pytest
import datetime
import re

methods = dict(
              xpath=By.XPATH,
              css=By.CSS_SELECTOR,
              name=By.NAME,
              tag_name=By.TAG_NAME,
              class_name=By.CLASS_NAME,
              link_text=By.LINK_TEXT,
              id=By.ID,
              partial_link_text=By.PARTIAL_LINK_TEXT
              )

CAPS = {
        "c": {"browserName": "chrome", "versions": "74.0"},
        "ff": {"browserName": "firefox", "versions": "66.0"},
        "o": {"browserName": "opera", "versions": "60.0"},
        "ie": {"browserName": "internet explorer", "versions": "11"},
        "ipad": {"deviceName": "iPad"},
        "galaxys5": {"deviceName": "Galaxy S5"}}

params = {
          "ipad": {
                   "y": (999, 100)
                  },
          "galaxys5": {
                       "y": (640, 150)
                      },
          "c": {
                "y": (999, 100)
                },
          "ff": {
                 "y": (999, 100)
                },
          "ie": {
                 "y": (999, 100)
                 }
                  }

class driver_object(object):

    def __init__(self, browser_name, check_log_error=True):
        self.browser_name = browser_name
        self.driver = None
        self.check_log_error = check_log_error
        self.run_browser()
        self.actions = None
        self._refresh = None
        self.log = log()
        self.index_repeat = 0
        self.url = self.driver.current_url
        self.time_start = datetime.datetime.now()

    def __str__(self):
        return "<object Test atr: browser_name={} | time_start={}|>".format(self.browser_name,
                                                                                         self.time_start)
    def __enter__(self):
        print(f'{raindbow("yellow", "init")} {raindbow("cyan", self.time)}')
        return self

    def __exit__(self, _type, value, tb):
        import traceback
        self.driver.save_screenshot(f'./screenshots/img.png')
        self.log.info(str(type(_type)) + " ".join(traceback.format_tb(tb)) + str(value))
        print(type, value)
        try:
            print(f'{raindbow("yellow", "close brawser ")}{raindbow("cyan", self.time)}')
            print(type, str(tb), value)
            self.driver.quit()
        except Exception:
            return False

    def ActionChains(self):
        self.actions = ActionChains(self.driver)
        self.actions.reset_actions()
        return self


    def move_to(self, elem, pause=0):
        self.actions.move_to_element(elem).pause(pause)
        return self

    def perform(self):
        self.actions.perform()
        return self


    def send_keys(self, value):
        self.actions.send_keys(value)
        return self

    def double_click(self, elem=None):
        self.actions.double_click(on_element=elem)
        return self

    def click(self, elem):
        self.actions.click(on_element=elem)
        return self


    def pause(self, second):
        self.actions.pause(second)
        return self

    def get(self, link):
        print(f'{raindbow("yellow", "open")} {raindbow("cyan", link)} {raindbow("yellow", "in")} {raindbow("cyan", self.time)}')
        self.log.info(f"open {link}")
        self.driver.get(link)
        self._refresh = str(time.time())
        if self.check_log_error:
            string = f"{self.check_errors_console_log()}"
            if len(string) > 3:
                self.log.warning(string)

    def refresh(self):
        self.driver.refresh()
        self._refresh = str(time.time())
        print(f'{raindbow("yellow", refresh)}refresh {raindbow("cyan", self.time)}')

    @staticmethod
    def re(string, pattern, method="search", _flags=[re.I], repeat=4, pause=2, group=0):
        import traceback
        for a in range(repeat):
            try:
                r = re.compile(f"{pattern}", *_flags)
                if method == "search":
                    r = re.search(r, string)
                    if r is not None:
                        if not isinstance(r, str):
                            return r.group(group)
                elif method == "findall":
                    r = re.findall(r, string)
                if r is not None:
                    return r
            except Exception as e:
                traceback.print_exc(file=sys.stdout)
                print(e, type(e))
                sleep(pause)

    def run_browser(self, window_size=(1920, 1080),
                    selenium_grid_url='http://127.0.0.1:4444/wd/hub'):
        if self.browser_name in ("ie", "c", "ff"):
            capabilities = CAPS[self.browser_name].copy()
            self.driver = webdriver.Remote(desired_capabilities=capabilities,
                                           command_executor=selenium_grid_url)
            print(self.browser_name)
        else:
            mobile_emulation = CAPS[self.browser_name]
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
            self.driver = webdriver.Remote(command_executor=selenium_grid_url,
                                           desired_capabilities=chrome_options.to_capabilities())
            print(f'{raindbow("yellow", "run browser")} {raindbow("cyan", self.time)}')
        self.driver.set_window_size(1920, 1080)
        return (self.driver, self.browser_name)

    def ajax_complite(self):
        try:
            WebDriverWait(self.driver, 60).until(lambda driver: self.driver.execute_script("return jQuery.active == 0"), message='Ошибка ожидание оканчания аджакс')
            print(f'{raindbow("yellow", "Ajax complite in")} {raindbow("cyan", self.time)}')
        except Exception:
            pass

    @staticmethod
    def save_file(file_name, value):
        with open(file_name, "w", encoding="utf-8") as fh:
            fh.write(value)

    @property
    def time(self):
        return time.strftime("%X", time.localtime())

    @property
    def get_browser_name(self):
        return self.driver.capabilities['browserName']

    @property
    def get_version(self):
        if self.get_browser_name == "firefox":
            key = "browserVersion"
        elif self.get_browser_name == "chrome":
            key = "version"
        elif self.get_browser_name == 'internet explorer':
            key = "browserVersion"
        return self.driver.capabilities[key]

    def get_doc(self, id_test, name_test, other):
        if self.browser_name in ("ie", "ff", "c"):
            window_size = "Разрешение: 1920х1080;"
        elif self.browser_name == "ipad":
            window_size = "Разрешение: 768х1024;"
        elif self.browser_name == "galaxys5":
            window_size = "Разрешение: 360х640;"
        print(self.driver.capabilities)
        string = f'ID: {id_test}; Название: {name_test}; {window_size} Браузер: {self.get_browser_name} {self.get_version}; Разное: {other}'
        self.log.info(f"{string}")
        return string
    @staticmethod
    def repeat(func):
        index_repeat = 0
        import traceback
        for a in range(2):
            index_repeat += 1
            try:
                func()
                return True
            except AssertionError as e:
                print(e, type(e))
                if index_repeat < 1:
                    print(f'{raindbow("red", "restart assert Попытка")} {raindbow("cyan", index_repeat)} \n {e}')
                    continue
                assert False
            except Exception as e:
                print(f'{raindbow("red", "restart xfail Попытка")} {raindbow("cyan", index_repeat)} \n {e}')
                print(e, type(e))
        return pytest.xfail()

    def doc_readyState(self):
        WebDriverWait(self.driver, 120).until(lambda x: x.execute_script("return document.readyState") == "complete")
        print(f'{raindbow("yellow", "Page ready  State complete ")}{raindbow("cyan", self.time)}')

    def check_errors_console_log(self):
        "Function to get the browser's console log errors"
        current_console_log_errors = []
        # As IE driver does not support retrieval of any logs,
        # we are bypassing the get_browser_console_log() method
        if self.browser_name == "c":
            log_errors = []
            new_errors = []
            log = self.get_browser_console_log()
            #print("Console Log: ", log)
            for entry in log:
                if entry['level'] == 'SEVERE':
                    log_errors.append(entry['message'])

            if current_console_log_errors != log_errors:
                # Find the difference
                new_errors = list(set(log_errors) - set(current_console_log_errors))
                # Set current_console_log_errors = log_errors
                current_console_log_errors = log_errors

            if len(new_errors) > 0:
                return ("Browser console error on url: %s\nConsole error(s):%s" % (self.driver.current_url, '\n----'.join(new_errors)))
        return ""
        
    def get_browser_console_log(self):
        "Get the browser console log"
        try:
            log = self.driver.get_log('browser')
            return log
        except Exception as e:
            print("Exception when reading Browser Console log")
            print(str(e))
