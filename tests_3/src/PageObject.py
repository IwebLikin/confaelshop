# -*- coding: utf-8 -*-
import sys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import *
from src.Support import raindbow, log
from time import sleep, time
import re

methods = dict(
              xpath=By.XPATH,
              css=By.CSS_SELECTOR,
              name=By.NAME,
              tag_name=By.TAG_NAME,
              class_name=By.CLASS_NAME,
              link_text=By.LINK_TEXT,
              id=By.ID,
              partial_link_text=By.PARTIAL_LINK_TEXT
              )
_button = dict(
             backspase=Keys.BACK_SPACE,
             enter=Keys.ENTER
                )
params = {
          "ipad": {
                   "y": (999, 100)
                  },
          "galaxys5": {
                       "y": (640, 150)
                      },
          "c": {
                "y": (999, 100)
                },
          "ff": {
                 "y": (999, 100)
                },
          "ie": {
                 "y": (999, 100)
                 }
                  }


class page_element(object):

    def __init__(self, Test, locator, type_locator="css", pause=[0, 0.5, 2], repeat=4, index=0, scroll=True, actions=[],
                 message="", wait=30, downoffset=200, name=""):
        self.locator = locator
        self.type_locator = self.get_locator_type(type_locator)
        self.Test = Test
        self.pause = pause
        self.repeat = repeat
        self.index = index
        self.__scroll = scroll
        self.message = message
        self.name = name
        self.wait = wait
        self.downoffset = downoffset
        self.location = {"y": None, "x": None, "link": "", "refresh_id": ""}

    def __str__(self):
        return f'Шаг: {raindbow("yellow", self.message)}; элемент: {raindbow("cyan", self.name)} локатор :{raindbow("yellow", self.locator)}'

    def _print(self, message):
        if self.Test.url != self.Test.driver.current_url:
            self.Test.url = self.Test.driver.current_url
            string = f"Page link {self.Test.driver.current_url}"
            self.Test.log.info(string)
        string = f'{message},  элемент: {self.name}; локатор: {self.locator}; index: {self.index} '
        self.Test.log.info(string)
        if self.Test.check_log_error:
            string = f"{self.Test.check_errors_console_log()}"
            if len(string) > 3:
                self.Test.log.warning(string)
        return f'{raindbow("yellow", message)} элемент: {raindbow("cyan", self.name)}; локатор: {raindbow("cyan", self.locator)} тип: {raindbow("cyan", self.type_locator)} index {raindbow("cyan", self.index)} in {raindbow("cyan", self._time)}'

    def get_locator_type(self, type_locator):
        if "//" in self.locator:
            return "xpath"
        else:
            return methods[type_locator]

    def find(self, message="find to "):
        type_locator = self.type_locator
        if __debug__:
            print(self._print(message))
        string = f'{raindbow("red", "Ошибка поиска:")}  Время ожидания элемента {raindbow("cyan", self.name)} вышло. {self.message}'
        wait = WebDriverWait(self.Test.driver, self.wait, ignored_exceptions=[StaleElementReferenceException])
        return wait.until(lambda x: x.find_elements(type_locator, self.locator), message=string)[self.index]

    def in_page(self):
        for a in range(2):
            try:
                if __debug__:
                    print(self._print(message="Check in_page локатор: "))
                if self.Test.driver.find_element(self.type_locator, self.locator) is not None:
                    return True
            except NoSuchElementException:
                pass
            except Exception as e:
                print(f'{raindbow("red", "method In_page error")}', e, type(e))
        else:
            return False

    def return_all(self, message="find all to"):
        if __debug__:
            print(self._print(message=message))
        string = f'{raindbow("red", "Ошибка поиска:")}  Время ожидания элемента вышло. {self.message}'
        wait = WebDriverWait(self.Test.driver, self.wait)
        return wait.until(lambda x: x.find_elements(self.type_locator, self.locator), message=string)

    def input(self, value, clear=True, check=True, action=None, wait=2, return_check=False, button=False):
        if button:
            value = _button[value]
        sleep(self.pause[0])
        for a in range(self.repeat):
            try:
                self.scroll()
            except Exception:
                self.custom_scroll()
            if clear:
                self.find().clear()
            if self.Test.browser_name in ("ie", "c", "ff") and action == "move":
                try:
                    self.Test.ActionChains().move_to(self.find(message="find to input ")).pause(0.5).send_keys(value).perform()
                except MoveTargetOutOfBoundsException as e:
                    loc = self._outofBouns(e)
                    print(f'{raindbow("yellow", loc)} MoveTargetOutOfBoundsException')
                    self.scroll_offset(loc[1], loc[0])
            elif self.Test.browser_name in ("ie", "c", "ff") and action == "click":
                try:
                    self.Test.ActionChains().click(self.find(message="find to input ")).pause(1).send_keys(value).perform()
                except MoveTargetOutOfBoundsException as e:
                    loc = self._outofBouns(e)
                    print(f'{raindbow("yellow", loc)} MoveTargetOutOfBoundsException')
                    self.scroll_offset(loc[1], loc[0])
            else:
                self.find(message="find to input ").send_keys(value)
            if __debug__:
                print(self._print(message="input to "))
            if check:
                atr_value = self.get_attribute("value")
                if str(atr_value) != str(value):
                    sleep(wait)
                    if __debug__:
                        print(f'Ошибка ввода: {value} != {atr_value}')
                    continue
                else:
                    if return_check:
                        return True
                    return self
                if return_check:
                    return False
                return self
            else:
                return self
        if return_check:
            return False
        return self

    def click(self, pause=None):
        sleep(self.pause[0])
        for a in range(self.repeat):
            try:
                self.scroll()
            except Exception:
                self.custom_scroll()

            if self.Test.browser_name in ("ie", "c", "ff"):
                try:
                    string = f'find to click ActionChains пауза ={pause}'
                    if pause:
                        ActionChains(self.Test.driver).move_to_element(self.find(message=string)).pause(pause)\
                            .click(on_element=self.find(message=string)).perform()
                    else:
                        ActionChains(self.Test.driver).click(on_element=self.find(message=string)).perform()
                    if __debug__:
                        print(self._print(message="click to "))
                    break
                except StaleElementReferenceException:
                    continue
                    sleep(0.5)
                    print(f'{raindbow("red", "error")}click  StaleElementReferenceException')
            else:
                try:
                    self.find(message=f"find to click").click()
                    if __debug__:
                        print(self._print(message="click to "))
                    break
                except StaleElementReferenceException:
                    continue
                    sleep(0.5)
                    print(f'{raindbow("red", "error")}click  StaleElementReferenceException')
        sleep(self.pause[2])
        return self

    def double_click(self):
        sleep(self.pause[0])
        for a in range(self.repeat):
            try:
                self.scroll()
            except Exception:
                self.custom_scroll()

            if self.Test.browser_name in ("ie", "c", "ff"):
                try:
                    string = f'find to double click ActionChains'
                    ActionChains(self.Test.driver).double_click(on_element=self.find(message=string)).perform()
                    if __debug__:
                        print(self._print(message="double click to "))
                    break
                except StaleElementReferenceException:
                    sleep(0.5)
                    continue
                    print(f'{raindbow("red", "error")}double click  StaleElementReferenceException')
            else:
                try:
                    self.find(message=f"find to click").click()
                    sleep(0.2)
                    self.find(message=f"find to click").click()
                    if __debug__:
                        print(self._print(message="double click to "))
                    break
                except StaleElementReferenceException:
                    continue
                    sleep(0.5)
                    print(f'{raindbow("red", "error")}click  StaleElementReferenceException')
        sleep(self.pause[2])
        return self

    def check(self, value, method="text", attribute=None):
        if method == "text":
            print(type(self.text), type(value), self.text == value, f"check{self.text}", f"value {value}")
            try:
                return WebDriverWait(self, self.wait).until(lambda x: str(x.text) == value, message='Error text != value')
            except Exception:
                return False

    def scroll(self, _y=0, _x=0):
        self.Test.ajax_complite()
        if self.__scroll:
            client_center = params[self.Test.browser_name]["y"][0] / 2
            if self.location["link"] != self.Test.driver.current_url or self.Test._refresh != self.location["refresh_id"]:
                print("сброс позиции")
                self.location["link"] = self.Test.driver.current_url
                self.reset_loc()
            loc = self.getBoundingClientRect
            if loc is None:
                self.custom_scroll()
                return self
            yoffset = loc["top"] + (loc["height"] / 2)
            _y = yoffset - client_center
            if yoffset < 200 or yoffset > params[self.Test.browser_name]["y"][0] - 200:
                if _y > 0:
                    _y += 100
                elif _y < 0:
                    _y -= 100
                self._scroll(y=_y, x=_x)
                if __debug__:
                    print(self._print(message="scroll to "))
        return self

    def custom_scroll(self):
        self.Test.ajax_complite()
        if self.__scroll:
            if self.location["link"] != self.Test.driver.current_url or self.Test._refresh != self.location["refresh_id"]:
                print("сброс позиции")
                self.location["link"] = self.Test.driver.current_url
                self.reset_loc()
        self.Test.driver.execute_script("arguments[0].scrollIntoView();", self.find())
        if __debug__:
            print(self._print(message="custom_scroll to "))
        self._scroll(-200)
        return self

    def scroll_offset(self, y=0, x=0):
        if self.__scroll:
            if self.location["link"] != self.Test.driver.current_url or self.Test._refresh != self.location["refresh_id"]:
                print("сброс позиции")
                self.location["link"] = self.Test.driver.current_url
                self.reset_loc()
            _y, _x = int(y), int(x)
            if _y < 0:
                _y -= 200
            elif _y > 0:
                _y += 200
            if _x > 0:
                _x += 100
            elif _x < 0:
                _x -= 100
            self._scroll(y=_y, x=_x)
        return self

    def _scroll(self, y, x=0):
        self.location["y"] += int(y)
        self.location["x"] += int(x)
        self.Test.driver.execute_script("window.scrollBy({}, {})".format(x, y))
        return self

    def reset_loc(self):
        self.location["y"] = params[self.Test.browser_name]["y"][0] / 2
        self.location["x"] = 0
        self.Test._refresh = str(time())
        self.location["refresh_id"] = str(self.Test._refresh )
        print(self.location)
        return self

    def get_attribute(self, name_attribute):
        for a in range(8):
            try:
                return self.find(message=f'find to get_attribute {name_attribute} элемента').get_attribute(name_attribute)
            except StaleElementReferenceException:
                sleep(0.4)
                print(f'{raindbow("red", "error")}find to get text  StaleElementReferenceException')

    def _outofBouns(self, messege_error):
        r = re.compile(r"Message: \((\S+), (\S+)\)")
        r = re.search(r, str(messege_error))
        return (r.group(1), r.group(2))

    def print_time(self):
        print(f"{self.Test.time}")
        return self

    @property
    def text(self):
        for a in range(8):
            try:
                return self.find(message=f"find to get text ").text
            except StaleElementReferenceException:
                sleep(0.5)
                print(f'{raindbow("red", "error")}find to get text  StaleElementReferenceException')
    @property
    def invisible(self):
        return WebDriverWait(self.Test.driver, self.wait).until(EC.invisibility_of_element_located((self.type_locator, self.locator)), message=f'Ошибка Проверки на отсутствие элемента: Время ожидания элемента {raindbow("cyan", self.locator)}  вышло. {self.message}')

    @property
    def visible(self):
        return not WebDriverWait(self.Test.driver, self.wait).until_not(EC.invisibility_of_element_located((self.type_locator, self.locator)), message=f'Ошибка Проверки на присутствие элемента: Время ожидания элемента {raindbow("cyan", self.locator)}  вышло. {self.message}')

    def is_displayed(self, _except="True"):
        if _except == "True":
            return WebDriverWait(self, self.wait).until(lambda x: x.find(message=f'find to is_displayed {_except}').is_displayed(), message=f'{raindbow("red", "Error")} is_displayed: Время ожидания элемента {raindbow("cyan", self.locator)}  вышло. {self.message}')
        elif _except == "False":
            return WebDriverWait(self, self.wait).until_not(lambda x: x.find(message=f'find to is_displayed {_except}').is_displayed(), message=f'{raindbow("red", "Error")} is_displayed: Время ожидания элемента {raindbow("cyan", self.locator)}  вышло. {self.message}')

    @property
    def getBoundingClientRect(self):
        try:
            return self.Test.driver.execute_script("return arguments[0].getBoundingClientRect();", self.find(message="find to getBoundingClientRect"))
        except Exception as e:
            print(e, type(e))


    @property
    def _time(self):
        return f'{raindbow("cyan", self.Test.time)}'


class elements(object):
    def __init__(self, Test, locators):
        self.Test = Test
        self.elements = {key: page_element(Test, value, name=key) for key, value in locators.items()}

    def __getattr__(self, name):
        return self.elements[name]

    def __str__(self):
        string = f'Элементов {raindbow("green", len(self.elements))} \n'
        return string + "\n".join(f'{raindbow("white", "Елемент")}  {raindbow("green", "key")} '
                                  f'{raindbow("white", "Локатор")} '
                                  f'{raindbow("yellow", value.locator)} '
                                  for key, value in self.elements.items())
