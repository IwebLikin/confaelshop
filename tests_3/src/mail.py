import imaplib
import email
import sys
from time import sleep
import datetime
def get_part_info(part):
    """Получить текст сообщения в правильной кодировке.

    Параметры:
      - part: часть сообщения email.Message.

    Результат:
      - message (str): сообщение;
      - encoding (str): кодировка сообщения;
      - mime (str): MIME-тип.

    """
    encoding = part.get_content_charset()
    # Если кодировку определить не удалось, используется по умолчанию
    if not encoding:
        encoding = sys.stdout.encoding
    mime = part.get_content_type()
    message = part.get_payload(decode=True).decode(encoding, errors="ignore").strip()

    return message, encoding, mime


def get_message_info(message):
    """Получить текст сообщения в правильной кодировке.

    Параметры:
      - message: сообщение email.Message.

    Результат:
      - message (str): сообщение или строка "Нет тела сообщения";
      - encoding (str): кодировка сообщения или "-";
      - mime (str): MIME-тип или "-".

    """
    message_text, encoding, mime = "Нет тела сообщения", "-", "-"

    if message.is_multipart():
        for part in message.walk():
            if part.get_content_type() in ("text/html", "text/plain"):
                message_text, encoding, mime = get_part_info(part)
                break  # Только первое вхождение
    else:
        message_text, encoding, mime = get_part_info(message)

    return message_text, encoding, mime


def get_mail():
    result = []
    server = 'imap.gmail.com'
    port = "993"
    mailserver = imaplib.IMAP4_SSL(server, port)
    mailserver.login('al8594212@gmail.com', "QLTj698rm6T6BVz")
    mailserver.select()
    response, messages_nums = mailserver.search(None, "ALL")
    if response != "OK":
        raise imaplib.IMAP4.error("Не удалось получить список писем.")
    messages_nums = messages_nums[0].split()
    for message_num in reversed(messages_nums[-3:]):
        # message_parts = "(RFC822)" - получение письма целиком
        response, data = mailserver.fetch(message_num,
                                          message_parts="(RFC822)")
        message_num = int(message_num.decode())
        if response != "OK":
            print("Не удалось получить письмо №", message_num)
            continue
        raw_message = data[0][1]
        message = email.message_from_bytes(raw_message)
        # Получение текста письма
        text, encoding, mime = get_message_info(message)
        result.append(text)
    mailserver.logout()
    return result


def log_on_mail():
    server = 'imap.gmail.com'
    port = "993"
    mailserver = imaplib.IMAP4_SSL(server, port)
    mailserver.login('al8594212@gmail.com', "QLTj698rm6T6BVz")
    mailserver.select()
    return mailserver


def get_mail_after_time(time_start):
    result = []
    for a in range(15):
        mailserver = log_on_mail()
        response, messages_nums = mailserver.search(None, "ALL", 'FROM', 'podarok@confaelshop.ru')
        if response != "OK":
            raise imaplib.IMAP4.error("Не удалось получить список писем.")
        messages_nums = messages_nums[0].split()
        for message_num in reversed(messages_nums[-3:]):
            # message_parts = "(RFC822)" - получение письма целиком
            response, data = mailserver.fetch(message_num,
                                              message_parts="(RFC822)")
            message_num = int(message_num.decode())
            if response != "OK":
                print("Не удалось получить письмо №", message_num)
                continue
            raw_message = data[0][1]
            raw_email_string = raw_message.decode('utf-8')
            email_message = email.message_from_string(raw_email_string)

            # Header Details
            date_tuple = email.utils.parsedate_tz(email_message['Date'])
            if date_tuple:
                temp = []
                for a in date_tuple:
                    temp.append(int(a))
                delta = datetime.datetime(*date_tuple[0:6]) - time_start
                print(datetime.datetime(*date_tuple[0:6]))
                print(time_start)
                print(float(str(delta.total_seconds())), float(str(delta.total_seconds())) > 0, '\n')
                if float(str(delta.total_seconds())) > 0:
                    mailserver.logout()
                    return True
        else:
            mailserver.logout()
            sleep(5)
    else:
        mailserver.logout()
        return False
