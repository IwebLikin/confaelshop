import sys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.touch_actions import TouchActions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import *
from selenium.webdriver.support.color import Color
from time import sleep
import traceback
import re


CAPS = {
        "ff": DesiredCapabilities.FIREFOX,
        "c": DesiredCapabilities.CHROME,
        "ie": DesiredCapabilities.INTERNETEXPLORER,
        "ipad": {
                "deviceName": "iPad"
                },
        "galaxys5": {
                    "deviceName": "Galaxy S5"
                    }
       }

params = {
          "ipad": {
                   "y": (999, 100)
                  },
          "galaxys5": {
                       "y": (640, 150)
                      },
          "c": {
                "y": (999, 100)
                },
          "ff": {
                 "y": (999, 100)
                },
          "ie": {
                 "y": (999, 100)
                 }
                  }

input_value = dict(
                    true=dict(
                             login="al8594212@gmail.com",
                             password="H6eLdspGkf55WcP"
                             ),
                    false=dict(
                               login="test 12345 !@#$%^&*()",
                               password="test 12345 !@#$%^&*()"
                              )
                   )
class Test(object):

    def __init__(self, browser_name, run=True):
        self.browser_name = browser_name
        self.xpaths = []
        self.driver = None
        if run:
            self.run_browser()

    def __str__(self):
        return "<object Test atr: browser_name={} | driver={} | xpaths_count={}>".format(self.browser_name,
                                                                                         self.driver,
                                                                                         len(self.xpaths))

    @classmethod
    def input_(cls, driver, xpath, value, browser_name, clear=True, alt_clear=False, click=False, double=False, index_=0, pause_after=0, move=True, check=True, scroll=True, downoffset=200):
        for a in range(6):
            try:
                if scroll:
                    Test.scroll_(driver, xpath, browser_name, index_=index_, downoffset=downoffset)
                elem = driver.find_elements_by_xpath(xpath)[index_]
                if click:
                    Test.click(driver, xpath, browser_name, index_=index_, double=double, move=move, downoffset=downoffset, scroll=scroll)
                if clear:
                    elem.clear()
                if alt_clear:
                    elem = driver.find_elements_by_xpath(xpath)[index_]
                    print(elem.text)
                    for a in elem.get_attribute("value"):
                        elem.send_keys(Keys.BACKSPACE)
                elem = driver.find_elements_by_xpath(xpath)[index_]
                if browser_name in ("ie", "c", "ff"):
                    ActionChains(driver).move_to_element(elem).pause(0.5).send_keys(value).perform()
                else:
                    elem.send_keys(value)
                sleep(pause_after)
                if check:
                    if str(elem.get_attribute("value")) != str(value):
                        print("Ввод не верный {} не равно {}".format(elem.get_attribute("value"), value))
                        sleep(3)
                        continue
            except StaleElementReferenceException:
                sleep(1)

            except MoveTargetOutOfBoundsException as e:
                loc = Test._outofBouns(e)
                Test.scroll_offset(driver, browser_name, loc[1], loc[0])
            else:
                break

    @classmethod
    def click(cls, driver, xpath, browser_name, pause=0, pause_after=2, double=False, index_=0, move=True, scroll=True, downoffset=200, repeat=4, smoke_test={"link": None, "elem": None}, except_result=None, message=""):
        for a in range(repeat):
            try:
                if smoke_test["link"] is not None:
                    "   Проверка на соответствие страницы"
                    if driver.current_url != smoke_test["link"]:
                        print("Ошибка проверки окружения, неверная страница")
                        return False
                if smoke_test["elem"] is not None:
                    flag = not WebDriverWait(driver, pause_after).until_not(EC.invisibility_of_element_located((By.XPATH, smoke_test["elem"])), message=f'Ошибка: {message}')
                    print(f'  {message} Проверка присутствует ли элемент {smoke_test["elem"]} {flag}')
                    if not flag:
                        assert flag, f'{message} Ошибка проверки окружения отсутствует элемент {smoke_test["elem"]}'
                if scroll:
                    Test.scroll_(driver, xpath, browser_name, pause=0, index_=index_, downoffset=downoffset)
                elem = driver.find_elements_by_xpath(xpath)[index_]
                if browser_name in ("ie", "c", "ff"):
                    if move:
                        ActionChains(driver).move_to_element(elem).pause(pause).perform()
                    elem = driver.find_elements_by_xpath(xpath)[index_]
                    if double:
                        ActionChains(driver).double_click(on_element=elem).perform()
                    else:
                        ActionChains(driver).click(on_element=elem).perform()
                else:
                    elem = driver.find_elements_by_xpath(xpath)[index_]
                    elem.click()
                if except_result is not None:
                    if except_result[0]:
                        print(f"  {message}Ожидание результата присутствует ли элемент {except_result[1]} \r ")
                        flag = not WebDriverWait(driver, pause_after).until_not(EC.invisibility_of_element_located((By.XPATH, except_result[1])), message=f'Ошибка: {message}')
                        if flag:
                            print("успешна")
                            return True
                        else:
                            sleep(1)
                            continue
                    else:
                        print(f" {message} Ожидание результата отствует ли элемент {except_result[1]} \r ")
                        flag = WebDriverWait(driver, pause_after).until(EC.invisibility_of_element((By.XPATH, except_result[1])), message=f'Ошибка: {message}')
                        if flag:
                            print("успешна")
                            return True
                        else:
                            sleep(1)
                            continue
                else:
                    sleep(pause_after)
                    break
            except StaleElementReferenceException as e:
                print(e, type(e))
            except MoveTargetOutOfBoundsException as e:
                loc = Test._outofBouns(e)
                Test.scroll_offset(driver, browser_name, loc[1], loc[0])
            except Exception as e:
                print(e, type(e))
                sleep(pause)
                Test.scroll_(driver, xpath, browser_name, index_=index_, downoffset=downoffset)

    @classmethod
    def move_to_and_scroll(cls, driver, xpath, xpath2, browser_name, pause=0, pause_after=0, index_1=0, index_2=0, downoffset=200):
        Test.scroll_(driver, xpath, browser_name, pause=0, index_=index_1)
        for a in range(4):
            try:
                elem = driver.find_elements_by_xpath(xpath)[index_1]
                ActionChains(driver).move_to_element(elem).pause(pause).perform()
                Test.scroll_(driver, xpath2, browser_name, index_2, downoffset=downoffset)
                sleep(pause_after)
                break
            except StaleElementReferenceException as e:
                print(e, type(e))
                sleep(1)
            except MoveTargetOutOfBoundsException as e:
                loc = Test._outofBouns(e)
                Test.scroll_offset(driver, browser_name, loc[1], loc[0])
            except Exception as e:
                print(e, type(e))
                Test.scroll_(driver, xpath, browser_name, index_1, downoffset=downoffset)

    @classmethod
    def move_to_and_click(cls, driver, xpath, xpath2, browser_name, pause=0, pause_after=0, double=False, index_1=0, index_2=0, scroll=True, downoffset=200):
        if scroll:
            Test.scroll_(driver, xpath, browser_name, pause=0, index_=index_1, downoffset=downoffset)
        for a in range(4):
            try:
                elem = driver.find_elements_by_xpath(xpath)[index_1]
                elem2 = driver.find_elements_by_xpath(xpath2)[index_2]
                if browser_name in ("ie", "c", "ff"):
                    if double:
                        ActionChains(driver).move_to_element(elem).pause(pause).double_click(on_element=elem2).perform()
                    else:
                        ActionChains(driver).move_to_element(elem).pause(pause).click(on_element=elem2).perform()
                else:
                    elem.click()
                sleep(pause_after)
                break
            except StaleElementReferenceException as e:
                print(e, type(e))
                sleep(1)
            except MoveTargetOutOfBoundsException as e:
                loc = Test._outofBouns(e)
                Test.scroll_offset(driver, browser_name, loc[1], loc[0])
            except Exception as e:
                print(e, type(e))
                Test.scroll_(driver, xpath, browser_name, index_1, downoffset=downoffset)

    @classmethod
    def _outofBouns(cls, messege_error):
        r = re.compile(r"Message: \((\S+), (\S+)\)")
        r = re.search(r, str(messege_error))
        return (r.group(1), r.group(2))

    @staticmethod
    def reset_loc(driver, browser_name):
        print(Test._scroll.__dict__)
        Test._scroll.y = params[browser_name]["y"][0] / 2
        Test._scroll.x = 0

    def get(self, link):
        self.driver.get(link)
        self.driver.implicitly_wait(10)

    @staticmethod
    def _scroll(driver, y, x=0):
        Test._scroll.y += int(y)
        Test._scroll.x += int(x)
        driver.execute_script("window.scrollBy({}, {})".format(x, y))

    @staticmethod
    def scroll_offset(driver, browser_name, y=0, x=0):
        if Test._scroll.__dict__.get("link", "") != driver.current_url:
            print("сброс позиции")
            Test._scroll.link = driver.current_url
            Test.reset_loc(driver, browser_name)
        _y, _x = int(y), int(x)
        if _y < 0:
            _y -= 100
        elif _y > 100:
            _y += 100
        if _x > 100:
            _x += 100
        elif _x < 100:
            _x -= 100
        Test._scroll(driver, y=_y, x=_x)

    @staticmethod
    def scroll_(driver, xpath, browser_name, index_=0, pause=0, downoffset=200):
        if Test._scroll.__dict__.get("link", "") != driver.current_url:
            print("сброс позиции")
            Test._scroll.link = driver.current_url
            Test.reset_loc(driver, browser_name)
        elem = driver.find_elements_by_xpath(xpath)
        if len(elem) == 0:
            print("Не найден элемент по xpath", xpath)

        elem = elem[index_]
        loc_ = elem.location
        loc = {"y": loc_["y"]}
        for a in range(5):
            skyline = dict(
                            up=Test._scroll.y - params[browser_name]["y"][0] / 2,
                            down=Test._scroll.y + params[browser_name]["y"][0] / 2
                            )
            elem = driver.find_elements_by_xpath(xpath)[index_]
            yoffset = 0
            if loc["y"] > skyline["down"] - downoffset:
                print("Вниз")
                yoffset = 100
                if loc["y"] - Test._scroll.y > 2000:
                    yoffset = loc["y"] - Test._scroll.y + 200
            elif loc["y"] < skyline["up"] + downoffset:
                print("вверх")
                yoffset = - 100
                if loc["y"] > 0:
                    yoffset = loc["y"] - Test._scroll.y - 200
            else:
                sleep(pause)
                break
            print("смещение ", yoffset)
            print("координата верт. элем ", loc["y"])
            print(Test._scroll.y, "Сохраненая координата", "Границы", skyline)
            Test._scroll(driver, yoffset)

    @classmethod
    def check_error(cls, driver, xpath, count_error, error="error"):
        if error == "error":
            error = xpath["error"]
        print(error)
        for a in range(2):
            try:
                flag_ = driver.find_elements_by_xpath(error)
                print(len(flag_), [a.text for a in flag_])
                if len(flag_) >= count_error:
                    sleep(2)
                    raise Exception
                return ["Наличие сообщений c ошибками", True]
            except Exception as e:
                sleep(2)
                traceback.print_exc(file=sys.stdout)
                print(e, type(e), "\n", "Ошибка поиска сообщений ошибок на странице сайта")
        else:
            return ["False сообщений с ошибками {} из {}".format(len(flag_), count_error), False]

    @classmethod
    def check_succes(cls, driver, xpath=None, string=(None, None)):
        for a in range(1):
            flag_ = False
            try:
                if xpath is not None:
                    flag_ = driver.find_element_by_xpath(xpath)
                    if flag_:
                        flag_ = True
                        return ["Наличие сообщения об отправки формы", flag_]
                elif string[0] is not None:
                    return string[0] == string[1]
            except Exception as e:
                print(e, type(e), "\n", "Ошибка поиска подтверждения")
                sleep(4)
        else:
            raise "Ошибка поиска подтверждения отправки формы"

    def refresh(self):
        self.driver.refresh()
        self.driver.implicitly_wait(10)
        Test.reset_loc(self.driver, self.browser_name)

    @staticmethod
    def re(string, pattern, method="search", flags=[re.I], repeat=4, pause=2):
        for a in range(repeat):
            try:
                r = re.compile(r"{}".format(pattern))
                if method == "search":
                    r = re.search(r, string)
                elif method == "findall":
                    r = re.findall(r, string)
                if r is not None:
                    return r
            except Exception as e:
                print(e, type(e))
                sleep(pause)

    def find(self, locator, method="xpath", repeat=2, pause=1, index_=0, return_text=False, return_atr=False, infisible=False):
        methods = dict(
                      xpath=By.XPATH,
                      css=By.CSS_SELECTOR,
                      name=By.NAME,
                      tag_name=By.TAG_NAME,
                      class_name=By.CLASS_NAME,
                      link_text=By.LINK_TEXT,
                      id=By.ID,
                      partial_link_text=By.PARTIAL_LINK_TEXT
                      )
        if infisible:
            return not WebDriverWait(self.driver, 10).until_not(EC.invisibility_of_element_located((By.XPATH, locator)))
        for a in range(repeat):
            try:
                if index_ == "all":
                    return self.driver.find_elements(methods[method], locator)
                else:
                    if return_text:
                        return self.driver.find_elements(methods[method], locator)[index_].text
                    if return_atr:
                        return self.driver.find_elements(methods[method], locator)[index_].get_attribute(return_atr)
                    return self.driver.find_elements(methods[method], locator)[index_]
            except Exception as e:
                sleep(pause)
                print("Ошибка поиска элемента, попытка {}/{}".format(a, repeat - 1), e, type(e))

    def run_browser(self, window_size=(1920, 1080),
                    selenium_grid_url='http://localhost:4444/wd/hub'):
        if self.browser_name in ("ie", "c", "ff"):
            capabilities = CAPS[self.browser_name].copy()
            self.driver = webdriver.Remote(desired_capabilities=capabilities,
                                           command_executor=selenium_grid_url)
            print(window_size, self.browser_name)
            self.driver.set_window_size(*window_size)
        else:
            mobile_emulation = CAPS[self.browser_name]
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
            self.driver = webdriver.Remote(command_executor=selenium_grid_url,
                                           desired_capabilities=chrome_options.to_capabilities())
            self.driver.implicitly_wait(10)
        return (self.driver, self.browser_name)

    @staticmethod
    def run_browser_(browser_name, window_size=(1920, 1080),
                    selenium_grid_url='http://localhost:4444/wd/hub'):
        if browser_name in ("ie", "c", "ff"):
            capabilities = CAPS[browser_name].copy()
            driver = webdriver.Remote(desired_capabilities=capabilities,
                                           command_executor=selenium_grid_url)
            print(window_size, browser_name)
            driver.set_window_size(*window_size)
        else:
            mobile_emulation = CAPS[browser_name]
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
            driver = webdriver.Remote(command_executor=selenium_grid_url,
                                           desired_capabilities=chrome_options.to_capabilities())
            driver.implicitly_wait(10)
        return (driver, browser_name)

    @staticmethod
    def save_file(file_name, value):
        with open(file_name, "w", encoding="utf-8") as fh:
            fh.write(value)
    def close(self):
        try:
            self.driver.quit()
        except Exception:
            pass
