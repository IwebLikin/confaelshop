from colorama import init, Fore
init(autoreset=True, convert=True)

def raindbow(color, text, end="yellow"):
    colors = dict(
                  yellow=Fore.YELLOW,
                  red=Fore.RED,
                  blue=Fore.BLUE,
                  cyan=Fore.CYAN,
                  white=Fore.WHITE,
                  green=Fore.GREEN
                  )
    return f'{colors[color]}{text}{colors[end]}'

import logging
logger_handler = logging.FileHandler('logging.log', encoding='utf-8')
logger_handler.setLevel(logging.INFO)
# Создайте Formatter для форматирования сообщений в логе
logger_formatter = logging.Formatter('[%(asctime)s | %(levelname)s]: %(message)s')
logger_formatter.datefmt = '%H:%M:%S'
# Добавьте Formatter в обработчик
logger_handler.setFormatter(logger_formatter)

class log:
    def __init__(self):

          # Создайте Logger
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # Создайте обработчик для записи данных в файл
        """console_out  = logging.StreamHandler()
        console_out .setLevel(logging.INFO)"""

        # Добавте обработчик в Logger
        self.logger.addHandler(logger_handler)
    def info(self, text):
        self.logger.info(text)

    def warning(self, text):
        self.logger.warning(text)
