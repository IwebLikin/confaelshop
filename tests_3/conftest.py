
import pytest
import datetime
import openpyxl
import start_test
from time import sleep
import src.DriverObject
from jinja2 import Template, PackageLoader, select_autoescape
from openpyxl import Workbook
from py.xml import html
from collections import OrderedDict
from openpyxl.worksheet import dimensions
from openpyxl.styles import colors, Font, Alignment
_temp = [
         ' <tr><td>Список <b style="color: #a10a0a;">проваленных  </b> тестов: </tr></td>']
report_html = [""]
html_test_case = []
collect_ignore = ["./src/Test.py"]
Project_name = "Конфаэль Шоп"
_time = [0]
def pytest_terminal_summary(terminalreporter, exitstatus, config):
    def get_column_letter(i):
        temp = {
                "1": "A",
                "2": "B",
                "3": "C",
                "4": "D",
                "5": "E",
                "6": "F",
                "7": "G"}
        return temp[str(i)]
    now = datetime.datetime.now()
    now_date = now.strftime("%d_%m_%Y")
    print(start_test.temp)
    string = 'report.xlsx'
    book = openpyxl.load_workbook(string)
    sheet = book["Sheet"]
    count_passed = len(terminalreporter.stats.get('passed', ""))
    count_failed = len(terminalreporter.stats.get('failed', "")) + len(terminalreporter.stats.get('skipped', "")) + len(terminalreporter.stats.get('xfailed', ""))
    count = count_passed + count_failed
    sheet.cell(row=1, column=2).value = "Confael Shop Итоги: "
    cell_ = sheet.cell(row=1, column=3)
    cell_.value = "Всего " + str(count)
    cell_.alignment = Alignment(horizontal='center', vertical='center')
    sheet.cell(row=2, column=2).value = "Успешно завершено: "
    cell_ = sheet.cell(row=2, column=3)
    cell_.value = count_passed
    cell_.alignment = Alignment(horizontal='center', vertical='center')
    cell_.font = Font(size=14, color="00099e13")

    sheet.cell(row=3, column=2).value = "Провалено: "
    cell_ = sheet.cell(row=3, column=3)
    cell_.value = count_failed
    cell_.alignment = Alignment(horizontal='center', vertical='center')
    cell_.font = Font(size=14, color="00a10a0a")
    column_widths = [0, 0, 0]
    for row in sheet.iter_rows():
        for i, cell in enumerate(row):
            try:
                if cell.value is not None:
                    column_widths[i] = max((column_widths[i], len(str(cell.value))))
            except IndexError as e:
                print(e, type(e))
                if cell.value is not None:
                    column_widths.append(len(str(cell.value)))

    for i, column_width in enumerate(column_widths):
        adjusted_width = (column_width + 2) * 1.2
        if i == 1:
            adjusted_width = column_width * 0.9
        sheet.column_dimensions[get_column_letter(i + 1)].width = adjusted_width
        sheet.column_dimensions[get_column_letter(i + 1)].auto_size = True
        sheet.column_dimensions[get_column_letter(i + 1)].bestFit = True
    book.save(string)
    global _temp
    if len(_temp) == 1:
        _temp = ""
    with open("./src/pattern/mail.html", "r", encoding='utf-8') as f:
        template = Template(f.read())

    html_ = template.render(count_pos=count_passed, count_neg=count_failed, error_link=start_test.temp[3][1], all_link=start_test.temp[3][0], all=count, _list="".join(_temp))

    with open("./src/pattern/report.html", "r", encoding='utf-8') as f:
        template = Template(f.read())

    report = template.render(table=report_html[0], count_fail=len(terminalreporter.stats.get('failed', "")),
                    count_xfail=len(terminalreporter.stats.get('xfailed', "")), count_skipped=len(terminalreporter.stats.get('skipped', "")),
                    count_passed=len(terminalreporter.stats.get('passed', "")), count_all_test=count, Project_name=Project_name, second_run=_time[0])
    print(_time[0])
    with open("./result/report.html", "w", encoding='utf-8') as f:
        f.write(report)
    with open("./result/mail.html", "w", encoding='utf-8') as f:
        f.write(html_)
    print(count, type(count))    
    if count > 100:
        import send_mail
        send_mail.send_mail()


def pytest_report_teststatus(report, config):
    print(type(str(report)), str(report))
    if str(report.when) == "call":
        with open("./src/pattern/mail_row.html", "r", encoding='utf-8') as f:
            template = Template(f.read())
        now = datetime.datetime.now()
        now_date = now.strftime("%d_%m_%Y")
        string = 'report.xlsx'
        book = openpyxl.load_workbook(string)
        sheet = book["Sheet"]
        row_ = 4
        while True:
            row_ += 1
            if sheet.cell(row=row_, column=2).value is None or sheet.cell(row=row_, column=2).value == "":
                break
        if str(report.outcome) == "passed":
            sheet.cell(row=row_, column=2).value = str(start_test.temp[0])
            cell_ = sheet.cell(row=row_, column=3)
            cell_.value = "пройден"
            cell_.alignment = Alignment(horizontal='center', vertical='center')
            cell_.font = Font(size=14, color="00099e13")
        elif report.outcome == "failed" or report.outcome == "skipped":
            _temp.append(template.render(text=str(start_test.temp[0])))
            sheet.cell(row=row_, column=2).value = str(start_test.temp[0])
            cell_ = sheet.cell(row=row_, column=3)
            cell_.value = "провален"
            cell_.alignment = Alignment(horizontal='center', vertical='center')
            cell_.font = Font(size=14, color="00a10a0a")
        book.save('report.xlsx')


@pytest.mark.optionalhook
def pytest_html_results_summary(prefix, summary, postfix):
    summary.extend(html.p())
    _time[0] = float(src.DriverObject.driver_object.re(" ".join([str(a) for a in summary]), "in (.+) seconds", group=1)) // 60

@pytest.mark.optionalhook
def pytest_html_results_table_header(cells):
    cells.insert(1, html.th('Name'))
    cells.pop()


@pytest.mark.optionalhook
def pytest_html_results_table_row(report, cells):
    dict_status_ru = dict(
                            Passed="Пройден",
                            Failed="Провален",
                            XFailed="Ошибка",
                            Skipped="Пропущен"
                            )
    cells.insert(1, html.td(report.description))
    status, name_test, program_name, time, t = [src.DriverObject.driver_object.re(str(a), r">(.+)<\/td>", group=1) for a in cells]
    with open("./src/pattern/table_row.html", "r", encoding='utf-8') as f:
        template = Template(f.read().replace("\n", ""))
    string = str(name_test).split(";")
    id = string[0].split(" ")[-1]
    name = ";".join(string[1:])[10:]
    report_html[0] += template.render(status_test_ru=dict_status_ru[status], id_test=id, name_test=name, program_name=program_name, time=time, status_test=status.lower())

@pytest.mark.hookwrapper
def pytest_runtest_makereport(item, call):
    outcome = yield
    report = outcome.get_result()
    report.description = str(start_test.temp[0])
