# -*- coding: utf-8 -*-
#!/usr/bin/python3.6
import os.path

import smtplib
import start_test
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.application import MIMEApplication
import datetime

def send_mail():
    # 1. Данные сервера и письма
    # 'email_from'         : адрес отправителя
    # 'email_to'           : адрес получателя
    # 'email_from_password': пароль для email_from
    email_from = "tester@imaginweb.ru"
    email_to = "ala@nkhstudio.ru" # "al8594212@gmail.com" "puls199@yandex.ru"
    # Для ввода пароля используется функция getpass.getpass,
    # не отображающая ввод
    email_from_password = ">3jEFybD>3jEFybD" # "QLTj698rm6T6BVz"

    # Текст сообщения для отправки в HTML-формате.
    # Возможно использовать и простой текст.
    with open("./result/mail.html", "r", encoding='utf-8') as f:
        string = f.read()
    message_ = string

    # 2. Создание контейнера с содержимым
    msg = MIMEMultipart()
    msg["Subject"] = "Конфаэль автотесты"
    msg["From"] = email_from
    msg["To"] = email_to

    # 2.1. Текст
    # Добавление вложения в контейнер, используя метод attach контейнера.
    # Для MIMEText если вместо HTML используется текст,
    # вторым параметром будет "plain".
    msg.attach(MIMEText(message_, "html"))

    # 2.2. Файл - Рисунок
    """img_filename = input("Имя файла с рисунком: ")
    with open(img_filename, "rb") as image:
        attachment = MIMEImage(image.read())
    # Обозначаем, что это вложение и указываем имя
    attachment.add_header("Content-Disposition", "attachment",
                          filename=os.path.basename(img_filename))
    msg.attach(attachment)"""

    now = datetime.datetime.now()
    now_date = now.strftime("%d_%m_%Y")
    string = '{}.xlsx'.format(start_test.temp[2])
    # 2.3. Файл - Код
    with open('report.xlsx', "rb") as f:
        attachment = MIMEApplication(f.read())
    # Необходимо обозначить, что это вложение и его имя
    attachment.add_header("Content-Disposition", "attachment",
                          filename=string)
    msg.attach(attachment)
    with open("./result/url_broken_links.csv", "rb") as f:
        attachment = MIMEApplication(f.read())
    attachment.add_header("Content-Disposition", "attachment",
                          filename="url_broken_links.csv")
    msg.attach(attachment)
    with open("./result/report.html", "rb") as f:
        attachment = MIMEApplication(f.read())
    attachment.add_header("Content-Disposition", "attachment",
                          filename="report.html")
    msg.attach(attachment)
    # 3. Подключение к серверу и отправка письма
    server = smtplib.SMTP("smtp.gmail.com", 587)
    try:
        # Установление защищенного соединения и авторизация
        server.starttls()
        server.login(email_from, email_from_password)
        # Отправка сообщения
        server.send_message(msg)
        print("Письмо успешно отправлено!")
    except smtplib.SMTPException as err:
        print("Ошибка при отправке письма:", err)
    finally:
        server.quit()
