# -*- coding: utf-8 -*-
#!/usr/bin/python3.6
false_input = "Тестирование !@#$%^&*(123"

true_input = {"name": "Тестирование",
              "name_two": "Test One",
              "name_tree": "Tester Two",
              "tel": "90698989898",
              "tel_two": "9876543211",
              "tel_tree": "7123456789",
              "email_one": "al8594212@gmail.com",
              "email_two": "aasd@asda.sad",
              "email_tree": "Test@One.asd",
              "email_chetiri": "Test@two.asd",
              "password_one": "12345678",
              "password_one_alt": "87654321",
              "password_two": "tt9KwFbjpiuMc4D",
              "password_tree_chetiri": "tt9KwFbjpiuMc4D",
              "aria_adress": "Тестирование",
              "aria_comment": "Много слов "}
fast_view_procedure =\
"""
1)  Открыть страницу  https://confaelshop.ru/catalog/komu/<br>
2)  Нажать на кнопку <Быстрый просмотр> (Кнопка появляется при наведение на блок товара)<br>
3)  Увеличить изображение щелкнув по нему<br>
4)  Закрыть просмотр увеличеного изображения щелкнув по нему<br>
5)  Смена товара влево нажатием на стрелку влево<br>
6)  Ввести в поле кол-ва товара значение > 1<br>
7)  Нажать на кнопку <В корзину><br>
8)  Смена товара вправо нажатием на стрелку вправо<br>
9)  Нажать на кнопку < + ><br>
10) Нажать на кнопку < - ><br>
11) Нажать на кнопку <В корзину><br>
12) Нажать на сердечко (добавление в отложенное)<br>
13) Закрытие окна быстрого просмотра
"""
fast_view_result =\
"""
2)Появление Окна быстрого просмотра<br>
7) Счетчик корзины + 1<br>
11) Счетчик корзины + 1<br>
12) Счетчик корзины -1, счетчик отложенного +1
"""
fast_view = (fast_view_procedure, fast_view_result)

Call_back_None_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Открыть форму быстрого звонка нажатием по <Заказать звонок><br>
3) Очистить поля ввода<br>
4) Отправить форму нажатием на кнопку <Отправить>
"""

Call_back_None_result =\
"""
4) Сообщения об ошибки
"""
Call_back_None = (Call_back_None_procedure, Call_back_None_result)

Call_back_False_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Открыть форму быстрого звонка нажатием по <Заказать звонок><br>
3) Заполнить поля ввода:<br>
<Ваше имя *>:{false_input}<br>
<Телефон *>:{false_input}<br>
4) Отправить форму нажатием на кнопку <Отправить>
"""

Call_back_False_result =\
"""
4) Сообщения об ошибки
"""
Call_back_false = (Call_back_False_procedure, Call_back_False_result)

Call_back_true_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Открыть форму быстрого звонка нажатием по <Заказать звонок><br>
3) Заполнить поля ввода:<br>
<Ваше имя *>: {true_input["name"]}<br>
<Телефон *>: {true_input["tel"]}<br>
4) Отправить форму нажатием на кнопку <Отправить>
"""

Call_back_true_result =\
"""
4) Сообщения об ошибки
"""
Call_back_true = (Call_back_true_procedure, Call_back_true_result)

add_del_basket_procedure =\
"""
1)  Открытие https://confaelshop.ru/catalog/komu/<br>
2)  В поле кол-во первого товара ввести значение > 1<br>
3)  Нажать на кнопку <В корзину>
4)  Закрытие popup со скидкой 500р для хрома, для<br>
    других браузеров удаление из кода страницы<br>
5)  Нажать на кнопку < + ><br>
6)  Нажать на кнопку < - ><br>
7)  Нажать на кнопку < В корзину ><br>
8)  Переход на страницу 3 товара<br>
9)  В поле кол-во товара ввести значение > 1<br>
10) Нажать на кнопку < + ><br>
11) Нажать на кнопку < - ><br>
12) Нажать на кнопку < В корзину ><br>
13) Добавление в отложенное<br>
14) Переход в корзину<br>
15) Удаление крестиком верхнего товара<br>
16) Удаление всех товаров кнопкой <очистить>
"""

add_del_basket_result =\
"""
3)  Счетчик корзины +1<br>
7)  Счетчик корзины +1<br>
12) Счетчик корзины +1<br>
13) Счетчик корзины -1  Счетчик Отложеного +1<br>
10) Товары в корзине соответствуют тем что добавляли<br>
15) Удаляемый товар пропадает из корзины<br>
16) Корзина пустая
"""
add_del_basket = (add_del_basket_procedure, add_del_basket_result)

fast_order_none_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Перейти на страницу товара<br>
3) Открыть форму <Купить в 1 клик> нажав на кнопку <Купить в 1 клик><br>
4) Очистить поля ввода<br>
5) Отправить форму нажав на кнопку <Отправить>
"""

fast_order_none_result =\
"""
5) Сообщения об результате отправки формы
"""
fast_order_none = (fast_order_none_procedure, fast_order_none_result)

fast_order_true_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Перейти на страницу товара<br>
3) Открыть форму <Купить в 1 клик> нажав на кнопку <Купить в 1 клик><br>
4) Заполнить поля ввода:<br>
<Имя *>: {true_input["name"]}<br>
<Телефон *>: {true_input["tel"]}<br>
<E-Mail *>: {true_input["email_one"]}<br>
<Адрес *>: {true_input["aria_adress"]}<br>
<Комментарий к заказу>: {true_input["aria_comment"]}<br>
5) Отправить форму нажав на кнопку <Отправить>
"""

fast_order_true_result =\
"""
5) Сообщения об результате отправки формы
"""
fast_order_true = (fast_order_true_procedure, fast_order_true_result)

fast_order_false_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Перейти на страницу товара<br>
3) Открыть форму <Купить в 1 клик> нажав на кнопку <Купить в 1 клик><br>
4) Заполнить поля ввода:<br>
<Имя *>: {false_input}<br>
<Телефон *>: {false_input}<br>
<E-Mail *>: {false_input}<br>
<Адрес *>: {false_input}<br>
<Комментарий к заказу>: {false_input}<br>
5) Отправить форму нажав на кнопку <Отправить>
"""

fast_order_false_result =\
"""
5) Сообщения об результате отправки формы
"""
fast_order_false = (fast_order_false_procedure, fast_order_false_result)

to_order_none_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Найти товар под заказ<br>
3) Открыть форму <Заказать товар><br>
4) Очистить поля ввода<br>
5) Отправить форму нажав на кнопку <Отправить>
"""

to_order_none_result =\
"""
5) Сообщения об результате отправки формы
"""
to_order_none = (to_order_none_procedure, to_order_none_result)

to_order_true_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Найти товар под заказ<br>
3) Открыть форму <Заказать товар><br>
4) Заполнить поля ввода:<br>
<Ваше имя *>: {true_input["name"]}<br>
<Телефон *>: {true_input["tel"]}<br>
<E-Mail *>: {true_input["email_one"]}<br>
<Сообщение>: {true_input["aria_comment"]}<br>
5) Отправить форму нажав на кнопку <Отправить>
"""

to_order_true_result =\
"""
5) Сообщения об результате отправки формы
"""
to_order_true = (to_order_true_procedure, to_order_true_result)

to_order_false_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Найти товар под заказ<br>
3) Открыть форму <Заказать товар><br>
4) Заполнить поля ввода:<br>
<Ваше имя *>: {false_input}<br>
<Телефон *>: {false_input}<br>
<E-Mail *>: {false_input}<br>
<Сообщение>: {false_input}<br>
5) Отправить форму нажав на кнопку <Отправить>
"""

to_order_false_result =\
"""
5) Сообщения об результате отправки формы
"""
to_order_false = (to_order_false_procedure, to_order_false_result)

in_gift_none_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Перейти на страницу товара <br>
3) Открыть форму <В подарок><br>
4) Очистить поля ввода<br>
5) Отправить форму нажав на кнопку <Отправить подсказку>
"""

in_gift_none_result =\
"""
5) Сообщения об результате отправки формы
"""
in_gift_none = (in_gift_none_procedure, in_gift_none_result)

in_gift_true_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Перейти на страницу товара <br>
3) Открыть форму <В подарок><br>
4) Заполнить поля ввода:<br>
<Ваше имя *>: {true_input["name"]}
<Имя получателя *>: {true_input["name"]}
<Электронная почта получателя *>: {true_input["email_one"]}
5) Отправить форму нажав на кнопку <Отправить подсказку>
"""

in_gift_true_result =\
"""
5) Сообщения об результате отправки формы
"""
in_gift_true = (in_gift_true_procedure, in_gift_true_result)

in_gift_false_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Перейти на страницу товара <br>
3) Открыть форму <В подарок><br>
4) Заполнить поля ввода:<br>
<Ваше имя *>: {false_input}
<Имя получателя *>: {false_input}
<Электронная почта получателя *>: {false_input}
5) Отправить форму нажав на кнопку <Отправить подсказку>
"""

in_gift_false_result =\
"""
5) Сообщения об результате отправки формы
"""
in_gift_false = (in_gift_false_procedure, in_gift_false_result)

send_ask_none_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Перейти на страницу товара <br>
3) Открыть форму <Задать вопрос><br>
4) Очистить поля ввода<br>
7) Отправить форму нажав на кнопку <Отправить>
"""

send_ask_none_result =\
"""
5) Сообщения об результате отправки формы
"""
send_ask_none = (send_ask_none_procedure, send_ask_none_result)

send_ask_true_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Перейти на страницу товара <br>
3) Открыть форму <Задать вопрос> нажав на кнопку <Задать вопрос><br>
4) Заполнить поля ввода:<br>
<Ваше имя *>: {true_input["name"]}
<Телефон *>: {true_input["tel"]}
<E-mail>: {true_input["email_one"]}
<Вопрос *>: {true_input["aria_comment"]}
5) Нажать на кнопку сбросить<br>
6) Заполнить поля повторно<br>
7) Отправить форму нажав на кнопку <Отправить>
"""

send_ask_true_result =\
"""
7) Сообщения об результате отправки формы
"""
send_ask_true = (send_ask_true_procedure, send_ask_true_result)

send_ask_false_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Перейти на страницу товара <br>
3) Открыть форму <Задать вопрос><br>
4) Заполнить поля ввода:<br>
<Ваше имя *>: {false_input}
<Телефон *>: {false_input}
<E-mail>: {false_input}
<Вопрос *>: {false_input}
5) Отправить форму нажав на кнопку <Войти>
"""

send_ask_false_result =\
"""
5) Сообщения об результате отправки формы
"""
send_ask_false = (send_ask_false_procedure, send_ask_false_result)

log_on_none_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Открыть форму <Авторизации><br>
3) Очистить поля ввода<br>
4)  Отправить форму нажав на кнопку <Войти>
"""

log_on_none_result =\
"""
4) Сообщения об Ошибки
"""
log_on_none = (log_on_none_procedure, log_on_none_result)

log_on_false_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Открыть форму <Авторизации><br>
3) Заполнить поля ввода:<br>
<Логин *>: {false_input}
<Пароль *>: {false_input}
4) Отправить форму нажав на кнопку <Войти>
"""

log_on_false_result =\
"""
4) Сообщения об ошибки
"""
log_on_false = (log_on_false_procedure, log_on_false_result)

log_on_true_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Открыть форму <Авторизации><br>
3) Заполнить поля ввода:<br>
<Логин *>: {true_input["email_two"]}
<Пароль *>: {true_input["email_two"]}
4) Отправить форму<br>
5)Перейти в личный кабинет
"""

log_on_true_result =\
"""
5) Переход успешен
"""
log_on_true = (log_on_true_procedure, log_on_true_result)

pass_recovery_none_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Открыть форму <Авторизации><br>
3) Переход на страницу востановления пароля<br>
4) Очистить поля ввода<br>
5) отправить форму нажав на кнопку <Восстановить>
"""

pass_recovery_none_result =\
"""
5)  Сообщения с ошибками
"""
pass_recovery_none = (pass_recovery_none_procedure, pass_recovery_none_result)

pass_recovery_false_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Открыть форму <Авторизации><br>
3) Переход на страницу востановления пароля<br>
3) Заполнить поля ввода:<br>
<E-Mail: *>: {true_input["email_one"]}
5) отправить форму нажав на кнопку <Восстановить>
"""

pass_recovery_false_result =\
"""
5)  Сообщения с ошибкой
"""
pass_recovery_false = (pass_recovery_false_procedure, pass_recovery_false_result)

pass_recovery_true_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Открыть форму <Авторизации><br>
3) Переход на страницу востановления пароля<br>
3) Заполнить поля ввода:<br>
<E-Mail: *>: {true_input["email_one"]}
5) отправить форму нажав на кнопку <Восстановить>
"""

pass_recovery_true_result =\
"""
5)  Сообщение с сайта конфаэль о востановление пароля на почте
"""
pass_recovery_true = (pass_recovery_true_procedure, pass_recovery_true_result)

pass_change_false__procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Авторизация<br>
логин: {true_input["email_one"]}
пароль: {true_input["password_one"]}
или
пароль: {true_input["password_one_alt"]}
3) Переход на страницу смены пароля<br>
4) Заполнить поля ввода:<br>
<Новый пароль*>: 123
<Новый пароль еще раз*>: 123
5) отправить форму нажав а <Сохранить изменения>
"""

pass_change_false_result =\
"""
5)  Сообщения с ошибками
"""
pass_change_false = (pass_change_false__procedure, pass_change_false_result)

pass_change_none__procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Авторизация<br>
3) Переход на страницу смены пароля<br>
4) Очистить поля ввода<br>
5) отправить форму нажав а <Сохранить изменения>
"""

pass_change_none_result =\
"""
5)  Сообщения с ошибками
"""
pass_change_none = (pass_change_none__procedure, pass_change_none_result)

pass_change_true_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Авторизация:<br>
логин: {true_input["email_one"]}
пароль: {true_input["password_one"]}
или
пароль: {true_input["password_one_alt"]}
3) Переход на страницу смены пароля<br>
4) Заполнить поля ввода:<br>
Поля:
<Новый пароль*>: <пароль>br>
<Новый пароль еще раз*>: <пароль><br>
Данные:
<пароль>: {true_input["password_one"]}
или
<пароль>: {true_input["password_one_alt"]}
5) отправить форму нажав а <Сохранить изменения><br>
6) Разлогиниться<br>
7) Авторизация c новым паролем
"""

pass_change_true_result =\
"""
7) Успешная авторизация
"""
pass_change_true = (pass_change_true_procedure, pass_change_true_result)

priv_change_false__procedure =\
"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Авторизация:<br>
<логин>: {true_input["email_tree"]}
или
<логин>: {true_input{"email_chetiri"}}
<пароль>: {true_input{"password_tree_chetiri"}}
3) Переход на страницу смены пароля<br>
4) Заполнить поля ввода:<br>
<Фамилия Имя Отчество*>:  {false_input}<br>
<Телефон*>: {false_input}<br>
<E-mail*>: {false_input}<br>
5) отправить форму нажав а <Сохранить изменения>
"""

priv_change_false_result =\
"""
5)  Сообщения с ошибками
"""
priv_change_false = (priv_change_false__procedure, priv_change_false_result)

priv_change_none_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Авторизация<br>
3) Переход на страницу смены пароля<br>
4) Очистить поля ввода<br>
5) отправить форму нажав а <Сохранить изменения><br>

"""

priv_change_none_result =\
"""
5) Сообщения об ошибки
"""
priv_change_none = (priv_change_none_procedure, priv_change_none_result)

priv_change_true_procedure =\
f"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Авторизация:<br>
<логин>: {true_input["email_tree"]}
или
<логин>: {true_input{"email_chetiri"}}
<пароль>: {true_input{"password_tree_chetiri"}}
3) Переход на страницу смены личных данных<br>
4) Заполнить поля ввода:<br>
Поля:
<Фамилия Имя Отчество*>:  <Фио><br>
<Телефон*>: <телефон><br>
<E-mail*>: <E-mail><br>
Использовать один из двух наборов данных:
<Фамилия Имя Отчество*>:  {true_input["name_two"]}<br>
<Телефон*>: {true_input["tel_two"]}<br>
<E-mail*> {true_input["email_tree"]}<br>
или
<Фамилия Имя Отчество*>:  {true_input["name_tree"]}<br>
<Телефон*>: {true_input["tel_tree"]}<br>
<E-mail*> {true_input["email_chetiri"]}<br>
5) отправить форму нажав а <Сохранить изменения><br>
6) Обновить страницу<br>
6) Разлогиниться<br>
7) Авторизация c новым логином
"""

priv_change_true_result =\
"""
6) Успешная авторизация
"""
priv_change_true = (priv_change_true_procedure, priv_change_true_result)

to__order_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Авторизация<br>
3) Переход на страницу <Моя корзина><br>
4) Очистка корзины нажатием на кнопку <Очистить><br>
5) Переход в католог<br>
6) добавление товара<br>
6) Переход в корзину<br>
7) Переход на страницу оформления товара<br>
8) выбор параметров см. раздел <разное> этого тест кейса.<br>
9)Отправка формы
"""

to__order_result =\
"""
9) Сообщение об успешном оформление
"""
to__order = (to__order_procedure, to__order_result)

filter_price_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Изменениен максимальной цены на 1/2 от максимальной<br>
3) Изменениен минимальной цены на 1/4 от изначальной максимальной<br>
4) Приминить фильтр<br>
5) Сменить страницу
"""

filter_price_result =\
"""
4) Цена товаров не должна быть ниже минимальной и  выше ваксимальной<br>
5) Цена товаров не должна быть ниже минимальной и  выше ваксимальной
"""
filter_price = (filter_price_procedure, filter_price_result)

filter_delivery_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/catalog/komu/<br>
2) Выбор фильтра доставки<br>
4) Приминить фильтр<br>
5) Перейти на страницу товара
"""

filter_delivery_result =\
"""
5) В информации о товаре должа быть таже доставка что и при выборе фильтра
"""
filter_delivery = (filter_delivery_procedure, filter_delivery_result)

search_procedure =\
"""
1) Открыть страницу https://confaelshop.ru/<br>
2) Ввод поискового запроса<br>
3) Подтверждения поиска<br>
"""
search_result =\
"""
3) В заголовке каждого товара должен присутствовать поисковой запрос
"""
search = (search_procedure, search_result)
