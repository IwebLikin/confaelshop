from bs4 import BeautifulSoup
import requests
import csv
import time
import re


DOMAIN = 'confaelshop.ru'
HOST = 'https://' + DOMAIN
FORBIDDEN_PREFIXES = ['#', 'tel:', 'mailto:', "?lang=", "."]
goroda = ("spb.c", "kz.c", "astana.c", "vgg.c", "ekb.", "kzn.", "mkh.c",
          "nn.", "nsk.", "obn.", "psk.", "rnd.", "sarov.", "plus.")
links = set()
blink = set()
parse_url = set("https://plus.google.com/+ConfaelshopRus")
count = 0
pattern = (r"\/catalog\/.*\/[0-9$]*\/$|"
           r"\/catalog\/{1,3}.*\/[0-9$]*\/index|"
           r"\.jpg$|"
           r"sort|"
           r"PAGEN|"
           r"filter|"
           r"display|"
           r"tags")
regex = re.compile(pattern)


def add_all_links_recursive(url, maxdepth=20, url_first=DOMAIN):
    links_to_handle_recursive = []
    respond = None
    info = {"link in broken list": 0, "skip bad prifix": 0,
            "skip for change gorod": 0, "skip": 0, "slip for parse last": 0,
            "del": 0, "check": 0}
    try:
        request = requests.get(url)
        respond = request.status_code
    except Exception as a:
        print(a, type(a))
    parse_url.add(url)
    if respond not in (200, 301):
        if url not in blink:
            blink.add((url, respond, url_first))
            string = request.url.replace("/", "").replace(".", "").replace("-", "").replace(":", "")
            with open(f"result/{string}.txt", "w", encoding="utf-8") as fh:
                fh.write(requests.get(url_first).text)
            print((url, respond))
        else:
            info["link in broken list"] += 1
    else:
        soup = BeautifulSoup(request.content, 'html.parser')
        for tag_a in soup.find_all('a', href=True):
            link = tag_a.get('href')
            m = re.findall(regex, link)
            if len(m) > 0:
                info["skip"] += 1
                continue
            if link is not None:
                if all(False for a in goroda if a in link):
                    if all((not link.startswith(prefix)) for prefix in
                            FORBIDDEN_PREFIXES):
                        if link.startswith('/') and not link.startswith('//'):
                            link = HOST + link
                        if link not in links:
                            links.add(link)
                            # print(link, respond)
                            if DOMAIN in link:
                                links_to_handle_recursive.append(link)
                            try:
                                request = requests.get(link)
                                respond = request.status_code
                                info["check"] += 1
                            except Exception:
                                pass
                            if respond not in (200, 301):
                                string = request.url.replace("/", "").replace(".", "").replace("-", "").replace(":", "")
                                with open(f"result/{string}.txt", "w", encoding="utf-8") as fh:
                                    fh.write(requests.get(url).text)
                                blink.add((link, respond, url))
                                # print((url, respond))
                            print(link)
                    else:
                        info["skip bad prifix"] += 1
                else:
                    info["skip for change gorod"] += 1
            else:
                info["skip"] += 1
    print(info)
    info = {"link in broken list": 0, "skip bad prifix": 0,
            "skip for change gorod": 0, "skip": 0, "slip for parse last": 0,
            "del": 0, "check": 0}
    print(blink)
        # pdb.set_trace()
    if maxdepth > 0:
        list_parse_ = links_to_handle_recursive
        for link in list_parse_:
            for a in parse_url:
                if a in list_parse_:
                    info["del"] += 1
                    list_parse_.remove(a)
            if link not in parse_url:
                global count
                count += 1
                print("********************************************", link,
                      count, "depth {}".format(maxdepth))
                add_all_links_recursive(link, url_first=url,
                                        maxdepth=maxdepth - 1)
            else:
                info["slip for parse last"] += 1


def main():
    try:
        add_all_links_recursive('https://confaelshop.ru/')
    except Exception:
        pass
    with open("result/url_broken_links.csv", "w", encoding="utf-8", newline="") as fh:
        writer = csv.writer(fh, quoting=csv.QUOTE_ALL)
        writer.writerow(["link", "status_code"])
        for row in blink:
            writer.writerow(row)

    result = (len(links), len(blink), len(parse_url))
    with open("result/url_result.csv", "w", encoding="utf-8", newline="") as fh:
        writer = csv.writer(fh, quoting=csv.QUOTE_ALL)
        writer.writerow(["Link check", "link error", "link_parse"])
        writer.writerow(result)

    with open("result/url_links.txt", "w", encoding="utf-8") as fh:
        print("Ссылки", file=fh)
        for row in links:
            print(row, file=fh)
    return result


if __name__ == '__main__':
    main()
