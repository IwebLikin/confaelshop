# -*- coding: utf-8 -*-
import conftest
import pytest
import datetime
from openpyxl import Workbook
from time import sleep
from src.DriverObject import driver_object
from src.test import Test
import check_url
temp = ["", [None, None], "", (0, 0, 0)]
NONE = "Пустой ввод"
TRUE = "Корректный Ввод"
FALSE = "Не корректный Ввод"


def doc(id, name, browser, other):
    browsers = dict(
                    c="Разрешение 1920х1080",
                    ff="Разрешение 1920х1080",
                    ie="Разрешение 1920х1080",
                    mobile="Galaxy S5",
                    ipad="Разрешение 768х1024",
                    galaxys5="Разрешение 360х640"
                    )
    if browser in ("ie", "ff", "c"):
        window_size = "Разрешение: 1920х1080;"
    elif browser == "galaxys5":
        window_size = "Разрешение: 768х1024;"
    elif browser == "ipad":
        window_size = "Разрешение: 360х640;"
    return f'ID: ConfaelShop {id}; Название: {name}; {window_size} Браузер: {browsers[browser]}; Разное: {other}'


def setup_module(module):
    book = Workbook()
    temp[2] = 'report'
    print(temp)
    book.save('report.xlsx')


def test_0001_fast_view():
    @driver_object.repeat
    def run():
        import t_0001_fast_view
        Test_ = driver_object("ff")
        temp[0] = Test_.get_doc(1, "быстрый просмотр", "")
        with Test_:
            assert t_0001_fast_view.step_1(Test_)
            assert t_0001_fast_view.step_2(Test_)
            assert t_0001_fast_view.step_3(Test_)


def test_0002_fast_view():
    @driver_object.repeat
    def run():
        import t_0001_fast_view
        Test_ = driver_object("c")
        temp[0] = Test_.get_doc(2, "быстрый просмотр", "")
        with Test_:
            assert t_0001_fast_view.step_1(Test_)
            assert t_0001_fast_view.step_2(Test_)
            assert t_0001_fast_view.step_3(Test_)


def test_0003_fast_view():
    @driver_object.repeat
    def run():
        import t_0001_fast_view
        Test_ = driver_object("ie")
        temp[0] = Test_.get_doc(3, "быстрый просмотр", "")
        with Test_:
            assert t_0001_fast_view.step_1(Test_)
            assert t_0001_fast_view.step_2(Test_)
            assert t_0001_fast_view.step_3(Test_)


def test_0004_f_Call_back():
    @driver_object.repeat
    def run():
        import t_0002_Call_back
        Test_ = driver_object("ff")
        temp[0] = Test_.get_doc(4, "Обратный звонок", NONE)
        with Test_:
            values = dict(
                          name="",
                          tel="")
            assert t_0002_Call_back.step_1(Test_)
            assert t_0002_Call_back.step_2(Test_, values)
            assert t_0002_Call_back.step_3(Test_, "None")


def test_0005_f_Call_back():
    @driver_object.repeat
    def run():
        import t_0002_Call_back
        Test_ = driver_object("c")
        temp[0] = Test_.get_doc(5, "Обратный звонок", NONE)
        with Test_:
            values = dict(
                          name="",
                          tel="")
            assert t_0002_Call_back.step_1(Test_)
            assert t_0002_Call_back.step_2(Test_, values)
            assert t_0002_Call_back.step_3(Test_, "None")


def test_0006_ie_Call_back():
    @driver_object.repeat
    def run():
        import t_0002_Call_back
        Test_ = driver_object("ie")
        temp[0] = Test_.get_doc(6, "Обратный звонок", NONE)
        with Test_:
            values = dict(
                          name="",
                          tel="")
            assert t_0002_Call_back.step_1(Test_)
            assert t_0002_Call_back.step_2(Test_, values)
            assert t_0002_Call_back.step_3(Test_, "None")


def test_0007_f_Call_back():
    @driver_object.repeat
    def run():
        import t_0002_Call_back
        Test_ = driver_object("ff")
        temp[0] = Test_.get_doc(7, "Обратный звонок", TRUE)
        with Test_:
            values = dict(
                          name="Тестирование",
                          tel="90798989898")
            assert t_0002_Call_back.step_1(Test_)
            assert t_0002_Call_back.step_2(Test_, values)
            assert t_0002_Call_back.step_3(Test_, "True")


def test_0008_c_Call_back():
    @driver_object.repeat
    def run():
        import t_0002_Call_back
        Test_ = driver_object("c")
        temp[0] = Test_.get_doc(8, "Обратный звонок", TRUE)
        with Test_:
            values = dict(
                          name="Тестирование",
                          tel="90798989898")
            assert t_0002_Call_back.step_1(Test_)
            assert t_0002_Call_back.step_2(Test_, values)
            assert t_0002_Call_back.step_3(Test_, "True")


def test_0009_ie_Call_back():
    @driver_object.repeat
    def run():
        import t_0002_Call_back
        Test_ = driver_object("ie")
        temp[0] = Test_.get_doc(9, "Обратный звонок", TRUE)
        with Test_:
            values = dict(
                          name="Тестирование",
                          tel="90798989898")
            assert t_0002_Call_back.step_1(Test_)
            assert t_0002_Call_back.step_2(Test_, values)
            assert t_0002_Call_back.step_3(Test_, "True")


def test_0010_f_Call_back():
    @driver_object.repeat
    def run():
        import t_0002_Call_back
        Test_ = driver_object("ff")
        temp[0] = Test_.get_doc(10, "Обратный звонок", FALSE)
        with Test_:
            values = dict(
                          name="Тестирование !@#$%^&*(123",
                          tel="Тестирование !@#$%^&*(123")
            assert t_0002_Call_back.step_1(Test_)
            assert t_0002_Call_back.step_2(Test_, values)
            assert t_0002_Call_back.step_3(Test_, "False")


def test_0011_Call_back():
    @driver_object.repeat
    def run():
        import t_0002_Call_back
        Test_ = driver_object("c")
        temp[0] = Test_.get_doc(11, "Обратный звонок", FALSE)
        with Test_:
            values = dict(
                          name="Тестирование !@#$%^&*(123",
                          tel="Тестирование !@#$%^&*(123")
            assert t_0002_Call_back.step_1(Test_)
            assert t_0002_Call_back.step_2(Test_, values)
            assert t_0002_Call_back.step_3(Test_, "False")


def test_0012_Call_back():
    @driver_object.repeat
    def run():
        import t_0002_Call_back
        Test_ = driver_object("ie")
        temp[0] = Test_.get_doc(12, "Обратный звонок", FALSE)
        with Test_:
            values = dict(
                          name="Тестирование !@#$%^&*(123",
                          tel="Тестирование !@#$%^&*(123")
            assert t_0002_Call_back.step_1(Test_)
            assert t_0002_Call_back.step_2(Test_, values)
            assert t_0002_Call_back.step_3(Test_, "False")

def test_0013_add_del_basket():
    @driver_object.repeat
    def run():
        import t_0003_add_del_basket
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(13, "Корзина сайта (Добавление и удаление товара)", "")
        with Test_:
            assert t_0003_add_del_basket.step_1(Test_)
            assert t_0003_add_del_basket.step_2(Test_)
            assert t_0003_add_del_basket.step_3(Test_)
            assert t_0003_add_del_basket.step_4(Test_)



def test_0014_add_del_basket():
    @driver_object.repeat
    def run():
        import t_0003_add_del_basket
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(14, "Корзина сайта (Добавление и удаление товара)", "")
        with Test_:
            assert t_0003_add_del_basket.step_1(Test_)
            assert t_0003_add_del_basket.step_2(Test_)
            assert t_0003_add_del_basket.step_3(Test_)
            assert t_0003_add_del_basket.step_4(Test_)


def test_0015_add_del_basket():
    @driver_object.repeat
    def run():
        import t_0003_add_del_basket
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(15, "Корзина сайта (Добавление и удаление товара)", "")
        with Test_:
            assert t_0003_add_del_basket.step_1(Test_)
            assert t_0003_add_del_basket.step_2(Test_)
            assert t_0003_add_del_basket.step_3(Test_)
            assert t_0003_add_del_basket.step_4(Test_)

def test_0016_add_del_basket():
    @driver_object.repeat
    def run():
        import t_0003_add_del_basket
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(16, "Корзина сайта (Добавление и удаление товара)", "")
        with Test_:
            assert t_0003_add_del_basket.step_1(Test_)
            assert t_0003_add_del_basket.step_2(Test_)
            assert t_0003_add_del_basket.step_3(Test_)
            assert t_0003_add_del_basket.step_4(Test_)

def test_0017_add_del_basket():
    @driver_object.repeat
    def run():
        import t_0003_add_del_basket
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(17, "Корзина сайта (Добавление и удаление товара)", "")
        with Test_:
            assert t_0003_add_del_basket.step_1(Test_)
            assert t_0003_add_del_basket.step_2(Test_)
            assert t_0003_add_del_basket.step_3(Test_)
            assert t_0003_add_del_basket.step_4(Test_)

def test_0018_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(18, "Форма Купить в один клик", NONE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "None")


def test_0019_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(19, "Форма Купить в один клик", NONE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "None")


def test_0020_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(20, "Форма Купить в один клик", NONE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "None")


def test_0021_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(21, "Форма Купить в один клик", TRUE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "True")


def test_0022_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(22, "Форма Купить в один клик", TRUE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "True")


def test_0023_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(23, "Форма Купить в один клик", TRUE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "True")


def test_0024_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(23, "Форма Купить в один клик", FALSE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "False")


def test_0025_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(24, "Форма Купить в один клик", FALSE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "False")


def test_0026_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(25, "Форма Купить в один клик", FALSE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "False")


def test_0027_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(26, "Форма Купить в один клик", NONE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "None")


def test_0028_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(27, "Форма Купить в один клик", TRUE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "True")


def test_0029_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(28, "Форма Купить в один клик", FALSE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "False")


def test_0030_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(29, "Форма Купить в один клик", NONE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "None")


def test_0031_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(30, "Форма Купить в один клик", TRUE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "True")


def test_0032_Fast_order():
    @driver_object.repeat
    def run():
        import t_0004_Fast_order
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(31, "Форма Купить в один клик", FALSE)
        with Test_:
            assert t_0004_Fast_order.step_1(Test_)
            assert t_0004_Fast_order.step_2(Test_, "False")


def test_t0033_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(32, "Форма Товар под заказ", NONE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "None")


def test_t0034_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(34, "Форма Товар под заказ", NONE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "None")

def test_t0035_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(35, "Форма Товар под заказ", NONE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "None")


def test_t0036_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(36, "Форма Товар под заказ", TRUE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "True")


def test_t0037_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(37, "Форма Товар под заказ", TRUE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "True")


def test_t0038_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(38, "Форма Товар под заказ", TRUE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "True")


def test_t0039_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(39, "Форма Товар под заказ", FALSE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "False")


def test_t0040_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(40, "Форма Товар под заказ", FALSE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "False")


def test_t0041_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(41, "Форма Товар под заказ", FALSE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "False")


def test_t0042_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(42, "Форма Товар под заказ", NONE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "None")


def test_t0043_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(43, "Форма Товар под заказ", TRUE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "True")


def test_t0044_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(44, "Форма Товар под заказ", FALSE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "False")

def test_t0045_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(45, "Форма Товар под заказ", NONE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "None")


def test_t0046_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(46, "Форма Товар под заказ", TRUE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "True")


def test_t0047_to_order():
    @driver_object.repeat
    def run():
        import t_0005_to_order
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(47, "Форма Товар под заказ", FALSE)
        with Test_:
            assert t_0005_to_order.step_1(Test_)
            assert t_0005_to_order.step_2(Test_)
            assert t_0005_to_order.step_3(Test_, "False")


def test_t0048_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(48, "Форма В подарок", NONE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "None")


def test_t0049_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(49, "Форма В подарок", NONE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "None")


def test_t0050_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(50, "Форма В подарок", NONE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "None")


def test_t0051_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(51, "Форма В подарок", TRUE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "True")


def test_t0052_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(52, "Форма В подарок", TRUE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "True")


def test_t0053_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(53, "Форма В подарок", TRUE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "True")


def test_t0054_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(54, "Форма В подарок", FALSE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "False")


def test_t0055_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(55, "Форма В подарок", FALSE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "False")


def test_t0056_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(56, "Форма В подарок", FALSE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "False")


def test_t0057_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(57, "Форма В подарок", NONE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "None")


def test_t0058_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(58, "Форма В подарок", TRUE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "True")


def test_t0059_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(59, "Форма В подарок", FALSE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "False")


def test_t0060_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(60, "Форма В подарок", NONE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "None")


def test_t0061_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(61, "Форма В подарок", TRUE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "True")


def test_t0062_in_gift():
    @driver_object.repeat
    def run():
        import t_0006_in_gift
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(62, "Форма В подарок", FALSE)
        with Test_:
            assert t_0006_in_gift.step_1(Test_)
            assert t_0006_in_gift.step_2(Test_)
            assert t_0006_in_gift.step_3(Test_, "False")

def test_t0063_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(63, "Форма Задать вопрос", NONE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "None")


def test_t0064_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(64, "Форма Задать вопрос", NONE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "None")


def test_t0065_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(65, "Форма Задать вопрос", NONE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "None")


def test_t0066_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(66, "Форма Задать вопрос", TRUE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "True")


def test_t0067_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(67, "Форма Задать вопрос", TRUE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "True")


def test_t0068_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(68, "Форма Задать вопрос", TRUE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "True")


def test_t0069_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(69, "Форма Задать вопрос", FALSE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "False")


def test_t0070_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(70, "Форма Задать вопрос", FALSE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "False")


def test_t0071_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(71, "Форма Задать вопрос", FALSE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "False")


def test_t0072_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(72, "Форма Задать вопрос", NONE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "None")


def test_t0073_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(73, "Форма Задать вопрос", TRUE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "True")


def test_t0074_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(74, "Форма Задать вопрос", FALSE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "False")


def test_t0075_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(75, "Форма Задать вопрос", NONE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "None")


def test_t0076_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(76, "Форма Задать вопрос", TRUE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "True")


def test_t0077_send_ask():
    @driver_object.repeat
    def run():
        import t007_send_ask
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(77, "Форма Задать вопрос", FALSE)
        with Test_:
            assert t007_send_ask.step_1(Test_)
            assert t007_send_ask.step_2(Test_)
            assert t007_send_ask.step_3(Test_, "False")

def test_t0078_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(78, "Форма Авторизация", NONE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "None")

def test_t0079_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(79, "Форма Авторизация", NONE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "None")

def test_t0080_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(80, "Форма Авторизация", NONE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "None")

def test_t0081_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(81, "Форма Авторизация", FALSE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "False")

def test_t0082_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(82, "Форма Авторизация", FALSE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "False")

def test_t0083_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(83, "Форма Авторизация", FALSE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "False")

def test_t0084_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(84, "Форма Авторизация", TRUE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "user_one")

def test_t0085_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(85, "Форма Авторизация", TRUE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "user_one")

def test_t0086_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(86, "Форма Авторизация", TRUE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "user_one")

def test_t0087_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(87, "Форма Авторизация", NONE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "None")

def test_t0088_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(88, "Форма Авторизация", TRUE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "user_one")

def test_t0089_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(89, "Форма Авторизация", FALSE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "False")

def test_t0090_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(90, "Форма Авторизация", NONE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "None")

def test_t0091_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(91, "Форма Авторизация", TRUE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "user_one")

def test_t0092_log_on():
    @driver_object.repeat
    def run():
        import t008_log_on
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(92, "Форма Авторизация", FALSE)
        with Test_:
            assert t008_log_on.step_1(Test_)
            assert t008_log_on.step_2(Test_, "False")

def test_t0093_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(93, "Форма Востановление пароля", NONE)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "None")

def test_t0094_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(94, "Форма Востановление пароля", NONE)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "None")

def test_t0095_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(95, "Форма Востановление пароля", NONE)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "None")

def test_t0096_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(96, "Форма Востановление пароля", TRUE)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "True")


def test_t0097_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(97, "Форма Востановление пароля", TRUE)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "True")


def test_t0098_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(98, "Форма Востановление пароля", TRUE)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "True")


def test_t0099_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(99, "Форма Востановление пароля", FALSE)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "False")


def test_t0100_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(100, "Форма Востановление пароля", FALSE)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "False")


def test_t0101_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(101, "Форма Востановление пароля", FALSE)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "False")


def test_t0102_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(102, "Форма Востановление пароля", None)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "None")


def test_t0103_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(103, "Форма Востановление пароля", None)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "None")


def test_t0104_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(104, "Форма Востановление пароля", None)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "None")


def test_t0105_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(105, "Форма Востановление пароля", None)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "None")


def test_t0106_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(106, "Форма Востановление пароля", None)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "None")


def test_t0107_pass_recovery():
    @driver_object.repeat
    def run():
        import t009_password_recovery
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(107, "Форма Востановление пароля", False)
        with Test_:
            assert t009_password_recovery.step_1(Test_)
            assert t009_password_recovery.step_2(Test_)
            assert t009_password_recovery.step_3(Test_, "False")

def test_t0108_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(108, "Форма Смена пароля", NONE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "None")


def test_t0109_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(109, "Форма Смена пароля", NONE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "None")


def test_t0110_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(110, "Форма Смена пароля", NONE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "None")


def test_t0111_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(111, "Форма Смена пароля", TRUE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "True")


def test_t0112_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(112, "Форма Смена пароля", TRUE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "True")


def test_t0113_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(113, "Форма Смена пароля", TRUE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "True")


def test_t0114_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(114, "Форма Смена пароля", FALSE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "False")


def test_t0115_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(115, "Форма Смена пароля", FALSE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "False")


def test_t0116_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(116, "Форма Смена пароля", FALSE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "False")


def test_t0117_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(117, "Форма Смена пароля", NONE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "None")


def test_t0118_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(118, "Форма Смена пароля", TRUE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "True")

def test_t0119_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(119, "Форма Смена пароля", FALSE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "False")


def test_t0120_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(120, "Форма Смена пароля", NONE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "None")


def test_t0121_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(121, "Форма Смена пароля", TRUE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "True")


def test_t0122_pass_change():
    @driver_object.repeat
    def run():
        import t010_password_change
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(122, "Форма Смена пароля", FALSE)
        with Test_:
            assert t010_password_change.step_1(Test_)
            assert t010_password_change.step_2(Test_, "False")

def test_t0123_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(123, "Форма Смена личных данных", NONE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "None")


def test_t0124_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(124, "Форма Смена личных данных", NONE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "None")


def test_t0125_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(125, "Форма Смена личных данных", NONE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "None")


def test_t0126_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(126, "Форма Смена личных данных", TRUE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "True")


def test_t0127_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(127, "Форма Смена личных данных", TRUE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "True")

def test_t0128_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(128, "Форма Смена личных данных", TRUE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "True")



def test_t0129_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(129, "Форма Смена личных данных", FALSE)
        with Test_:
            assert t011_priv_change.step_1(Test_)

            assert t011_priv_change.step_2(Test_, "False")


def test_t0130_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(130, "Форма Смена личных данных", FALSE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "False")


def test_t0131_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(131, "Форма Смена личных данных", FALSE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "False")


def test_t0132_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(132, "Форма Смена личных данных", NONE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "None")


def test_t0133_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(133, "Форма Смена личных данных", TRUE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "True")


def test_t0134_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(134, "Форма Смена личных данных", FALSE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "False")



def test_t0135_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(135, "Форма Смена личных данных", NONE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "None")


def test_t0136_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(136, "Форма Смена личных данных", TRUE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "True")


def test_t0137_priv_change():
    @driver_object.repeat
    def run():
        import t011_priv_change
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(137, "Форма Смена личных данных", FALSE)
        with Test_:
            assert t011_priv_change.step_1(Test_)
            assert t011_priv_change.step_2(Test_, "False")


def test_t0138_search():
    @driver_object.repeat
    def run():
        import t0012_search
        browser = "ff"
        value = "Шоколад"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(138, "Форма Поиска", f"Строка поиска {value}")
        with Test_:
            assert t0012_search.step_1(Test_, value, repeat)

"""def test_t00138_search():
    @driver_object.repeat
    def run():
        import t0012_search
        browser = "ff"
        value = "1234ASDQWE"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(138, "Форма Поиска", f"Строка поиска {value}")
        with Test_:
            assert t0012_search.step_1(Test_, value, repeat)"""

def test_t0139_search():
    @driver_object.repeat
    def run():
        import t0012_search
        browser = "c"
        value = "Шоколад"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(139, "Форма Поиска", f"Строка поиска {value}")
        with Test_:
            assert t0012_search.step_1(Test_, value, repeat)


def test_t0140_search():
    @driver_object.repeat
    def run():
        import t0012_search
        browser = "ie"
        value = "Шоколад"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(140, "Форма Поиска", f"Строка поиска {value}")
        with Test_:
            assert t0012_search.step_1(Test_, value, repeat)


def test_t0141_search():
    @driver_object.repeat
    def run():
        import t0012_search
        browser = "ipad"
        value = "Шоколад"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(141, "Форма Поиска", f"Строка поиска {value} Примичание: Пропущенно по причине найденного бага")
        with Test_:
            pass
            # assert t0012_search.step_1(Test_, value, repeat)


def test_t0142_search():
    @driver_object.repeat
    def run():
        import t0012_search
        browser = "galaxys5"
        value = "Шоколад"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(142, "Форма Поиска", f"Строка поиска {value}")
        with Test_:
            assert t0012_search.step_1(Test_, value, repeat)

def test_t0143_filter_price():
    @driver_object.repeat
    def run():
        import t0013_filter_price
        browser = "ff"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(143, "Форма фильтр цены", f"Макс. цена 1/2 от изначальной, Мин.цена 1/4 от максимальной изначальной")
        with Test_:
            assert t0013_filter_price.step_1(Test_)
            assert t0013_filter_price.step_2(Test_, repeat)


def test_t0144_filter_price():
    @driver_object.repeat
    def run():
        import t0013_filter_price
        browser = "c"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(144, "Форма фильтр цены", f"Макс. цена 1/2 от изначальной, Мин.цена 1/4 от максимальной изначальной")
        with Test_:
            assert t0013_filter_price.step_1(Test_)
            assert t0013_filter_price.step_2(Test_, repeat)


def test_t0145_filter_price():
    @driver_object.repeat
    def run():
        import t0013_filter_price
        browser = "ie"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(145, "Форма фильтр цены", f"Макс. цена 1/2 от изначальной, Мин.цена 1/4 от максимальной изначальной")
        with Test_:
            assert t0013_filter_price.step_1(Test_)
            assert t0013_filter_price.step_2(Test_, repeat)


def test_t0146_filter_price():
    @driver_object.repeat
    def run():
        import t0013_filter_price
        browser = "ipad"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(146, "Форма фильтр цены", f"Макс. цена 1/2 от изначальной, Мин.цена 1/4 от максимальной изначальной")
        with Test_:
            assert t0013_filter_price.step_1(Test_)
            assert t0013_filter_price.step_2(Test_, repeat)


def test_t0147_filter_price():
    @driver_object.repeat
    def run():
        import t0013_filter_price
        browser = "galaxys5"
        repeat = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(147, "Форма фильтр цены", f"Макс. цена 1/2 от изначальной, Мин.цена 1/4 от максимальной изначальной")
        with Test_:
            assert t0013_filter_price.step_1(Test_)
            assert t0013_filter_price.step_2(Test_, repeat)


cheker_dict = {
                "0": "Доставка только по Москве",
                "1": "Доставка только по Москве и МО",
                "2": "Доставка по РФ",
                "3": "Доставка только по Москве, МО и городам с бутиками Конфаэль"
                }


def test_t00148_filter_delivery():
    @driver_object.repeat
    def run():
        import t0014_filter_delivery
        browser = "ff"
        repeat = 2
        index = 0
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(148, "Форма фильтр доставки", f"Выбранный фильтр доставки {cheker_dict[str(index)]}")
        with Test_:
            assert t0014_filter_delivery.step_1(Test_, index)
            assert t0014_filter_delivery.step_2(Test_, index, 3)


def test_t00149_filter_delivery():
    @driver_object.repeat
    def run():
        import t0014_filter_delivery
        browser = "c"
        repeat = 2
        index = 0
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(149, "Форма фильтр доставки", f"Выбранный фильтр доставки {cheker_dict[str(index)]}")
        with Test_:
            assert t0014_filter_delivery.step_1(Test_, index)
            assert t0014_filter_delivery.step_2(Test_, index, 3)


def test_t00150_filter_delivery():
    @driver_object.repeat
    def run():
        import t0014_filter_delivery
        browser = "ie"
        repeat = 2
        index = 0
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(150, "Форма фильтр доставки", f"Выбранный фильтр доставки {cheker_dict[str(index)]}")
        with Test_:
            assert t0014_filter_delivery.step_1(Test_, index)
            assert t0014_filter_delivery.step_2(Test_, index, 3)


def test_t00151_filter_delivery():
    @driver_object.repeat
    def run():
        import t0014_filter_delivery
        browser = "ipad"
        repeat = 2
        index = 0
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(151, "Форма фильтр доставки", f"Выбранный фильтр доставки {cheker_dict[str(index)]}")
        with Test_:
            assert t0014_filter_delivery.step_1(Test_, index)
            assert t0014_filter_delivery.step_2(Test_, index, 3)


def test_t00152_filter_delivery():
    @driver_object.repeat
    def run():
        import t0014_filter_delivery
        browser = "galaxys5"
        repeat = 2
        index = 0
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(152, "Форма фильтр доставки", f"Выбранный фильтр доставки {cheker_dict[str(index)]}")
        with Test_:
            assert t0014_filter_delivery.step_1(Test_, index)
            assert t0014_filter_delivery.step_2(Test_, index, 3)


def test_t00153_filter_delivery():
    @driver_object.repeat
    def run():
        import t0014_filter_delivery
        browser = "ff"
        repeat = 2
        index = 1
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(153, "Форма фильтр доставки", f"Выбранный фильтр доставки {cheker_dict[str(index)]} только для {browser}")
        with Test_:
            assert t0014_filter_delivery.step_1(Test_, index)
            assert t0014_filter_delivery.step_2(Test_, index, 3)


def test_t00154_filter_delivery():
    @driver_object.repeat
    def run():
        import t0014_filter_delivery
        browser = "ff"
        repeat = 2
        index = 2
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(154, "Форма фильтр доставки", f"Выбранный фильтр доставки  только для {browser} {cheker_dict[str(index)]}")
        with Test_:
            assert t0014_filter_delivery.step_1(Test_, index)
            assert t0014_filter_delivery.step_2(Test_, index, 3)


def test_t00155_filter_delivery():
    @driver_object.repeat
    def run():
        import t0014_filter_delivery
        browser = "ff"
        repeat = 2
        index = 3
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(155, "Форма фильтр доставки", f"Выбранный фильтр доставки  только для {browser} {cheker_dict[str(index)]}")
        with Test_:
            assert t0014_filter_delivery.step_1(Test_, index)
            assert t0014_filter_delivery.step_2(Test_, index, 3)


def test_t00156__cart():
    @driver_object.repeat
    def run():
        import t0016_cart
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(156, "Форма Баланс карты", f"")
        with Test_:
            assert t0016_cart.step_1(Test_)


def test_t00157__cart():
    @driver_object.repeat
    def run():
        import t0016_cart
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(157, "Форма Баланс карты", f"")
        with Test_:
            assert t0016_cart.step_1(Test_)

def test_t00158__cart():
    @driver_object.repeat
    def run():
        import t0016_cart
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(158, "Форма Баланс карты", f"")
        with Test_:
            assert t0016_cart.step_1(Test_)


def test_t00159__cart():
    @driver_object.repeat
    def run():
        import t0016_cart
        browser = "ipad"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(159, "Форма Баланс карты", f"")
        with Test_:
            assert t0016_cart.step_1(Test_)


def test_t00160__cart():
    @driver_object.repeat
    def run():
        import t0016_cart
        browser = "galaxys5"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(160, "Форма Баланс карты", f"")
        with Test_:
            assert t0016_cart.step_1(Test_)

def test__t0161_to__order():
    @driver_object.repeat
    def run():
        import t0017__to_order
        browser = "c"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(161, "Оформление",  "Регион: Москва, Доставка:  За пределы МКАД, Оплата: Яндекс картойт")
        input_order = (["//a[@class='quick-location-tag']", 0],
                       ["//div[@class='bx-soa-pp-delivery-cost']", 0],
                       ["//div[@class='bx-soa-pp-company-smalltitle']", 0])
        with Test_:
            assert t0017__to_order.run_script(1, input_order, Test_)


def test__t0162_to__order():
    @driver_object.repeat
    def run():
        import t0017__to_order
        browser = "ff"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(162, "Оформление",  "Регион: Москва, Доставка:  За пределы МКАД, Оплата: Яндекс картойт")
        input_order = (["//a[@class='quick-location-tag']", 0],
                       ["//div[@class='bx-soa-pp-delivery-cost']", 0],
                       ["//div[@class='bx-soa-pp-company-smalltitle']", 0])
        with Test_:
            assert t0017__to_order.run_script(1, input_order, Test_)


def test__t0163_to__order():
    @driver_object.repeat
    def run():
        import t0017__to_order
        browser = "ie"
        Test_ = driver_object(browser)
        temp[0] = Test_.get_doc(163, "Оформление",  "Регион: Москва, Доставка:  За пределы МКАД, Оплата: Яндекс картойт")
        input_order = (["//a[@class='quick-location-tag']", 0],
                       ["//div[@class='bx-soa-pp-delivery-cost']", 0],
                       ["//div[@class='bx-soa-pp-company-smalltitle']", 0])
        with Test_:
            assert t0017__to_order.run_script(1, input_order, Test_)

def test_000_check_link():
    temp[0] = "Проверка страниц"
    temp[3] = check_url.main()

"""def test_report_1():
    temp[0] = "ID: ConfaelShop 1; Название: Форма  реп 1; Разрешение: 1920х1080;"
    return "true"

def test_report_2():
    temp[0] = "ID: ConfaelShop 2; Название: реп 2; Разрешение: 1920х1080;"
    assert 0

def test_report_3():
    temp[0] = "ID: ConfaelShop 3; Название: реп 3; Разрешение: 1920х1080;"
    pytest.xfail()"""
