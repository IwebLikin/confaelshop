import sys
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.touch_actions import TouchActions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import MoveTargetOutOfBoundsException, StaleElementReferenceException
from time import sleep
import traceback
import re
from src.test import Test
from src.PageObject import elements
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps

def step_1(Test, e):
    steps.log_on(Test, "user_one")
    Test.get("https://confaelshop.ru" + "/basket/")
    Test.doc_readyState()
    Test.ajax_complite
    try:
        i = len(e.remove_basket.return_all())
        for a in range(i):
            remove = e.remove_basket
            remove.wait = 10
            remove.click()
    except Exception as er:
        print(er)
    Test.get("https://confaelshop.ru" + "/catalog/komu/index.php?display=list")
    Test.doc_readyState()
    Test.ajax_complite
    return (True, "step 0")


def step_2(value, Test, el):
    Test.doc_readyState()
    Test.ajax_complite
    to_cart = None
    for a in range(10):
        print("добавление")
        el.price.index = a
        print(el.price.text)
        if el.price.text.replace(" ", "") == "" or int(el.price.text.replace(" ", "")) >= 1900:
            continue
        if Test.browser_name in ("ie", "c", "ff"):
            to_cart = el.to_cart
            to_cart.index = a
        else:
            to_cart = el.to_cart_
            to_cart.index = a
        if "display: none;" in to_cart.get_attribute("style"):
            continue
        # el.aria_count.click(double=True).input("backspase", button=True).input(value)
        to_cart.click()
        break
    return True


def step_3(Test, e):
    for a in range(4):
        Test.get("https://confaelshop.ru" + "/basket/")
        Test.doc_readyState()
        Test.ajax_complite
        return True


def step_4(Test , e):
    for a in range(8):
        e.basket_btn.click()
        if Test.driver.current_url == "https://confaelshop.ru" + "/order/":
            Test.doc_readyState()
            break
        sleep(0.5)
    return True


def step_5(input_order, Test, _el):
    def nav(e, action):
        if action == "next":
            e.next.click()
        else:
            e.prev.click()
        e.wait.in_page()

    def select(Test, el, _e):
        while True:
            try:
                sleep(2)
                id_ = ("bx-soa-basket", "bx-soa-region", "bx-soa-delivery", "bx-soa-pickup", "bx-soa-paysystem", "bx-soa-properties")
                select = str(elem(Test, "//div[contains(@class, 'bx-selected')]").get_attribute("id"))
                select_ = str(el.get_attribute("id"))
                print(id_.index(select) - id_.index(select_), id_.index(select), id_.index(select_), select, select_)
                if id_.index(select) - id_.index(select_) < 0:
                    nav(_e, "next")
                elif id_.index(select) - id_.index(select_) > 0:
                    nav(_e, "prev")
                else:
                    break
            except Exception as e:
                traceback.print_exc(file=sys.stdout)
                print(e, type(e))
    Test.doc_readyState()
    Test.ajax_complite
    for a in range(8):
        _el.wait.in_page()
        select(Test, elem(Test, "//div[@id='bx-soa-region']"), _el)
        sleep(2)
        try:
            region = elem(Test, "//div[@id='bx-soa-region']" + input_order[0][0], index=input_order[0][1])
            print(region.text)
            region_ = region.text.split(" ")[0]
            # break
        except Exception as e:
            print(e, type(e))
            sleep(2)
        try:
            region.click
            Test.doc_readyState()
            Test.ajax_complite
            _el.wait.in_page()
        except Exception as e:
            print(e, type(e))
            sleep(2)
    Test.doc_readyState()
    Test.ajax_complite
    for a in range(8):
        _el.wait.in_page()
        select(Test, elem(Test, "//div[@id='bx-soa-delivery']"), _el)
        sleep(2)
        try:
            coast_1 = elem(Test, "//div[@id='bx-soa-delivery']" + input_order[1][0], index=input_order[1][1])
            coast_2 = elem(Test, "//div[@id='bx-soa-delivery']//div[contains(@class, 'bx-selected')]//div[@class='bx-soa-pp-delivery-cost']")
            print(coast_1.text, coast_2.text)
            if coast_1.text == coast_2.text:
                break
        except Exception as e:
            print(e, type(e))
            sleep(2)
        try:
            coast_1.click()
            _el.wait.in_page()
            Test.doc_readyState()
            Test.ajax_complite
        except Exception as e:
            print(e, type(e))
            sleep(2)

    for a in range(8):
        _el.wait.in_page()
        select(Test, elem(Test, "//div[@id='bx-soa-paysystem']"), _el)
        Test.doc_readyState()
        Test.ajax_complite
        if input_order[2][0] == "skip":
            break
        try:
            title_1 = elem(Test, "//div[@id='bx-soa-paysystem']" + input_order[2][0], index=input_order[2][1])
            title_2 = elem(Test, "//div[@id='bx-soa-paysystem']//div[contains(@class, 'bx-selected')]//div[@class='bx-soa-pp-company-smalltitle']")
            print(title_1.text, title_2.text)
            if title_1.text == title_2.text:
                break
        except Exception as e:
            print(e, type(e))
            sleep(2)
        try:
            title_1.click()
            _el.wait.in_page()
            Test.doc_readyState()
            Test.ajax_complite
        except Exception as e:
            print(e, type(e))
            sleep(2)
    return True


def step_6(Test, _e):
    for a in range(5):
        try:
            elem(Test, "//a[contains(@class, 'js-giftd-checkout-finish')]").click()
        except MoveTargetOutOfBoundsException as e:
            print(e, type(e))
        try:
            Test.doc_readyState()
            Test.ajax_complite
            if elem(Test, "//h1[@id='pagetitle']").text == "Заказ сформирован":
                return True
        except Exception as e:
            print(e, type(e))
            sleep(2)
    else:
        return False

def run_script(product_count, input_order, Test):
    params = {
              "ipad": {
                       "y": (1024, 100)
                      },
              "galaxys5": {
                           "y": (640, 150)
                          },
              "c": {
                    "y": (999, 100)
                    },
              "ff": {
                     "y": (999, 100)
                    },
              "ie": {
                     "y": (999, 100)
                     }
                      }

    xpath = dict(
                 login="//input[@name='USER_LOGIN']",
                 password="//input[@name='USER_PASSWORD']",
                 alert="//div[starts-with(@class, 'alert')]",
                 basket_link="//a[@href='/basket/']",
                 to_cart="//div[contains(@class, 'catalog')]//div[contains(@class, 'button_block')]//span[contains(@class, 'to-cart')]",
                 to_cart_="//div[contains(@class, 'catalog')]//div[contains(@class, 'button_block')]//span[contains(@class, 'to-cart')]",
                 price="//div[contains(@class, 'list_item_wrapp')]//span[starts-with(@class, 'price_value')]",
                 aria_count="//div[contains(@class, 'list_item_wrapp')]//input[@name='quantity']",
                 next="//a[contains(@class, 'pull-right')]",
                 prev="//a[contains(@class, 'pull-left')]",
                 city="//div[contains(@class, 'active')]//a[@class='quick-location-tag']",
                 dostavka="//div[@id='bx-soa-delivery']//div[@class='bx-soa-pp-company-smalltitle']",
                 oplata="//div[@id='bx-soa-paysystem']//div[@class='bx-soa-pp-company-smalltitle']",
                 name="//input[@name='NAME']",
                 tel="//input[@name='PERSONAL_PHONE']",
                 email_="//input[@name='EMAIL']",
                 sumbit="//button[@name='Login']",
                 sumbit2="//a[contains(@class, 'js-giftd-checkout-finish')]",
                 error="//label[@class='error']",
                 success="//font[text()='Изменения сохранены']",
                 exit="//a[@href='/?logout=yes&login=yes']",
                 cheack_clear_basket="//div[@class='bx-sbb-empty-cart-text']",
                 remove_basket="//span[@class='basket-item-actions-remove']",
                 basket_btn="//button[@data-entity='basket-checkout-button']",
                 wait="//div[@id='loading_screen']"
                 )
    input_value = dict(
                       login="al8594212@gmail.com",
                       password="H6eLdspGkf55WcP"
                       )
    other = dict(
                 flag_dostavka=True,
                 flag_parse=True,
                 parse=dict(
                             city={
                                        "//a[@data-id='187']": (
                                                               (
                                                                "//input[@id='ID_DELIVERY_ID_3']",
                                                                "//input[@id='ID_DELIVERY_ID_26']"
                                                               ),
                                                               (
                                                                "//input[@id='ID_PAY_SYSTEM_ID_2']",
                                                                "//input[@id='ID_PAY_SYSTEM_ID_4']",
                                                                "//input[@id='ID_PAY_SYSTEM_ID_11']"
                                                               )
                                                              ),
                                        "//a[@data-id='18260']": (
                                                                 (
                                                                  "//input[@id='ID_DELIVERY_ID_3']",
                                                                  "//input[@id='ID_DELIVERY_ID_22']"
                                                                 ),
                                                                 (
                                                                  "//input[@id='ID_PAY_SYSTEM_ID_2']",
                                                                  "//input[@id='ID_PAY_SYSTEM_ID_4']",
                                                                  "//input[@id='ID_PAY_SYSTEM_ID_11']"
                                                                 )
                                                                ),
                                        "//a[@data-id='188']": (
                                                               (
                                                                "//input[@id='ID_DELIVERY_ID_206']",
                                                                "//input[@id='ID_DELIVERY_ID_189']",
                                                                "//input[@id='ID_DELIVERY_ID_214']",
                                                                "//input[@id='ID_DELIVERY_ID_4']",
                                                                "//input[@id='ID_DELIVERY_ID_31']"
                                                               ),
                                                               (
                                                                "//input[@id='ID_PAY_SYSTEM_ID_2']",
                                                                "//input[@id='ID_PAY_SYSTEM_ID_8']"
                                                               )
                                                              ),
                                        "//a[@data-id='19816']": (
                                                                 (
                                                                  ""
                                                                 )
                                                                )

                                   },
                             dostavka="""dict(
                                       "//input[@id='ID_DELIVERY_ID_3']",
                                       "//input[@id='ID_DELIVERY_ID_26']")""",

                             oplata=(

                                     "//input[@id='ID_PAY_SYSTEM_ID_11']"
                                     ),
                             ),
                 complite=dict(
                               city=[],
                               dostavka=[],
                               oplata=[]))
    """"//a[@data-id='19314']"
    "//a[@data-id='19676']",
    "//a[@data-id='20321']",
    "//a[@data-id='20732']"""
    temp = []
    try:
        result = []
        e = elements(Test, xpath)
        args = (Test, e)
        "открытие формы входа"
        flag = step_1(*args)
        print("step 1 ", flag)

        "переход к форме востановления пароля"
        flag = step_2(product_count, *args)
        print("step 2 ", flag)

        "ввод данных"
        flag = step_3(*args)
        print("step 3", flag)
        "Отправка формы"
        flag = step_4(*args)
        print("step_4 ", flag is True)

        "Проверка "
        flag = step_5(input_order, *args)
        print("step_5 ", flag)
        flag = step_6(*args)
        print("step_6 ", flag)
        return flag
        result.append(flag)
        print(result)

    except Exception as e:
        print("Тест в браузере {} завершен с ошибкой".format(Test.browser_name))
        print(e, type(e))
        traceback.print_exc(file=sys.stdout)
