import sys
import re
from time import sleep
import traceback
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
from src.PageObject import elements
import src.steps as steps
"""LOCATORS"""
locators = dict(
                search="input#title-search-input_fixed",
                product_link="div.block_list div.item-title a",
                retail="div.retailrocket-items",
                btn_to_order="div.button_block span[data-name=toorder]",
                mobile_btn_to_order="div.adaptive_button_buy span[data-name=toorder]",
                name="input[data-sid='CLIENT_NAME']",
                tel="input[data-sid='PHONE']",
                email="input[data-sid='EMAIL']",
                text="textarea[data-sid=MESSAGE]",
                next_page="a.flex-next",
                locator_form="div.form.TOORDER",
                sumbit="div.form.TOORDER input.btn",
                error="div.form.TOORDER label.error",
                success="div.success.form_result"
                )
"""LOCATORS"""

def step_1(Test):
    Test.get("https://confaelshop.ru/catalog/komu/index.php?display=table")
    return True


def step_2(Test):
    el = elements(Test, locators)
    if Test.browser_name in ("ie", "c", "ff", "ipad"):
        btn = el.btn_to_order
    else:
        btn = el.mobile_btn_to_order
    for a in range(30):
        btn.wait = 5
        flag = btn.in_page()
        if flag:
            Test.doc_readyState()
            Test.ajax_complite()
            btn.wait = 10
            btn.click()
            return True
        else:
            print("next page")
            el.next_page.click()
    else:
        return False


def step_3(Test, type_input):
    el = elements(Test, locators)
    if type_input == "None":
        name = ""
        tel = ""
        email = ""
        text = ""
        locator_result = locators["error"]
        count = 2
    elif type_input == "False":
        name = "Тестирование !@#$%^&*(123"
        tel = "Тестирование !@#$%^&*(123"
        email = "Тестирование !@#$%^&*(123"
        text = "Тестирование !@#$%^&*(123"
        locator_result = locators["error"]
        count = 1
    else:
        name = "Тестирование"
        tel = "90698989898"
        email = "al8594212@gmail.com"
        text = ""
        locator_result = locators["success"]
        count = 1

    el.tel.scroll = False
    el.tel.double_click()
    el.tel.input(tel, check=False)

    el.email.scroll = False
    el.email.double_click()
    el.email.input(email)

    el.name.scroll = False
    el.name.double_click()
    el.name.input(name)

    el.text.scroll = False
    el.text.double_click()
    el.text.input(text)
    el.sumbit.click()
    label = elem(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {locator_result}")
    if label.in_page():
        return len(label.return_all()) == count
    return False

if __name__ == "__main__":
    with driver_object("galaxys5") as Test:
        step_1(Test)
        step_2(Test)
        step_3(Test, "False")
