import sys
import re
from time import sleep, time
import traceback
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps
import src.mail as mail
"""LOCATORS"""
link_forgot = "a.forgot"
css_email = "input[type=email]"
alert = "div.alert"
css_sumbit = "div.but-r button.btn"
css_error = "label.error"
css_alert = "font.errortext"
css_succes = "font.notetext"
css_new_password = "input[name=NEW_PASSWORD]"
css_new_password_confirm = "input[name=NEW_PASSWORD_CONFIRM]"
"""LOCATORS"""


def step_1(Test):
    return steps.open_log_on(Test)


def step_2(Test, type_input):
    steps.log_on(Test, "user_two", check=False)
    if not elem(Test, alert).in_page():
        Test.get("https://confaelshop.ru/personal/change-password/")
        Test.doc_readyState()
        Test.ajax_complite
        return step_3(Test, "user_two_alt_password", type_input)
    steps.log_on(Test, "user_two_alt_password", check=False)
    Test.get("https://confaelshop.ru/personal/change-password/")
    Test.doc_readyState()
    Test.ajax_complite
    return step_3(Test, "user_two", type_input)


def step_3(Test, login, type_input):
    values = {"user_two": {
                          "name": "al8594212@gmail.com",
                          "password": "12345678",
                          },
              "user_two_alt_password": {
                                          "name": "al8594212@gmail.com",
                                          "password": "87654321",
                                            }
             }
    if type_input == "True":
        locator_result = css_succes
        password = (values[login]["password"], values[login]["password"])
    elif type_input == "False":
        locator_result = css_alert
        count = 1
        password = ("12334567890", "0987654321")
    else:
        locator_result = css_error
        count = 2
        password = ("", "")
    elem(Test, css_new_password).click().input(password[0])
    elem(Test, css_new_password_confirm).click().input(password[1])
    elem(Test, css_sumbit).click()
    if type_input == "True":
        Test.doc_readyState()
        Test.ajax_complite
        if Test.re(elem(Test, css_succes).find().text, "Изменения сохранены"):
            Test.get("https://confaelshop.ru/?logout=yes&login=yes")
            Test.doc_readyState()
            Test.ajax_complite
            steps.open_log_on(Test)
            return steps.log_on(Test, login)
    else:
        Errors = elem(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {css_error}", wait=15)
        if Errors.in_page():
            count_error = len(Errors.return_all())
            print(f'колво ошибок не совпадает с заданным {count_error} / {count}')
            return count_error == count
        print("не корректный ввод не верен")
        return False

if __name__ == "__main__":
    with driver_object("galaxys5") as Test:
        step_1(Test)
        step_2(Test, "True")
