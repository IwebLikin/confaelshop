import sys
import re
from time import sleep, time
import traceback
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps
import src.mail as mail
"""LOCATORS"""
link_forgot = "a.forgot"
css_email = "input[type=email]"
css_sumbit = "div.but-r button.btn"
css_error = "label.error"
css_succes = "font.notetext"
"""LOCATORS"""
def step_1(Test):
    return steps.open_log_on(Test)


def step_2(Test):
    for a in range(2):
        link = Test.driver.current_url
        elem(Test, link_forgot).click()
        if link == Test.driver.current_url:
            continue
        else:
            Test.doc_readyState()
            Test.ajax_complite
            return True
        return False

def step_3(Test, type_input):
    if type_input == "True":
        value = "al8594212@gmail.com"
    elif type_input == "None":
        value = ""
    else:
        value = "Тестирование !@#$%^&*(123"
    elem(Test, css_email).click().input(value)
    elem(Test, css_sumbit).click()
    if type_input == "True":
        return mail.get_mail_after_time(Test.time_start)
    else:
        return elem(Test, css_error).in_page()

if __name__ == "__main__":
    with driver_object("galaxys5") as Test:
        step_1(Test)
        step_2(Test)
        step_3(Test, "True")
