import requests
import bs4
from src.DriverObject import driver_object
ignore = r"^tel:|^#|^\/{1,2}$|\d+\/*$|\/filter\/|\?display|\?sort|\/upload\/|\/search\/|\/cdn-cgi\/|\/{2}kz|\/{2}ekb|\/{2}astana|\/{2}vgg|\/{2}spb|\/{2}mkh|\/{2}nn|\/{2}psk|\/{2}nsk|\/{2}rnd|\/{2}sarov|\/{2}obn"
pattern = r"http.*\/{2}.+\.(\w+)\..+|http.*\/{2}(\w+).+"

def parse(link):
    r = driver_object.re(link, pattern)
    parse.site_name = r.group(1) or r.group(2)
    parse.main_link = link
    parse.links = set()
    parse.links_info = set()
    parse.parse_links = set()
    parse.broken_links = set()
    parse.links.add(link)
    while True:
        print(f"update link set")
        for link in parse.links:
            print(f"get link {link}")
            if link in parse.parse_links:
                continue
            if link[0] == "/":
                link = parse.main_link + link
            response = requests.get(link)
            if not check_broken_link(response):
                dom = bs4.BeautifulSoup(response.text)
                parse_link(dom)
            parse.parse_links.add(link)
        else:
            if parse.links ^ parse.parse_links is None:
                return


def check_broken_link(response):
    if response.status_code == 200:
        return False
    else:
        print(f"add broken link {response.url}, code {response.status_code}")
        parse.broken_links.add(response.url)
        return True


def parse_link(dom):
    print(f"parse link")
    parse_link.result = set()
    for b in dom.find_all('a', href=True):
        _link = b.get("href")
        if _link in parse.links or len(_link) < 1:
            continue
        r = driver_object.re(_link, pattern)
        if r is not None:
            name = r.group(1) or r.group(2)
        else:
            name = ""
        r2 = driver_object.re(_link, ignore)
        if r2 is not None:
            continue
        elif _link[0] == "/" or parse.site_name in name:
            parse_link.result.add(_link)
            print("ссылка", _link)
        elif parse.site_name not in name:
            try:
                check_broken_link(requests.get(_link))
            except Exception:
                pass
    else:
        print(f"{len(parse.links)}, {len(parse.parse_links)}, {len(parse.broken_links)}")
        parse.links = parse.links | parse_link.result
        print(len(parse_link.result))


parse("https://confaelshop.ru")
