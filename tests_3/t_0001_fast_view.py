# -*- coding: utf-8 -*-
import sys
import re
from time import sleep
import traceback
from src.DriverObject import driver_object
from src.PageObject import page_element, elements
"""LOCATORS"""
locators = dict(
                product="div.catalog_item",
                button_fast_view="div.catalog_item div.fast_view_block",
                form_fast_view="div.fast_view_frame",
                currenr_img="div.slides li.current",
                fancebox="div.fancybox-stage",
                close_img="img.fancybox-image",
                search="input#title-search-input_fixed",
                left="span.flex-prev",
                right="span.flex-next",
                tittle_product="div.fast_view_frame div.title a",
                count="div.fast_view_frame input[name=quantity]",
                plus="div.fast_view_frame span.plus",
                minus="div.fast_view_frame span.minus",
                to_cart="div.fast_view_frame span.to-cart",
                like="div.fast_view_frame div.like_icons",
                count_like="a.delay span.js-basket-block span.count",
                count_basket="a.basket-count span.js-basket-block span.count",
                close_frame="div.fast_view_frame a.close"
                )
""""LOCATORS"""
type_locator = "css"
result = []

def step_1(Test):
    el = elements(Test, locators)
    print(el)
    result[:] = []
    link = 'https://confaelshop.ru/catalog/komu/'
    Test.get(link)
    Test.doc_readyState()
    Test.ajax_complite()
    el.button_fast_view.index = 10
    el.button_fast_view.message = "Нажатие на кнопку быстрый просмотр"
    el.button_fast_view.click(pause=1)
    for a in range(3):
        if Test.driver.current_url != 'https://confaelshop.ru/catalog/komu/':
            sleep(0.5)
            continue
    el.form_fast_view.message = "Ожидание открытия формы быстрый просмотр"
    el.form_fast_view.scroll = False
    el.form_fast_view.visible
    return True

def step_2(Test):
    link = 'https://confaelshop.ru/catalog/komu/'
    el = elements(Test, locators)
    el.currenr_img.message = "Открытие увел. изобр"
    el.currenr_img.scroll = False
    el.currenr_img.click(pause=1)

    el.fancebox.message = "Ожидание появления увел. изобр"
    el.fancebox.scroll = False
    el.fancebox.visible

    el.close_img.Message = "Закрытие увел. изобр"
    el.close_img.scroll = False
    el.close_img.click()

    el.fancebox.message = "Ожидание закрытия увел. изобр"
    return el.fancebox.invisible

def step_3(Test):
    el = elements(Test, locators)
    link = 'https://confaelshop.ru/catalog/komu/'
    el.tittle_product.message = "Получение ссылки товара"
    el.tittle_product.scroll = False

    el.count.message = "Ввод кол-ва товара"
    el.count.scroll = False

    el.to_cart.message = "добавление товара в корзину"
    el.to_cart.scroll = False
    el.count_basket.scroll = False
    one = Test.re(el.tittle_product.get_attribute("href"), r"\/(\d+)\/", group=1)

    el.left.message="Переключение товара влево"
    el.left.scroll = False
    el.left.click()

    tree = Test.re(el.tittle_product.get_attribute("href"), r"\/(\d+)\/", group=1)
    result.append(["Смена товара влево", one != tree])

    flag = el.count.double_click().input("backspase", button=True, check=False).input(2, return_check=True)
    result.append(["кол-во товара ручной ввод ручной ввод", flag])

    el.to_cart.click()
    flag = el.count_basket.text == "1"
    result.append(["Счетчик корзины + 1", flag])
    el.right.message = "Переключение товара вправо"
    el.right.scroll = False
    el.right.click()

    tree = Test.re(el.tittle_product.get_attribute("href"), r"\/(\d+)\/", group=1)

    result.append(["Смена товара вправо", one == tree])

    el.plus.scroll = False
    el.plus.click()
    result.append(("Кнопка плюс работает", el.count.get_attribute("value") == "2"))
    el.minus.scroll = False
    el.minus.click()
    result.append(("Кнопка минус работает", el.count.get_attribute("value") == "1"))

    el.to_cart.click()
    flag = el.count_basket.text == "2"
    result.append(["Счетчик корзины + 1", flag])

    el.like.scroll = False
    el.like.click()
    flag = el.count_basket.text == "1"
    result.append(["Счетчик корзины - 1", flag])
    el.count_like.scroll = False
    flag = el.count_like.text == "1"
    result.append(["Счетчик отложеного + 1", flag])

    el.close_frame.scroll = False
    el.close_frame.click()
    el.form_fast_view.message = "Ожидание Закрытия формы быстрый просмотр"
    el.form_fast_view.scroll = False
    el.form_fast_view.invisible
    result.append(["Окно быстрого просмотра закрылось", flag])
    print(result)
    return all(x[1] for x in result)
