import sys
import re
from time import sleep
import traceback
from src.DriverObject import driver_object
from src.PageObject import elements
import src.steps as steps
from src.PageObject import page_element
import pytest
from selenium.webdriver import ActionChains
"""LOCATORS"""
locators = dict(
    search="input#title-search-input_fixed",
    counter="div.counter_block input.text",
    to_cart="span.to-cart.btn",
    to_cart_alt="div.info_item span.to-cart.btn",
    plus="span.plus",
    minus="span.minus",
    product_title="div.item-title a",
    count_like="a.delay span.js-basket-block span.count",
    count_basket="a.has_prices span.js-basket-block span.count",
    mobile_count_basket="div.mobileheader-v1  a.basket span.count",
    mobile_count_like="div.mobileheader-v1  a.delay span.count",
    like_btn="div.item_slider div.like_icons.iblock",
    basket_btn="a.has_prices",
    mobile_basket_btn="a.basket-link.basket",
    delete="td.basket-items-list-item-remove span.basket-item-actions-remove",
    mobile_delete="span.basket-item-actions-remove",
    delete_all="span.delete_all",
    restore="a[data-entity=basket-item-restore-button]",
    empty="div.bx-sbb-empty-cart-container"
    )
""""LOCATORS"""
result = []


def step_1(Test):
    el = elements(Test, locators)
    result[:] = []
    Test.get("https://confaelshop.ru/catalog/komu/index.php?display=list")
    Test.doc_readyState()
    Test.ajax_complite()
    return True


def step_2(Test):
    el = elements(Test, locators)
    if Test.browser_name in ("ie", "c", "ff"):
        count_basket = el.count_basket
        count_like = el.count_like
    else:
        count_basket = el.mobile_count_basket
        count_like = el.mobile_count_like
    Test.ajax_complite()
    el.counter.index = 5
    el.counter.double_click()
    el.counter.input("backspase", button=True, check=False)
    el.counter.input(22, clear=False)
    count = el.counter.get_attribute("value")

    result.append(["Ручной ввод", count == "22"])
    el.to_cart.index = 5
    el.to_cart.click(pause=1)
    el.to_cart.is_displayed(_except="False")
    result.append(["+1 корзина ", count_basket.check("1")])
    try:
        e = page_element(Test, "div.sendsay-popup").find()
        Test.driver.execute_script("""
                          var element = arguments[0];
                          element.parentNode.removeChild(element);
                          """, e)
        e = page_element(Test, "div.giftd-small-wrapper").find()
        Test.driver.execute_script("""
                          var element = arguments[0];
                          element.parentNode.removeChild(element);
                          """, e)
    except Exception:
        pass
    el.counter.index = 6
    el.plus.index = 6
    el.plus.click()
    result.append(["+ - ввод", el.counter.get_attribute("value") == "2"])

    el.plus.click()
    result.append(["+ - ввод", el.counter.get_attribute("value") == "3"])

    el.minus.index = 6
    el.minus.click()
    result.append(["+ - ввод", el.counter.get_attribute("value") == "2"])

    el.to_cart.index = 6
    el.to_cart.click(pause=1)
    el.to_cart.is_displayed(_except="False")
    result.append(["+1 корзина", count_basket.check("2")])
    return True


def step_3(Test):
    el = elements(Test, locators)
    if Test.browser_name in ("ie", "c", "ff"):
        count_basket = el.count_basket
        count_like = el.count_like
    else:
        count_basket = el.mobile_count_basket
        count_like = el.mobile_count_like
    el.product_title.index = 8
    el.product_title.click()
    Test.doc_readyState()
    Test.ajax_complite()
    el.counter.double_click()
    el.counter.input("backspase", button=True, check=False)
    el.counter.input(2, clear=False)
    result.append(["Ручной ввод ", el.counter.get_attribute("value") == "2"])
    el.plus.click()
    result.append(["+ - ввод ", el.counter.get_attribute("value") == "3"])
    el.minus.click()
    result.append(["+ - ввод ", el.counter.get_attribute("value") == "2"])
    el.to_cart_alt.click(pause=1)
    el.to_cart_alt.is_displayed(_except="False")
    result.append(["+1 корзина", count_basket.check("3")])
    if Test.browser_name != "galaxys5":
        el.like_btn.click()
    return True

def step_4(Test):
    el = elements(Test, locators)
    if Test.browser_name in ("ie", "c", "ff"):
        count_basket = el.count_basket
        count_like = el.count_like
        el.basket_btn.click()
    else:
        el.mobile_basket_btn.index = 1
        el.mobile_basket_btn.click()
        count_basket = el.mobile_count_basket
        count_like = el.mobile_count_like
    Test.doc_readyState()
    Test.ajax_complite()
    Test.ajax_complite()
    if Test.browser_name != "galaxys5":
        result.append(["-1 корзина", count_basket.check("2")])
        result.append(["+1 отложенное", count_like.check("1")])

    if Test.browser_name != "galaxys5":
        el.delete.click()
    else:
        el.mobile_delete.click()
    result.append(["Удаление крестиком", el.restore.visible])
    Test.ajax_complite()
    if Test.browser_name in ("ie", "c", "ff"):
        el.delete_all.click()
        result.append(["Удаление всей корзины", el.empty.visible])
    print(result)
    return all(a[1] for a in result)
