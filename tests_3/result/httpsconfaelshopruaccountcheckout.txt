
<!doctype html>
<html lang="ru">
<head>
<link rel="icon" href="/img/favicon/favicon.ico" />
<link rel="icon" type="image/png" href="/img/favicon/favicon-32x32.png" />

<link rel="icon" sizes="192x192" href="/img/favicon/android-chrome-192x192.png">

<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/img/favicon/apple-touch-icon-180x180.html">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/favicon/apple-touch-icon-72x72-precomposed.html">

<link rel="apple-touch-icon-precomposed" href="/img/favicon/apple-touch-icon-precomposed.html">
<meta property="og:url" content="https://chocotelegram.ru/index.php" />
<meta property="og:title" content="Шокотелеграммы" />
<meta property="og:description" content="Уникальный подарок из шоколада" />
<meta property="og:image" content="https:///img/opengraph.png" />
<meta property="og:image:type" content="image/png" />
<meta property="og:image:width" content="840" />
<meta property="og:image:height" content="210" />
<meta property="twitter:card" content="summary" />
<meta property="twitter:title" content="Шокотелеграммы" />
<meta property="twitter:description" content="Уникальный подарок из шоколада" />
<meta property="twitter:image" content="https:///img/opengraph.png" />
<meta name="yandex-verification" content="d394a506d900f3ac" />
<script data-skip-moving="true" src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<title>Шокотелеграммы</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="index, follow" />
<meta name="keywords" content="шокотелеграммы, шоколадные подарки, конфеты ручной работы, наборы конфет с индивидуальным дизайном, подарочные наборы, наобр конфет" />
<meta name="description" content="Шокотелеграмма - индивидуальный подарок, который можно приурочить к любому событию. Набор конфет ручной работы поможет собрать послание, которое приятно удивит адресата." />
<script data-skip-moving="true">(function(w, d, n) {var cl = "bx-core";var ht = d.documentElement;var htc = ht ? ht.className : undefined;if (htc === undefined || htc.indexOf(cl) !== -1){return;}var ua = n.userAgent;if (/(iPad;)|(iPhone;)/i.test(ua)){cl += " bx-ios";}else if (/Android/i.test(ua)){cl += " bx-android";}cl += (/(ipad|iphone|android|mobile|touch)/i.test(ua) ? " bx-touch" : " bx-no-touch");cl += w.devicePixelRatio && w.devicePixelRatio >= 2? " bx-retina": " bx-no-retina";var ieVersion = -1;if (/AppleWebKit/.test(ua)){cl += " bx-chrome";}else if ((ieVersion = getIeVersion()) > 0){cl += " bx-ie bx-ie" + ieVersion;if (ieVersion > 7 && ieVersion < 10 && !isDoctype()){cl += " bx-quirks";}}else if (/Opera/.test(ua)){cl += " bx-opera";}else if (/Gecko/.test(ua)){cl += " bx-firefox";}if (/Macintosh/i.test(ua)){cl += " bx-mac";}ht.className = htc ? htc + " " + cl : cl;function isDoctype(){if (d.compatMode){return d.compatMode == "CSS1Compat";}return d.documentElement && d.documentElement.clientHeight;}function getIeVersion(){if (/Opera/i.test(ua) || /Webkit/i.test(ua) || /Firefox/i.test(ua) || /Chrome/i.test(ua)){return -1;}var rv = -1;if (!!(w.MSStream) && !(w.ActiveXObject) && ("ActiveXObject" in w)){rv = 11;}else if (!!d.documentMode && d.documentMode >= 10){rv = 10;}else if (!!d.documentMode && d.documentMode >= 9){rv = 9;}else if (d.attachEvent && !/Opera/.test(ua)){rv = 8;}if (rv == -1 || rv == 8){var re;if (n.appName == "Microsoft Internet Explorer"){re = new RegExp("MSIE ([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}else if (n.appName == "Netscape"){rv = 11;re = new RegExp("Trident/.*rv:([0-9]+[\.0-9]*)");if (re.exec(ua) != null){rv = parseFloat(RegExp.$1);}}}return rv;}})(window, document, navigator);</script>
<link href="/bitrix/js/main/core/css/core.css?15392173903963" type="text/css" rel="stylesheet" />
<link href="/css/desktop/app.css?1559732345192963" type="text/css" data-template-style="true" rel="stylesheet" />
<link href="/local/templates/chocotelegram/template_styles.css?15460829902231" type="text/css" data-template-style="true" rel="stylesheet" />

<script type='text/javascript' data-skip-moving='true'>
					var rrPartnerId = '5c86511097a5281d447d7a3b';
					var rrApi = {};
					var rrApiOnReady = rrApiOnReady || [];
					rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view =
						rrApi.recomMouseDown = rrApi.recomAddToCart = function() {};
					(function(d) {
						var ref = d.getElementsByTagName('script')[0];
						var apiJs, apiJsId = 'rrApi-jssdk';
						if (d.getElementById(apiJsId)) return;
						apiJs = d.createElement('script');
						apiJs.id = apiJsId;
						apiJs.async = true;
						apiJs.src = '//cdn.retailrocket.ru/content/javascript/api.js';
						ref.parentNode.insertBefore(apiJs, ref);
					}(document));
				</script>



<meta name="csrf-token" content="SWIT9suy2NrvVKdzST6KqLsCk4viNPN5AAg8i2Sd">
<meta charset="UTF-8">
<meta name="viewport" content="width=1024, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" href="/img/fav.html" type="image/png">
</head>
<body class="index" data-page="index">
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PNFPR7"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<symbol id="arrow-direct" viewBox="0 0 24 24">
<style>.ast0 {
                fill: none;
                stroke: #ab683b;
                stroke-width: 4;
                stroke-linecap: round;
                stroke-miterlimit: 10
            }</style>
<path class="ast0" d="M19.5 12.5H2M16 17.5l5-5-5-5" />
</symbol>
<symbol viewBox="0 0 50 50" id="arrow-triangle">
<path d="M20.1 20.9h10l-5 8z" />
</symbol>
<symbol id="back" viewBox="0 0 21 24">
<style>.cst0 {
                fill: none;
                stroke-width: 4;
                stroke-linecap: round;
                stroke-miterlimit: 10
            }</style>
<path class="cst0" d="M4.8 7.5h7c3.9 0 7 3.1 7 7v.5c0 3.9-3.2 7-7 7h-7" />
<path class="cst0" d="M9.2 2.2L3.8 7.5l5.4 5.2" />
</symbol>
<symbol viewBox="0 0 20 20" id="basket">
<path d="M16 16a2 2 0 1 0 2 2 2 2 0 0 0-2-2zM0 1a1 1 0 0 0 1 1h1l3.6 7.6L4.3 12a1.9 1.9 0 0 0-.3 1 2 2 0 0 0 2 2h11a1 1 0 1 0 0-2H6.4a.2.2 0 0 1-.2-.2l.9-1.8h7.5a2 2 0 0 0 1.7-1l3.6-6.5A1 1 0 0 0 20 3a1 1 0 0 0-1-1H4.2L3.7.9A1.7 1.7 0 0 0 2.3 0H1a1 1 0 0 0-1 1zm6 15a2 2 0 1 0 2 2 2 2 0 0 0-2-2z" fill-rule="evenodd" />
</symbol>
<symbol viewBox="0 0 16 16" id="category">
<path d="M3 4H1c-.6 0-1-.4-1-1V1c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1z" />
<path d="M3 4H1c-.6 0-1-.4-1-1V1c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM15 4h-2c-.6 0-1-.4-1-1V1c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM9 4H7c-.6 0-1-.4-1-1V1c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM3 16H1c-.6 0-1-.4-1-1v-2c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM15 16h-2c-.6 0-1-.4-1-1v-2c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM9 16H7c-.6 0-1-.4-1-1v-2c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1z" />
<path d="M3 16H1c-.6 0-1-.4-1-1v-2c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM15 16h-2c-.6 0-1-.4-1-1v-2c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM9 16H7c-.6 0-1-.4-1-1v-2c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM3 10H1c-.6 0-1-.4-1-1V7c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM15 10h-2c-.6 0-1-.4-1-1V7c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1zM9 10H7c-.6 0-1-.4-1-1V7c0-.6.4-1 1-1h2c.6 0 1 .4 1 1v2c0 .6-.4 1-1 1z" />
</symbol>
<symbol viewBox="0 0 13 13" id="close">
<path d="M3.1 12.2c-.3.3-.9.3-1.3 0L.5 10.9c-.3-.3-.3-.9 0-1.3l3.3-3.3L.5 3.1c-.3-.3-.3-.9 0-1.3L1.8.5c.3-.3.9-.3 1.3 0l3.3 3.3L9.7.5c.3-.3.9-.3 1.3 0l1.3 1.3c.3.3.3.9 0 1.3L8.9 6.4l3.3 3.3c.3.3.3.9 0 1.3l-1.3 1.3c-.3.3-.9.3-1.3 0L6.4 8.9l-3.3 3.3z" />
</symbol>
<symbol id="delete" viewBox="0 0 13 13">
<style>.gst0 {
                fill-rule: evenodd;
                clip-rule: evenodd
            }</style>
<path class="gst0" d="M3.1 12.2c-.3.3-.9.3-1.3 0L.5 10.9c-.3-.3-.3-.9 0-1.3l3.3-3.3L.5 3.1c-.3-.3-.3-.9 0-1.3L1.8.5c.3-.3.9-.3 1.3 0l3.3 3.3L9.7.5c.3-.3.9-.3 1.3 0l1.3 1.3c.3.3.3.9 0 1.3L8.9 6.4l3.3 3.3c.3.3.3.9 0 1.3l-1.3 1.3c-.3.3-.9.3-1.3 0L6.4 8.9l-3.3 3.3z" />
</symbol>
<symbol viewBox="0 0 122.9 66.2" id="logo"><title>Vector Smart Object</title>
<path d="M122.3 59.6H121l.6-4a3 3 0 0 0-.5-2.1 2.9 2.9 0 0 0-2-1.1 3.1 3.1 0 0 0-3.1 1.4l-.2-.3a2.9 2.9 0 0 0-2-1.1 2.8 2.8 0 0 0-2.2.6v-.3l-.5-.2h-1.9a.65.65 0 1 0 0 1.3h1.1l-1 5.8H108a.65.65 0 1 0 0 1.3h3.8a.65.65 0 1 0 0-1.3h-1.2l.7-3.7v-.3c0-.1.6-2.3 2.4-2a1.5 1.5 0 0 1 1.4 1.8l-.7 4.7a.6.6 0 0 0 .1.5l.5.2h2a.65.65 0 0 0 0-1.3h-1.3l.5-3.6a.6.6 0 0 0 .3-.4c0-.1.6-2.3 2.4-2a1.5 1.5 0 0 1 1.4 1.8l-.7 4.7a.6.6 0 0 0 .1.5l.5.2h2a.65.65 0 0 0 0-1.3M106.4 59.5h-1l.2-.7c.6-2.2 1.4-4.9-.5-6.1s-5.1-.1-5.2-.1a.65.65 0 0 0 .5 1.2s2.4-1.1 3.9-.1a2 2 0 0 1 .6 2 5.1 5.1 0 0 0-4.9.1 2.7 2.7 0 0 0-1.4 2.4 2.8 2.8 0 0 0 1.5 2.4 3.2 3.2 0 0 0 1.5.3 5.2 5.2 0 0 0 2.4-.7v.2l.5.2h1.9a.65.65 0 1 0 0-1.3m-5.8 0a1.5 1.5 0 0 1-.8-1.3 1.5 1.5 0 0 1 .8-1.3 3.9 3.9 0 0 1 4.1.2l-.4 1.4v.2h-.3c-1.3.9-2.5 1.2-3.4.7M98.2 52.4a4.6 4.6 0 0 0-3.8 1.1v-.3a.6.6 0 0 0-.1-.5l-.5-.2h-1.9a.65.65 0 0 0 0 1.3H93l-.3 1.9v.2l-.7 3.6h-1.4a.65.65 0 1 0 0 1.3h3.8a.65.65 0 1 0 0-1.3h-1.1l.6-3.5a4.4 4.4 0 0 1 3.4-2.4l-.2 1.4a.658.658 0 1 0 1.3.2l.3-2a.6.6 0 0 0-.5-.7M91.1 53.2a.6.6 0 0 0-.6-.6h-2a.6.6 0 0 0-.6.5l-.2.9-.2-.3a4.1 4.1 0 0 0-3.1-1.3 4.7 4.7 0 0 0-4.7 4.2 3.9 3.9 0 0 0 1 3 4.1 4.1 0 0 0 3 1.3 4.7 4.7 0 0 0 2.8-1l-.4 2.5a3 3 0 0 1-2.8 2.5 2.1 2.1 0 0 1-1.6-.6 1.9 1.9 0 0 1-.6-1.4.65.65 0 0 0-1.3 0 3.2 3.2 0 0 0 1 2.3 3.4 3.4 0 0 0 2.3.9h.1a4.3 4.3 0 0 0 4-3.5l1.8-8.8h1.5a.6.6 0 0 0 .6-.6m-7.4 6.5a2.8 2.8 0 0 1-2.1-.9 2.7 2.7 0 0 1-.7-2 3.4 3.4 0 0 1 3.4-3 2.8 2.8 0 0 1 2.1.9 2.7 2.7 0 0 1 .7 2 3.4 3.4 0 0 1-3.4 3M74.9 57.5c1.9 0 3.3-.4 3.9-1.2a2.2 2.2 0 0 0 .4-1.9 2.8 2.8 0 0 0-3-2.2 4.8 4.8 0 0 0-3.2 1.4 4.1 4.1 0 0 0-1.2 2.5 3.8 3.8 0 0 0 2.5 4.4l1.7.4a4.3 4.3 0 0 0 3.2-1.5.64.64 0 1 0-1-.8 3.1 3.1 0 0 1-3.4.8 2.3 2.3 0 0 1-1.7-2H75zm-1-3a3.5 3.5 0 0 1 2.4-1 1.6 1.6 0 0 1 1.7 1.1 1 1 0 0 1-.2.9c-.4.5-1.4.7-2.9.7h-1.8a2.8 2.8 0 0 1 .8-1.7z" fill="#531000" fill-rule="evenodd" />
<path d="M70.9 59.4l-.7.2a1.2 1.2 0 0 1-1.2-1.1l1.7-9.3a.6.6 0 0 0-.1-.5l-.5-.2h-1.9a.65.65 0 1 0 0 1.3h1.1l-1.5 8.6a2.4 2.4 0 0 0 2.4 2.4 2.4 2.4 0 0 0 1.5-.5.64.64 0 0 0-.8-1M62.8 57.5c1.9 0 3.3-.4 3.9-1.2a2.2 2.2 0 0 0 .4-1.9 2.8 2.8 0 0 0-3-2.2 4.8 4.8 0 0 0-3.3 1.4 4.1 4.1 0 0 0-1.2 2.5 3.8 3.8 0 0 0 2.5 4.4l1.6.3a4.3 4.3 0 0 0 3.3-1.4.64.64 0 1 0-1-.8 3.1 3.1 0 0 1-3.4.8 2.3 2.3 0 0 1-1.7-2h1.9zm-1-3a3.5 3.5 0 0 1 2.4-1 1.6 1.6 0 0 1 1.7 1.1 1 1 0 0 1-.2.9c-.4.5-1.4.7-2.9.7H61a2.8 2.8 0 0 1 .7-1.7z" fill="#531000" fill-rule="evenodd" />
<path d="M56.9 59.5h-1.2l1.7-9.6a18.5 18.5 0 0 0 3.8.3c2.3-.1 3.6-1.1 3.9-2.8a.658.658 0 1 0-1.3-.2c-.1.7-.6 1.6-2.8 1.7a25.3 25.3 0 0 1-5.5-.6l-3.1-.5a3.4 3.4 0 0 0-3.8 2.8.6.6 0 0 0 .6.7h.1a.6.6 0 0 0 .6-.6 2.1 2.1 0 0 1 2.5-1.7l3 .4h.8l-1.7 9.8H53a.65.65 0 1 0 0 1.3h3.8a.65.65 0 1 0 0-1.3M27.6 52.4a4.7 4.7 0 0 0-4.7 4.2 3.9 3.9 0 0 0 1 3 4.1 4.1 0 0 0 3 1.3 4.7 4.7 0 0 0 4.7-4.2 3.9 3.9 0 0 0-1-3 4.1 4.1 0 0 0-3.1-1.3m-.5 7.3a2.8 2.8 0 0 1-2.1-.9 2.7 2.7 0 0 1-.7-2 3.4 3.4 0 0 1 3.4-3 2.8 2.8 0 0 1 2.1.9 2.7 2.7 0 0 1 .7 2 3.4 3.4 0 0 1-3.4 3M40.4 52a.6.6 0 0 0-.7.5v.5a4.2 4.2 0 0 0-2.2-.6 4.7 4.7 0 0 0-4.7 4.2 3.9 3.9 0 0 0 1 3 4.1 4.1 0 0 0 3 1.3 4.7 4.7 0 0 0 3.1-1.2.602.602 0 1 0-.8-.9 3.4 3.4 0 0 1-2.3.9 2.8 2.8 0 0 1-2.1-.9 2.6 2.6 0 0 1-.7-2 3.4 3.4 0 0 1 3.4-3 2.8 2.8 0 0 1 2 .8l-.4 1.5a.6.6 0 0 0 .5.7h.1a.6.6 0 0 0 .6-.5l.7-3.6a.6.6 0 0 0-.5-.7M46.2 52.4a4.7 4.7 0 0 0-4.7 4.2 3.9 3.9 0 0 0 1 3 4.1 4.1 0 0 0 3 1.3 4.7 4.7 0 0 0 4.7-4.2 3.9 3.9 0 0 0-1-3 4.1 4.1 0 0 0-3.1-1.3m-.6 7.2a2.8 2.8 0 0 1-2.1-.9 2.7 2.7 0 0 1-.7-2 3.4 3.4 0 0 1 3.4-3 2.8 2.8 0 0 1 2.1.9 2.7 2.7 0 0 1 .7 2 3.4 3.4 0 0 1-3.4 3M21.9 59.4h-1.2v-.2c.5-2.7 1.1-6.1-1.7-6.9a3.8 3.8 0 0 0-3.2.6l.7-3.8a.6.6 0 0 0-.1-.5l-.5-.2H14a.65.65 0 1 0 0 1.3h1.1l-.8 4.9a.6.6 0 0 0-.1.4l-.8 4.5H12a.65.65 0 0 0 0 1.3h3.8a.65.65 0 1 0 0-1.3h-1.1l.8-4.5c.4-.5 1.5-2 3.3-1.5s1.3 2.5.8 5.4l-.2.9a.6.6 0 0 0 .1.5l.5.3h1.9a.65.65 0 1 0 0-1.3M10.3 59.1c-2.6 1.6-5 1.9-6.8.8a5.3 5.3 0 0 1-2.2-5.1 6.4 6.4 0 0 1 6-5.9 3.5 3.5 0 0 1 3.2 1.6.6.6 0 0 0 .4.3l-.3 2.2a.6.6 0 0 0 .5.7h.1a.6.6 0 0 0 .6-.5l.8-4.7a.658.658 0 1 0-1.3-.2l-.2 1a4.9 4.9 0 0 0-3.9-1.6 7.7 7.7 0 0 0-7.3 7A6.6 6.6 0 0 0 2.8 61a5.7 5.7 0 0 0 3.1.9 9.4 9.4 0 0 0 5-1.7.652.652 0 0 0-.7-1.1" fill="#531000" fill-rule="evenodd" />
<path d="M39 47.1a11.7 11.7 0 0 1-6.7-1.9c-3.4-2.3-5.2-6.2-6.1-9.6-3.9.5-7.6.5-9.4-.6a2.3 2.3 0 0 1-.7-3.5 2.8 2.8 0 0 1 1.1-.7 5.5 5.5 0 0 1-2.8-2.3 2.6 2.6 0 0 1 .1-2.5 2.7 2.7 0 0 1 2.1-1.6 4.5 4.5 0 0 1 2.9 1 4.6 4.6 0 0 1-.1-3.3 2.3 2.3 0 0 1 2-1.2c3-.3 3.9 3.8 4.5 6.9l.3 1.5.3 1.4c.2.9.4 2.1.7 3.4l1.7-.3a16.2 16.2 0 0 1-.1-2.6 16.4 16.4 0 0 0-2.4-10.3c-.8-1.4-1.5-2.6-.5-3.8a2.7 2.7 0 0 1 1.9-1.1 4 4 0 0 1 1.7.2c-1.1-1.8-2.3-4.3-2.2-6.1a2.3 2.3 0 0 1 .7-1.7 2.8 2.8 0 0 1 2.1-.7l.9.3C29.4 2.3 33.5.1 33.6.1a.7.7 0 0 1 1 .6c.2 3 1.5 5.4 4.8 9 4.2 5.3 1.8 11.7-.4 17.4l-1 2.6-.2.8a32.8 32.8 0 0 0 4.4-3.8c1.8-1.7 3.3-3.3 5.2-3.8a4 4 0 0 1 3.3.4 4.8 4.8 0 0 1 2.2 2.7.7.7 0 0 1-.7.8 1.7 1.7 0 0 0-1.3.3c-1 .8-1.2 3.1-1.4 4.6V33c-.3 2.6-1 10.7-10.2 10.7H39a9.7 9.7 0 0 1-7.3-2.9 11.5 11.5 0 0 1-2.7-5.7l-1.5.3C28.4 38.5 30 42 33 44s5.5 2.1 9.2 1.4a.7.7 0 1 1 .3 1.3 17.7 17.7 0 0 1-3.5.4m-8.7-12.3a10.2 10.2 0 0 0 2.4 5 8.4 8.4 0 0 0 6.3 2.5c8.1.1 8.7-6.8 9-9.4v-1.3c.2-1.8.4-4.3 1.9-5.5a2.8 2.8 0 0 1 1.2-.6 3.7 3.7 0 0 0-1.1-1.2 2.6 2.6 0 0 0-2.2-.3c-1.6.5-3 1.9-4.7 3.5a29.8 29.8 0 0 1-5.6 4.6 5.7 5.7 0 0 0 2.6 5.3.7.7 0 0 1-.7 1.2 7.1 7.1 0 0 1-3.2-5.6 17.6 17.6 0 0 1-3.1 1.2l-2.7.7m-10.9-3.2a3.8 3.8 0 0 0-2.5.7.9.9 0 0 0-.1.9 1 1 0 0 0 .5.5c1.3.8 4.5.9 8.4.4-.3-1.3-.5-2.4-.7-3.3l-.3-1.3-.3-1.5c-.5-2.4-1.3-6-3-5.8a1 1 0 0 0-.9.5c-.6 1.1.8 4 1.5 5.2a.7.7 0 0 1-1.1.8c-1.2-1.4-3-3.1-4.3-2.9a1.3 1.3 0 0 0-1.1.9 1.3 1.3 0 0 0-.1 1.2c.7 1.4 3.8 2.3 5 2.5a.7.7 0 0 1-.2 1.3h-.8m8.4-14.5h-.2a1.3 1.3 0 0 0-.9.5c-.4.5-.2.9.6 2.3a17.6 17.6 0 0 1 2.6 11 14.7 14.7 0 0 0 .1 2.3l2.6-.6a16.3 16.3 0 0 0 3.5-1.4 11 11 0 0 1 .5-2.2l1-2.6c2.1-5.6 4.3-11.4.6-16.1s-4.6-5.8-5-8.6c-1 1-2.4 3.4-.2 7.8a.7.7 0 0 1-1.1.7A4 4 0 0 0 29.7 9a1.4 1.4 0 0 0-1.1.4.9.9 0 0 0-.3.7c-.1 2.3 3 6.9 3.6 7.5a.7.7 0 0 1-.9 1 6 6 0 0 0-3-1.4M62 26.7l-4.1-3.1a.6.6 0 0 0-.9.4l-.3 5-4.6 1.8a.6.6 0 0 0-.1 1l4.1 3h.4l.4-.2 5.3-7.1a.6.6 0 0 0-.2-.8zm-5.7 6.8l-2.8-2.1 4-1.5a.6.6 0 0 0 .4-.5l.2-4.2 2.8 2.1z" fill="#f6931d" fill-rule="evenodd" />
<path d="M52.8 30h-.3a.6.6 0 0 1-.1-.8l3-4.1a.6.6 0 1 1 .9.7l-3 4.1-.5.2" fill="#f6931d" fill-rule="evenodd" />
</symbol>
<symbol id="mastercard" viewBox="0 0 36 24">
<defs>
<style>.icls-1 {
                    fill-rule: evenodd
                }</style>
</defs>
<title>Vector Smart Object4</title>
<path class="icls-1" d="M2 3.25A1.25 1.25 0 0 1 3.25 2h29.5A1.25 1.25 0 0 1 34 3.25v17.5A1.25 1.25 0 0 1 32.75 22H3.25A1.25 1.25 0 0 1 2 20.75V3.25zM0 2.5A2.5 2.5 0 0 1 2.5 0h31A2.5 2.5 0 0 1 36 2.5v19a2.5 2.5 0 0 1-2.5 2.5h-31A2.5 2.5 0 0 1 0 21.5v-19z" />
<path class="icls-1" d="M19.25 14.91a6 6 0 1 1 0-5.81 4 4 0 0 0 0 5.81z" />
<path class="icls-1" d="M22 16a4 4 0 1 0-4-4 4 4 0 0 0 4 4zm0 2a6 6 0 1 0-6-6 6 6 0 0 0 6 6z" />
</symbol>
<symbol viewBox="0 0 20 20" id="menu"><title>Vector Smart Object2</title>
<path d="M1 15c0-.6.4-1 1-1h16c.6 0 1 .4 1 1s-.4 1-1 1H2c-.6 0-1-.4-1-1zm0-5c0-.6.4-1 1-1h16c.6 0 1 .4 1 1s-.4 1-1 1H2c-.6 0-1-.4-1-1zm0-5c0-.6.4-1 1-1h16c.6 0 1 .4 1 1s-.4 1-1 1H2c-.6 0-1-.4-1-1z" />
</symbol>
<symbol id="paypal" viewBox="0 0 36 24">
<defs>
<style>.kcls-1 {
                    fill-rule: evenodd
                }</style>
</defs>
<title>Vector Smart Object1</title>
<path class="kcls-1" d="M2 3.25A1.25 1.25 0 0 1 3.25 2h29.5A1.25 1.25 0 0 1 34 3.25v17.5A1.25 1.25 0 0 1 32.75 22H3.25A1.25 1.25 0 0 1 2 20.75V3.25zM0 2.5A2.5 2.5 0 0 1 2.5 0h31A2.5 2.5 0 0 1 36 2.5v19a2.5 2.5 0 0 1-2.5 2.5h-31A2.5 2.5 0 0 1 0 21.5v-19z" />
<path class="kcls-1" d="M23.19 8.53a4 4 0 0 1-.12.95 4.81 4.81 0 0 1-1.3 2.29A2 2 0 0 1 20 12.95a6.88 6.88 0 0 1-2.51.45H16l-.61 2.46-.19.74h-.9l-.4 1.4h3.26l.84-3.2h2.34a4.45 4.45 0 0 0 4.6-3.17 2.43 2.43 0 0 0-1.75-3.1zm-7-.57h1.6a1 1 0 0 1 1.11 1.33 2 2 0 0 1-1.88 1.33h-1.48zM13.47 6L11 15.67h3.27l.8-3.2h2.34A4.43 4.43 0 0 0 22 9.29C22.56 7 20.67 6 19 6h-5.53z" />
</symbol>
<symbol id="visa" viewBox="0 0 36 24">
<defs>
<style>.lcls-1 {
                    fill-rule: evenodd
                }</style>
</defs>
<title>Vector Smart Object2</title>
<path class="lcls-1" d="M2 3.25A1.25 1.25 0 0 1 3.25 2h29.5A1.25 1.25 0 0 1 34 3.25v17.5A1.25 1.25 0 0 1 32.75 22H3.25A1.25 1.25 0 0 1 2 20.75V3.25zM0 2.5A2.5 2.5 0 0 1 2.5 0h31A2.5 2.5 0 0 1 36 2.5v19a2.5 2.5 0 0 1-2.5 2.5h-31A2.5 2.5 0 0 1 0 21.5v-19z" />
<path class="lcls-1" d="M25.56 10.34l-.21.53-.65 1.55H26l-.37-1.6zM8 9.5h2.6a.67.67 0 0 1 .73.47l.57 2.55A5.28 5.28 0 0 0 8 9.61zm3 5.5l-1.33-4.37a4.63 4.63 0 0 1 2.19 2.26l.09.39 1.58-3.78h1.71L12.7 15H11zm3.69 0l1-5.55h1.62L16.3 15h-1.62zm4.31.23a5.48 5.48 0 0 1-1.8-.3l.23-1.24.21.09a3.57 3.57 0 0 0 1.52.29.84.84 0 0 0 1-.54 1 1 0 0 0-.84-.69 1.89 1.89 0 0 1-1.42-1.5c0-1.08 1.13-1.84 2.73-1.84a4.48 4.48 0 0 1 1.37.24l-.22 1.2-.15-.06a3 3 0 0 0-1.21-.21c-.64 0-.93.24-.93.48a1.22 1.22 0 0 0 .91.68 1.73 1.73 0 0 1 1.36 1.51c.03 1.15-1.07 1.89-2.76 1.89zm7.5-.23l-.19-.83h-2.08l-.34.83H22.2l2.4-5a.81.81 0 0 1 .85-.46h1.25L28 15h-1.5z" />
</symbol>
<symbol id="wallet" viewBox="0 0 30.86 23.75">
<defs>
<style>.mcls-1 {
                    fill-rule: evenodd
                }</style>
</defs>
<title>Vector Smart Object</title>
<path class="mcls-1" d="M26.57 11.45h1.71v5.94h-1.71v-5.94zM1.71 7.85a1.07 1.07 0 0 1 1.07-1.06H25.5a1.06 1.06 0 0 1 1.07 1.06V21a1.07 1.07 0 0 1-1.07 1.06H2.78A1.06 1.06 0 0 1 1.71 21V7.85zM0 5.94a.85.85 0 0 1 .85-.85h25.29a2.13 2.13 0 0 1 2.14 2.12v14.42a2.13 2.13 0 0 1-2.14 2.12h-24A2.13 2.13 0 0 1 0 21.63V5.94z" />
<path class="mcls-1" d="M21 15.69a1.27 1.27 0 1 0-1.29-1.27A1.28 1.28 0 0 0 21 15.69zm0 1.7a3 3 0 1 0-3-3 3 3 0 0 0 3 3z" />
<path class="mcls-1" d="M23.78 11.45h4.93a2.13 2.13 0 0 1 2.15 2.12v1.7a2.13 2.13 0 0 1-2.15 2.12h-4.93a4.64 4.64 0 0 0 .9-1.7h3.39a1.07 1.07 0 0 0 1.07-1.06v-.42a1.07 1.07 0 0 0-1.07-1.06h-3.39a4.64 4.64 0 0 0-.9-1.7zM1.71 5.94a1.31 1.31 0 0 1 1.14-1.17l18.3-3.06a.94.94 0 0 1 1.14.95v3.28H1.71zM0 5.54a2.75 2.75 0 0 1 2.28-2.62L21.72 0A1.92 1.92 0 0 1 24 2v4H0v-.46z" />
</symbol>
</svg>
<nav class="slide-menu">
<div class="slide-menu-inner">
<div class="top-bar">
<a class="logo" href="/"></a>
<div class="right">
<a href="#" class="btn ghost icon menu">
<svg>
<use xlink:href="#arrow-direct"></use>
</svg>
</a>
</div>
</div>
<ul>
<li><a href="/constructor/">Создать шокотелеграмму</a></li>
<li><a href="/our-sweets/">Наши конфеты</a></li>
<li><a href="/how-it-works/">Как это работает</a></li>
<li><a href="/business-gifts/">Подарки для бизнеса</a></li>
<li><a href="/faq/">FAQ и контакты</a></li>
<li><a href="/login/">Личный кабинет</a></li>
</ul>
</div>
</nav>
<nav role="navigation">
<a class="logo" href="/"></a>
<div class="right index">
<a href="/login/" class="btn ghost">личный кабинет</a>
<a href="/account/checkout/" class="btn ghost icon basket">
<svg>
<use xlink:href="#basket"></use>
</svg>
</a>
<a href="#" class="btn ghost icon menu">
<svg>
<use xlink:href="#menu"></use>
</svg>
</a>
<a href="/constructor/" class="btn primary index_create_tel_btn_menu">Создать подарок</a>
</div>
</nav>
<video id="video" preload="preload" muted="muted">
<source type="video/mp4" src="/video/new4/choko-ru_4.mp4">
<p>Your browser does not support HTML5 video.</p>
</video>
<video id="video-reverse" preload="preload" muted="muted">
<source type="video/mp4" src="/video/new4/choko-ru_4-reverse.mp4">
<p>Your browser does not support HTML5 video.</p>
</video>
<div class="slide" id="scene-1">
<h1>После прочтения<br> съесть!</h1>
<p class="scene-1_text">Добро пожаловать на сайт ChocoTelegram!
<br>Здесь вы можете составить телеграмму из шоколада — и отправить её в любую точку страны.
</p> <div class="index_create_tel_btn_bottom-wrapper">
<a href="/constructor/" class="btn primary index_create_tel_btn_bottom">Соберите свой подарок</a> </div>
</div>
<div class="slide about" id="scene-2">
<div class="info">
<h2>36 букв — все ваши!</h2>
<p>Составьте телеграмму из шоколада — и мы отправим<br>
её вашим любимым и друзьям в любую точку страны.</p>
<a href="/constructor/" class="btn primary">Заказать</a> </div>
</div>
<div class="slide inside" id="scene-3">
<div class="info">
<h2>Алфавит на любой<br>
вкус и цвет</h2>
<p>У нас есть три вида шоколада, пять вкусных начинок<br> и цветной алфавит, в котором все буквы выглядят
так,<br> как их видел самый известный синестетик в мире&nbsp;&mdash;<br> Владимир Владимирович Набоков.
</p>
<a href="/our-sweets/" class="btn primary">Посмотреть все вкусы</a> </div>
<div class="tooltip top">
<h3>Шоколад</h3>
<p>Все шокотелеграммы изготавливаются из<br>
знаменитого тёмного, молочного и белого<br>
элитного шоколада «Конфаэль».</p> </div>
<div class="tooltip bottom">
<h3>Начинка</h3>
<p>Начинки из натуральных ягод и фруктов<br>
не дадут заскучать за чтением приятных слов!</p> </div>
</div>
<div class="slide outside" id="scene-4">
<h2>Подарок с<br>
приятной<br>
наружностью</h2>
<p>5 тематических упаковок позволят<br>
нарядить шокотелеграмму на любой<br>
случай жизни.</p> </div>
<div class="slide shipment" id="scene-5">
<h2>Слова, которые<br>
всегда кстати</h2>
<p>Наши курьеры доставят шокотелеграмму<br>
точно в нужный день &mdash; вне зависимости<br>
от ситуации на дорогах и погоды за окном.</p> </div>
<div class="slide constructor" id="scene-6">
<h2>Соберите свою<br>
шокотелеграмму!</h2>
<p>Шокотелеграммы &mdash;<br>
это весело, романтично и нелитературно вкусно!</p>
<a href="/constructor/" class="btn primary mod_slide-7">Соберите свой подарок</a>
<br />
<a class="btn primary btn_additional show-video">Посмотреть видео</a> </div>
<div class="mouse">
<img src="/img/mouse.png">
<h4>SCROLL</h4>
</div>
<section class="index_navigate-dot">
<ul class="index_navigate-dot_list">
<li class="dot_list_item active_dot_list_item"></li>
<li class="dot_list_item"></li>
<li class="dot_list_item"></li>
<li class="dot_list_item"></li>
<li class="dot_list_item"></li>
<li class="dot_list_item"></li>
<li class="dot_list_item"></li>
</ul>
</section>
<section class="index_login-personal-area login">
<div class="index_login-personal-area_content">
<img class="index_login-personal-area_btn-close" src="/img/mobile/basket/img_btn_close.png" alt="">
<div class="index_login-personal-area_content-main">
<h5 class="index_login-personal-area_title">Вход в личный кабинет</h5>
<a class="index_login-personal-area_btn mod_facebook" href="https://www.facebook.com/v2.8/dialog/oauth?client_id=897218913704734&amp;redirect_uri=https%3A%2F%2Fchocotelegram.ru%2Foauth%2Ffb%2Fcallback&amp;scope=email&amp;response_type=code">
<img class="personal-area_btn_img mod_facebook" src="/img/pop-up_log-in_facebook.png" alt="">
<p class="personal-area_btn_title mod_facebook">через facebook</p>
</a>
<a class="index_login-personal-area_btn mod_vk" href="https://oauth.vk.com/authorize?client_id=5877449&amp;redirect_uri=https%3A%2F%2Fchocotelegram.ru%2Foauth%2Fvk%2Fcallback&amp;scope=email&amp;response_type=code">
<img class="personal-area_btn_img mod_vk" src="/img/pop-up_log-in_vk.png" alt="">
<p class="personal-area_btn_title mod_vk">через вконтакте</p>
</a>
<hr class="index_login-personal-area_HR mod_left">
<p class="index_login-personal-area_OR">или</p>
<hr class="index_login-personal-area_HR mod_right">
<form class="index_login-personal-area_form" method="post" action="https://chocotelegram.ru/login">
<input type="hidden" name="_token" value="SWIT9suy2NrvVKdzST6KqLsCk4viNPN5AAg8i2Sd">
<div class="index_login-personal-area_form_item mod_email">
<input class="form_input mod_email" type="text" name="email" placeholder="e-mail">
</div>
<div class="index_login-personal-area_form_item mod_password">
<input class="form_input mod_password" type="text" name="password" placeholder="пароль">
</div>
<button type="submit">Войти</button>
<div class="form_check-box">
<input class="form_check-box_input" name="non-exit" value="1" id="non-exit" type="checkbox">
<label class="form_check-box_label" for="non-exit">
<span class="form_check-box_span"></span>Не выходить из системы
</label>
</div>
</form>
<a href="password/email.html" class="forget-password">Забыли пароль?</a>
</div>
</div>
</section>
<div class="notification">
<section class="notif notif-warn">
<h6 class="notif-title">Warning!</h6>
<p>There is as yet insufficient data for a meaningful answer.</p>
<img class="notif-close" src="/img/mobile/basket/img_btn_close.png" alt="">
</section>
<section class="notif notif-notice">
<h6 class="notif-title">Congratulations!</h6>
<p>You found the answer to the ultimate question of life, the universe, and everything.</p>
<img class="notif-close" src="/img/mobile/basket/img_btn_close.png" alt="">
</section>
<section class="notif notif-alert">
<h6 class="notif-title">Error!</h6>
<p>No information signal or material object can travel faster than light in vacuum.</p>
<img class="notif-close" src="/img/mobile/basket/img_btn_close.png" alt="">
</section>
</div>
<div class="popup video" style="display: none;">
<div class="back">
<img class="instruction__banner" src="/img/bg-instruction.png" alt="">
</div>
<nav class="popup__header">
<div class="popup__header-inner">
<a class="logo" href="/"></a>
<div class="right">
<a class="btn ghost icon popup__close">
<svg><use xlink:href="#close"></use></svg>
</a>
</div>
</div>
</nav>
<div class="popup-content">
<iframe width="560" height="315" src="https://www.youtube.com/embed/ABueqfb2Wz4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
</div>
</div>
<footer class="footer" role="contentinfo">
<div class="copyright">
<div class="social">
</div>
</div>
</footer>
<script src="/js/jquery.autocomplete.js"></script>
<script>
        jQuery.browser = {};
        (function () {
            jQuery.browser.msie = false;
            jQuery.browser.version = 0;
            if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                jQuery.browser.msie = true;
                jQuery.browser.version = RegExp.$1;
            }
        })();
    </script>
<script src="/js/city.js?1559831457"></script>
<script>if(!window.BX)window.BX={};if(!window.BX.message)window.BX.message=function(mess){if(typeof mess=='object') for(var i in mess) BX.message[i]=mess[i]; return true;};</script>
<script>(window.BX||top.BX).message({'JS_CORE_LOADING':'Загрузка...','JS_CORE_NO_DATA':'- Нет данных -','JS_CORE_WINDOW_CLOSE':'Закрыть','JS_CORE_WINDOW_EXPAND':'Развернуть','JS_CORE_WINDOW_NARROW':'Свернуть в окно','JS_CORE_WINDOW_SAVE':'Сохранить','JS_CORE_WINDOW_CANCEL':'Отменить','JS_CORE_WINDOW_CONTINUE':'Продолжить','JS_CORE_H':'ч','JS_CORE_M':'м','JS_CORE_S':'с','JSADM_AI_HIDE_EXTRA':'Скрыть лишние','JSADM_AI_ALL_NOTIF':'Показать все','JSADM_AUTH_REQ':'Требуется авторизация!','JS_CORE_WINDOW_AUTH':'Войти','JS_CORE_IMAGE_FULL':'Полный размер'});</script>
<script>(window.BX||top.BX).message({'LANGUAGE_ID':'ru','FORMAT_DATE':'MM/DD/YYYY','FORMAT_DATETIME':'MM/DD/YYYY H:MI:SS T','COOKIE_PREFIX':'BX18','SERVER_TZ_OFFSET':'10800','SITE_ID':'s4','SITE_DIR':'/','USER_ID':'','SERVER_TIME':'1559831457','USER_TZ_OFFSET':'0','USER_TZ_AUTO':'Y','bitrix_sessid':'a430f30ec134a66d9cc99333202ba00e'});</script><script src="/bitrix/js/main/core/core.js?1558967766123541"></script>
<script src="/bitrix/js/main/core/core_promise.js?15589677785220"></script>
<script src="/bitrix/js/main/polyfill/promise/js/promise.js?15409899358241"></script>
<script src="/bitrix/js/main/core/core_ajax.js?155896777841997"></script>
<script src="/bitrix/js/main/loadext/loadext.js?15409899492917"></script>
<script src="/bitrix/js/main/loadext/extension.js?15409899492895"></script>
<script src="/bitrix/js/main/core/core_db.js?153921739020929"></script>
<script src="/bitrix/js/main/json/json2.min.js?15392173903442"></script>
<script src="/bitrix/js/main/core/core_ls.js?153921739010430"></script>
<script src="/bitrix/js/main/core/core_fx.js?153921739016888"></script>
<script src="/bitrix/js/main/core/core_frame_cache.js?154098992917797"></script>
<script type='text/javascript'>
								$(document).ready(function(){

									$('body').on('mousedown', 'span.to-cart', function() {
										var atr = $(this).attr('data-item');
										try{rrApi.addToBasket(atr)} catch(e){}
									});
									

								});
							</script><script>(function(w, d){
    if (w.Giftd || typeof w.giftdAsync != 'undefined') { return; }
    w.giftdAsync = d.cookie.indexOf('giftd_s=') === -1;
    var ncs = 'giftd_nocache';
    var rnd = (d.cookie.indexOf(ncs) !== -1 || w.location.search.indexOf(ncs) !== -1) ? ('&' + Date.now()) : '';
    var vr = d.cookie.match(/giftd_v=([a-z0-9]+)+/i);
    var v = vr ? ('&v=' + vr[1]) : '';
    var fc = String.fromCharCode;
    var html = fc(60) + 'script src=\'https://giftd.tech/widgets/js/giftd_v2?pid=confaelshop.ru' + rnd + v + '\' id=\'giftd-script\' crossorigin=\'anonymous\' ' + (w.giftdAsync ? 'async=\'async\'' : '') + fc(62, 60) + '\/script' + fc(62);
    if (d.readyState == 'loading' && !document.querySelectorAll('script[src*=\'www.googletagmanager.com\']').length && !window.dataLayer) {
        d.write(html);    
    } else {
        var s = d.createElement('script'); s.src = 'https://giftd.tech/widgets/js/giftd_v2?pid=confaelshop.ru' + rnd + v;
        (d.body || d.querySelector('head')).appendChild(s); 
    }
})(window, document);</script>
<script>
					(function () {
						"use strict";

						var counter = function ()
						{
							var cookie = (function (name) {
								var parts = ("; " + document.cookie).split("; " + name + "=");
								if (parts.length == 2) {
									try {return JSON.parse(decodeURIComponent(parts.pop().split(";").shift()));}
									catch (e) {}
								}
							})("BITRIX_CONVERSION_CONTEXT_s4");

							if (cookie && cookie.EXPIRE >= BX.message("SERVER_TIME"))
								return;

							var request = new XMLHttpRequest();
							request.open("POST", "/bitrix/tools/conversion/ajax_counter.php", true);
							request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
							request.send(
								"SITE_ID="+encodeURIComponent("s4")+
								"&sessid="+encodeURIComponent(BX.bitrix_sessid())+
								"&HTTP_REFERER="+encodeURIComponent(document.referrer)
							);
						};

						if (window.frameRequestStart === true)
							BX.addCustomEvent("onFrameDataReceived", counter);
						else
							BX.ready(counter);
					})();
				</script>
<script src="/js/swiper.min.js?1550863858127934"></script>
<script src="/js/jquery.maskedinput.min.js?15460829754324"></script>
<script src="/js/custom.js?1559557580586"></script>
<script src="/js/desktop/app.js?15597346252151313"></script>
<script>new Image().src='https://confaelshop.ru/bitrix/spread.php?s=QlgxOF9BQlRFU1RfczQBATE1OTA5MzU0NTcBLwEBAQJCWDE4X0dVRVNUX0lEATM5NTc0NjABMTU5MDkzNTQ1NwEvAQEBAkJYMThfTEFTVF9WSVNJVAEwNi4wNi4yMDE5IDE3OjMwOjU3ATE1OTA5MzU0NTcBLwEBAQI%3D&k=8a004b89619cfbc1ede76602bab634ff';
new Image().src='https://lakomie.ru/bitrix/spread.php?s=QlgxOF9BQlRFU1RfczQBATE1OTA5MzU0NTcBLwEBAQJCWDE4X0dVRVNUX0lEATM5NTc0NjABMTU5MDkzNTQ1NwEvAQEBAkJYMThfTEFTVF9WSVNJVAEwNi4wNi4yMDE5IDE3OjMwOjU3ATE1OTA5MzU0NTcBLwEBAQI%3D&k=8a004b89619cfbc1ede76602bab634ff';
</script>
</body>
</html>