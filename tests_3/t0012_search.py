import sys
from time import sleep
import traceback
import re
from src.test import Test
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps
import src.mail as mail
"""XPATH end"""
mobile_search_button = "//button[contains(@class, 'inline-search-sho')]"
mobile_search_sumbit = "//div[@class='search-page-wrap']//input[@type='submit']"
pc_search_button = "button.btn-search"
pc_aria_search = "//div[@id='title-search_fixed']//input[@class='search-input']"
mobile_aria_search = "//div[@class='search-page-wrap']//input[@type='text']"
_search_result = "//div[contains(@class, 'catalog') and contains(@class, 'search')]"
_title = "//div[contains(@class, 'catalog') and contains(@class, 'search')]//div[contains(@class, 'item-title')]"
page_num = "//a[text()='{}']"
"""XPATH end"""
def step_1(Test, value, repeat):
    print(Test)
    Test.get("https://confaelshop.ru")
    Test.doc_readyState()
    Test.ajax_complite
    if Test.browser_name in ("ff", "ie", "c"):
        elem(Test, pc_aria_search, scroll=False, type_locator="xpath").click().input(value)
        elem(Test, pc_search_button, scroll=False).click()
    else:
        elem(Test, mobile_search_button, scroll=False).click()
        Test.doc_readyState()
        Test.ajax_complite
        assert elem(Test, mobile_aria_search, type_locator="xpath").in_page()
        elem(Test, mobile_aria_search, type_locator="xpath").click().input(value)
        elem(Test, mobile_search_sumbit).click()
    result = []
    Test.doc_readyState()
    Test.ajax_complite
    for page_number in range(repeat):
        count = len(elem(Test, _title, "xpath").return_all())
        for index in range(count):
            title = elem(Test, _title, "xpath", index=index).find()
            print((title.text, value, page_number + 1, count, " товар {}/{}".format(index + 1, count)))
            for a in value.split(" "):
                temp = []
                flag = Test.re(title.text, r"{}\S*".format(a))
                if flag is None:
                    temp.append("В заголовке товара <{}> отсутствует часть поискового запроса <{}> Страница поиска №{}".format(flag, title.text, a, page_number + 1))
            else:
                if len(temp) > 0:
                    result.append(temp)
        nav_page = elem(Test, page_num.format(page_number + 2), wait=10)
        if nav_page.in_page():
            print(result)
            return True if len(result) == 0 else False
            break
        nav_page.click()
    else:
        print(result)
        return True if len(result) == 0 else False
