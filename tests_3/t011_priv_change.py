import sys
import re
from time import sleep, time
import traceback
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps
import src.mail as mail
"""LOCATORS"""
link_forgot = "a.forgot"
css_email = "input[type=email]"
alert = "div.alert"
css_sumbit = "div.but-r button.btn"
css_error = "label.error"
css_alert = "font.errortext"
css_succes = "font.notetext"
css_fio = "input[name=NAME]"
css_tel = "input[name=PERSONAL_PHONE]"
css_email = "input[name=EMAIL]"
css_new_password_confirm = "input[name=NEW_PASSWORD_CONFIRM]"
"""LOCATORS"""
result = []

def step_1(Test):
    return steps.open_log_on(Test)


def step_2(Test, type_input):
    steps.log_on(Test, "user_tree", check=False)
    if not elem(Test, alert).in_page():
        Test.get("https://confaelshop.ru/personal/private/")
        Test.doc_readyState()
        Test.ajax_complite
        return step_3(Test, "user_tree_alt_login", type_input)
    steps.log_on(Test, "user_tree_alt_login", check=False)
    Test.get("https://confaelshop.ru/personal/private/")
    Test.doc_readyState()
    Test.ajax_complite
    return step_3(Test, "user_tree", type_input)


def step_3(Test, login, type_input):
    values = {"user_tree": {
                          "email": "Test@One.asd",
                          "tel": "+7 (907) 444-40-83",
                          "fio": "Test One"
                          },
              "user_tree_alt_login": {
                                      "email": "Test@two.asd",
                                      "tel": "+7 (907) 083-44-44",
                                      "fio": " Test Two"
                                        }
              }
    if type_input == "True":
        locator_result = css_succes
        password = (values[login]["email"], values[login]["tel"], values[login]["fio"])
    elif type_input == "False":
        locator_result = css_error
        count = 2
        password = ("x3", "Тестирование !@#$%^&*(123", "Тестирование !@#$%^&*(123")
    else:
        locator_result = css_error
        count = 3
        password = ("", "", "")

    email = elem(Test, css_email).double_click().input("backspase", button=True, check=False).input(password[0])
    tel = elem(Test, css_tel).double_click().input("backspase", button=True, check=False)
    for a in tel.get_attribute("value"):
        tel.input("backspase", button=True, check=False)
    tel.input(password[1], check=False)
    fio = elem(Test, css_fio).double_click().input("backspase", button=True, check=False).input(password[2])
    elem(Test, css_sumbit).click()
    if type_input == "True":
        Test.doc_readyState()
        Test.ajax_complite
        Test.get("https://confaelshop.ru/?logout=yes&login=yes")
        sleep(3)
        Test.get("https://confaelshop.ru/auth/?backurl=/personal/")
        Test.doc_readyState()
        Test.ajax_complite
        result.append(["Авторизация с новым логином успешна ", steps.log_on(Test, login)])
        print(login, result)
        return all(a[1] for a in result)
    else:
        Errors = elem(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {css_error}", wait=15)
        if Errors.in_page():
            count_error = len(Errors.return_all())
            print(f'колво ошибок не совпадает с заданным {count_error} / {count}')
            return count_error == count
        print("не корректный ввод не верен")
        return False
