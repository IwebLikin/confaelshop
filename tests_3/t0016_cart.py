from src.PageObject import elements
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps
"""LOCATORS"""
locators = dict(
                check="div.sale-personal-section-index-block",
                iframe="div.fancybox-container"
                )
"""LOCATORS"""
values = {"True": {
                   "tel": "89070834444",
                   "number_cart": "********"
                   },
          "None": {
                   "tel": "",
                   "number_cart": ""
                   },
          "False": {
                    "tel": "Тестирование !@#$%^&*(123",
                    "number_cart": "Тестирование !@#$%^&*(123"
                    }}

def step_1(Test):
    e = elements(Test, locators)
    steps.log_on(Test, "user_four")
    Test.get("https://confaelshop.ru/personal/bonus/")
    Test.doc_readyState()
    Test.ajax_complite
    e.check.click()
    return e.iframe.in_page()
