import sys
from time import sleep
import traceback
import re
from src.test import Test
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps

"""XPATH end"""
param = "//div[@class='display_fixed_wrapper']//div[contains(@data-prop_code, 'delivery')]//label[contains(@class, 'bx_filter_param_label')]"
pc_sumbit = "//div[@class='display_fixed_wrapper']//input[contains(@class, 'bx_filter_search_button')]"
_price = "//div[contains(@class, 'block_list')]//div[@class='price']"
_title = "//div[contains(@class, 'block_list')]//div[contains(@class, 'item-title')]"
page_num = "//a[text()='{}']"
mobile_filter_menu_open="//a[contains(@class, 'filter_opener')]"
mobile_menu_close= "//a[contains(@class, 'opened')]"
mobile_simbit = "//div[@class='bx_filter_section']//input[contains(@class, 'bx_filter_search_button')]"
mobile_param = "//div[contains(@data-prop_code, 'delivery')]//label[contains(@class, 'bx_filter_param_label')]"
retail = "//div[contains(@class, 'retailrocket-items')]"
pc_menu_open = "//div[contains(@data-prop_code, 'delivery')]//div[contains(@class, 'bx_filter_parameters_box_title')]"
pc_menu_check = "//div[@data-prop_code='delivery']//div[contains(@class, 'bx_filter_block') and contains(@class, 'limited_block')]"
finally_check = "//div[@class='info_item']"
"""XPATH end"""

cheker_dict = {
                "0": "Доставка только по Москве",
                "1": "Доставка только по Москве и МО",
                "2": "Доставка по РФ",
                "3": "Доставка только по Москве, МО и городам с бутиками Конфаэль"
                }


def step_1(Test, index):
    Test.get("https://confaelshop.ru/catalog/komu")
    if Test.browser_name in ("ff", "c", "ie"):
        print(elem(Test, param, index=index).find())
        for a in range(5):
            elem(Test, pc_menu_open).click()
            string = elem(Test, pc_menu_check).get_attribute("style")
            sleep(1)
            if Test.re(string, "display: none;") is None:
                break
        elem(Test, param, index=index).click()
        sleep(5)
        string = Test.driver.current_url
        return True if Test.re(string, "delivery-is") is not None else False
    else:
        print(elem(Test, param, index=index).find())
        for a in range(10):
            if not elem(Test, mobile_menu_close, wait=4).in_page():
                elem(Test, mobile_filter_menu_open).click()
                sleep(1)
            elif "active" not in elem(Test, "//div[@data-prop_code='delivery']").get_attribute("class"):
                elem(Test, "//div[@data-prop_code='delivery']").click()
            else:
                break
        elem(Test, mobile_param, index=index).click()
        for a in range(8):
            Test.doc_readyState()
            Test.ajax_complite
            string = Test.driver.current_url
            flag = Test.re(string, "delivery-is")
            print(Test.re(string, "delivery-is"), flag)
            if Test.re(string, "delivery-is") is not None:
                return True
            sleep(0.5)
        return False


def step_2(Test, indexcheck, count_links_check):
    result = []
    links = []
    sleep(6)
    count = len(elem(Test, _title).return_all())
    if count > count_links_check:
        count = count_links_check
    for index in range(count):
        title = elem(Test, _title + "//a", index=index).get_attribute("href")
        if Test.re(title, "confaelshop.ru", repeat=1) is None:
            title = "https://confaelshop.ru" + title
        links.append(title)
    print(count_links_check)
    for index in range(count_links_check):
        Test.get(links[index])
        html = elem(Test, finally_check).text
        if Test.re(html, cheker_dict[str(indexcheck)]) is None:
            result.append("Товар {} не прошел проверку по доставке <{}>".format(links[index], cheker_dict[str(indexcheck)]))
    else:
        print(result)
        return True if len(result) == 0 else False
