import sys
from time import sleep
import traceback
import re
from src.test import Test
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
import src.steps as steps
"""XPATH end"""
max_price = "//div[@class='display_fixed_wrapper']//input[@class='max-price']"
min_price = "//div[@class='display_fixed_wrapper']//input[@class='min-price']"
pc_sumbit = "//div[@class='display_fixed_wrapper']//input[contains(@class, 'bx_filter_search_button')]"
_price = "//div[contains(@class, 'block_list')]//div[@class='price']"
_title = "//div[contains(@class, 'block_list')]//div[contains(@class, 'item-title')]"
page_num = "//a[text()='{}']"
mobile_filter_menu_open = "//a[contains(@class, 'filter_opener')]"
mobile_menu_close = "//a[contains(@class, 'opened')]"
mobile_max_price = "//div[@class='bx_filter_section']//input[@class='max-price']"
mobile_min_price = "//div[@class='bx_filter_section']//input[@class='min-price']"
mobile_simbit = "//div[@class='bx_filter_section']//input[contains(@class, 'bx_filter_search_button')]"
retail = "//div[contains(@class, 'retailrocket-items')]"
"""XPATH end"""

def step_1(Test):
    Test.get("https://confaelshop.ru/catalog/komu")
    if Test.browser_name in ("ff", "c", "ie"):
        max_pr = elem(Test, max_price)
        print(max_pr.get_attribute("placeholder").replace(" ", ""))

        value = int(int(max_pr.get_attribute("placeholder").replace(" ", "")) / 2)
        step_2.max_price = value
        elem(Test, max_price).click().input(str(value))
        for a in range(6):
            print("макс", max_pr.get_attribute("value"), "макс ввод",  value, int(max_pr.get_attribute("value")) == value)
            if int(max_pr.get_attribute("value")) == value:
                break
            sleep(0.5)
        assert int(max_pr.get_attribute("value")) == value, "Ошибка ввода максимальной цены"
        value = int(value / 2)
        step_2.min_price = value
        min_pr = elem(Test, min_price)
        min_pr.click().input(str(value))
        for a in range(6):
            print("мин", min_pr.get_attribute("value"), "мин ввод", value, int(min_pr.get_attribute("value")) == value)
            if int(min_pr.get_attribute("value")) == value:
                break
            sleep(0.5)
        assert int(min_pr.get_attribute("value")) == value, "Ошибка ввода минимальной цены"
        for a in range(8):
            elem(Test, pc_sumbit).click()
            Test.doc_readyState()
            Test.ajax_complite
            string = Test.driver.current_url
            flag = Test.re(string, r".+MIN.+MAX.+")
            print(Test.re(string, "price-base-from-"), flag)
            print(string)
            if Test.re(string, "price-base-from-") is not None or Test.re(string, r".+MIN.+MAX.+") is not None:
                return True
            sleep(2)
        return False
    else:
        print("телефон")
        max_pr = elem(Test, mobile_max_price)
        for a in range(10):
            menu = elem(Test, mobile_filter_menu_open).click()
            sleep(0.8)
            if "opened" in menu.get_attribute("class"):
                break
        print(max_pr.get_attribute("placeholder").replace(" ", ""))
        value = int(int(max_pr.get_attribute("placeholder").replace(" ", "")) / 2)
        step_2.max_price = value
        max_pr.click().input(str(value))
        assert int(max_pr.get_attribute("value")) == value, "Ошибка ввода максимальной цены"
        sleep(3)

        value = int(value / 2)
        step_2.min_price = value
        min_pr = elem(Test, mobile_min_price).click().input(str(value))
        assert int(min_pr.get_attribute("value")) == value, "Ошибка ввода максимальной цены"

        for a in range(8):
            elem(Test, mobile_simbit).click()
            Test.doc_readyState()
            Test.ajax_complite
            string = Test.driver.current_url
            flag = Test.re(string, r".+MIN.+MAX.+")
            print(Test.re(string, "price-base-from-"), flag)
            if Test.re(string, "price-base-from-") is not None or Test.re(string, r".+MIN.+MAX.+") is not None:
                return True
            sleep(0.5)
        return False

def step_2(Test, repeat):
    result = []
    sleep(3)
    Test.doc_readyState()
    Test.ajax_complite
    for page_number in range(repeat):
        count = len(elem(Test, _title, "xpath").return_all())
        for index in range(count):
            count = len(elem(Test, _title, "xpath").return_all())
            if count < index + 1:
                break
            title = elem(Test, _title, "xpath", index=index).text
            price = int(elem(Test, _price, index=index).get_attribute("data-value"))
            print((title, price, page_number + 1, count, " товар {}/{}".format(index + 1, count)))
            if price < step_2.min_price or price > step_2.max_price:
                result.append("Товар {} с ценой {}. ошибка фильтра цена должна быть больше {} или меньше {}". format(title, price, step_2.min_price, step_2.max_price))
        nav_page = elem(Test, page_num.format(page_number + 2), repeat=1, wait=10)
        if not nav_page.in_page():
            print(result)
            return True if len(result) == 0 else False
        nav_page.click()
    print(result)
    return True if len(result) == 0 else False
