import schedule
import time

def job():
    import os
    os.system('pytest -s --html=report.html')


schedule.every().day.at("02:23").do(job)

while True:
    schedule.run_pending()
    time.sleep(2)
