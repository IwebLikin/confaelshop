import sys
import re
from time import sleep, time
import traceback
from src.DriverObject import driver_object
from src.PageObject import page_element as elem
from src.PageObject import elements
import src.steps as steps
import src.mail as mail
"""LOCATORS"""
locators = dict(
                search="input#title-search-input_fixed",
                wait="div.mobileheader-v1 I.svg-inline-basket",
                product_link="div.block_list div.item-title a",
                btn_send_gift="span[data-name=send_gift]",
                locator_form="div.form.SEND_GIFT",
                name="input[data-sid=FIO]",
                name_alt="input[data-sid=CLIENT_NAME_RECIPIENT]",
                email="input[data-sid=EMAIL_RECIPIENT]",
                sumbit="div.form.SEND_GIFT input.btn",
                error="div.form.SEND_GIFT label.error",
                success="div.form.SEND_GIFT div.success.form_result"
                )
""""LOCATORS"""

def step_1(Test):
    return steps.go_to_product_page(Test)


def step_2(Test):
    el = elements(Test, locators)
    Test.doc_readyState()
    Test.ajax_complite()
    el.btn_send_gift.click(pause=1)
    return True


def step_3(Test, type_input):
    el = elements(Test, locators)
    if type_input == "None":
        locator_result = locators["error"]
        name = ""
        name_alt = ""
        email = ""
        count = 3
    elif type_input == "False":
        name = "Тестирование !@#$%^&*(123"
        name_alt = "Тестирование !@#$%^&*(123"
        email = "Тестирование !@#$%^&*(123"
        locator_result = locators["error"]
        count = 1
    else:
        name = str(time())
        name_alt = "90698989898"
        email = "al8594212@gmail.com"
        locator_result = locators["success"]
        count = 1

    el.email.scroll = False
    el.email.double_click()
    el.email.input(email)

    el.name.scroll = False
    el.name.double_click()
    el.name.input(name)

    el.name_alt.scroll = False
    el.name_alt.double_click()
    el.name_alt.input(name_alt)
    el.sumbit.click(pause=1)
    if type_input == "True":
        for a in range(20):
            list_text = mail.get_mail()
            for text in list_text:
                result = re.search(name, text)
                if result:
                    print(result, "\n", name)
                    return True
            sleep(5)
        else:
            return False
    label = elem(Test, locator_result, scroll=False, message=f"Ошибка не найден элемент {locator_result}")
    if label.in_page():
        return len(label.return_all()) == count

if __name__ == "__main__":
    with driver_object("ff") as Test:
        step_1(Test)
        step_2(Test)
        step_3(Test, "False")
